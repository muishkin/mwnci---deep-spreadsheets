* Use the XVolatileResult interface. Initially return a placeholder indicating that the cell is being processed. 
* Put a bit of a debounce on things so we don't have loads of individual requests. Union the individual bits of SPARQL together and ensure we have some request ID included so that we can tell which result belongs to which caller. Could this be described as multiplexing or is batching just the best term to describe it?
* The initial result should be analysed by any cells which are dependent on it. It should only process its own request once it has recieved the resultant value.
* Cells with dependents will get updated with the new value once the dependent cell has written the value out.
* Ensure that requests are allowed to run in parallel (RxJava) so that if we do have concurrent requests, they don't block each-other.

Hopefully the above will improve performance just enough that it won't cause LibreOffice to freeze? 


Now I just have to figure out how on earth I'm going to cope with being able to get the formula OR the text value from cells.
Do I need two copies of functions to be run independently of oneanother?



Some random ideas:

Maybe the queries should look something like sparqlsearch("WikiData", "City", "http://www.wikidata.org/entity/Q126269")
Maybe the entity lookup should look something like sparqlentity("WikiData", A1) - Passing A1 as raw text
And to keep it simple sparqlvalue(A1, "WikiData", "Population") Passing A1 as raw text.

But this raw text passing would mean that we miss out on being able to deep-search through spreadsheets. Maybe we have an extra bit of functionality which keeps an lookup index synced for each spreadsheet we have open and stores this somewhere in the sheet (down the bottom) or as extra info in ODS files?
