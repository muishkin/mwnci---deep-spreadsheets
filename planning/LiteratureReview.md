# Literature Review
This literature review starts by exploring common spreadsheet errors which are relevent to the tool being build. This is then followed by a review of the  existing tools and methods for searching and exploring linked data so as to guide development of the user interface. Similarities are then drawn between existing tooling and this project in order to place it into context. The review continues by noting some of the tools used to bridge the gap between linked data and spreadsheet data in order to highlight standards and best practices which can be followed to support the interoperability of tooling in this space. This is followed by a brief review of the current state of automatic entity linking and disambiguation in order to provide direction for the future development of the Project Mwnci tooling. Finally, an analysis of Microsoft's [Project Yellow](https://www.microsoft.com/en-us/garage/wall-of-fame/new-data-types-in-excel/) will discuss the *Stocks & Geographies* data feature which is currently available to members of the Office Insiders programme.

## Errors in Spreadsheets
Errors are a common occurence in spreadsheets which can have devastating and long-term financial implications -- [Political analysis of error](https://www.theguardian.com/politics/2013/apr/18/uncovered-error-george-osborne-austerity) [Cambridge Journal of Economics highlighting the error](https://academic.oup.com/cje/article/38/2/257/1714018?casa_token=s23kozOnJskAAAAA:2kM-ECyRrfryus2qLO_FTlbLnAksXJBIFdGC1t8R-q7jtf9RBAggZ-lNobZ5CZhqYZS1pl8ch_ehKw#25150792). Much research has been done in to the types and frequency of errors which commonly affect users of spreadsheets and there are claims of per-cell error rates varying between 1% and 5% with errors existing in approximately 90% of operational spreadsheets -- [What We Don’t Know About Spreadsheet Errors Today - Panko](https://arxiv.org/pdf/1602.02601.pdf).  

### Semantic Errors
> Semantic errors occur where the contents of a spreadsheet are poorly understood by the end-user. 

Where spreadsheets are shared between multiple users or used as a tool for mass communication of data, the structure and information contained can often be ambiguous, ill-defined or inconveniently defined. It has been suggested by [Context in Spreadsheet Comprehension](https://pdfs.semanticscholar.org/e22a/4f017038230fd80118a8167698c48d0ec72e.pdf) that providing supplementary contextual information helps avoid common semantic errors. Following on from this research, it stands to reason that allowing users to annotate cells with globally defined URIs to defined terms and contents could go some way to providing the context which could make similar errors more difficult. To maximise the benefit of semantic understanding it would likely be necessary to develop functionality to bring a summary of the contextual information to the user inside the spreadsheet editor.

### Inaccurate Data
Many errors in spreadsheets are due to inaccurate data. Inaccurate data may result from incorrect user input or be due to the data being out of date. Project Mwnci should address these issues by providing access to accurate and up-to-date data and so should help combat this issue.

[Spreadsheet Errors and Decision Making: Evidence from Field Interviews a --- "Inaccurate data"](https://www.igi-global.com/article/spreadsheet-errors-decision-making/3827) discusses a scenario where data in a cell was drawn from a database call. Users placed too much trust in the magical value from the database and were uncritical of what it represented. Project Mwnci must be aware of this issue and address it by making the contextual information of values easily available if it is to avoid *increasing* the number of errors made by its users.   

## Searching and Browsing Linked-Data
The primary purpose of project Mwnci is to allow end-users to explore and build on top of linked-data within the familiar spreadsheet environment. Whilst this project cannot hope to replicate the more exotic methods for querying and browsing linked-data, it should take note of the better methodologies and enable them to be incorporated at a later date.

### Querying
Querying interfaces occupy a number of different forms in the linked-data space. There are three types of model which surface from an analysis of the literature: querying using the technical SPARQL query language, querying using some form of natural language interpreted query and faceted browsing. Querying for aggregated values will not be considered at this stage of the project as it will naturally require a more complex user interface which is beyond the planned scope.

#### SPARQL
SPARQL querying represents the technical solution to enabling users to query large sets of linked-data [(W3C specification)](https://www.w3.org/TR/sparql11-query/). It allows the user to filter, group and perform aggregate queries on data and in general has a similar set of querying functionality to the more widely-known SQL language. 

The language must be correctly formatted by the user and requires a reasonably degree of technical skill and understanding of the data's graph structure in order to craft appropriate queries. Further compounding this technical complexity, [Wikidata](https://wikidata.org), which in the author's view comprises the most general purpose and useful source of linked-data currently available, chooses to further obfuscate SPARQL queries by using generic and meaningless URIs to represent common entities and relationships. An example of a simple query which lists the name of towns can be seen [here](https://query.wikidata.org/#SELECT%20DISTINCT%0A%20%20%3FtownName%0A%20%20%3FcountryName%0AWHERE%20%7B%20%0A%20%20%3Ftown%20wdt%3AP31%2Fwdt%3AP279%2a%20wd%3AQ3957%3B%20%23%20wdt%3AP31%2Fwdt%3AP279%2a%20means%20%27is%20or%20is%20a%20subclass%20of%27.%20wd%3AQ3957%20means%20%27town%27.%0A%20%20%20%20%20%20%20%20rdfs%3Alabel%20%3FtownName%3B%0A%20%20%20%20%20%20%20%20wdt%3AP17%20%3Fcountry.%20%23%20wdt%3AP17%20represents%20the%20country%20of%20the%20entity.%0A%20%20%3Fcountry%20rdfs%3Alabel%20%3FcountryName.%0A%20%20%0A%20%20FILTER%28LANG%28%3FtownName%29%3D%27en%27%29%20%23%20Ensures%20we%20only%20get%20english%20languages%20results%20for%20%3FtownName%0A%20%20FILTER%28LANG%28%3FcountryName%29%3D%27en%27%29%20%23%20Ensures%20we%20only%20get%20english%20languages%20results%20for%20%3FcountryName%0A%7D%0ALIMIT%20100%0A):

```sparql  
SELECT DISTINCT
  ?townName
  ?countryName
WHERE { 
  ?town wdt:P31/wdt:P279* wd:Q3957; # wdt:P31/wdt:P279* means 'is or is a subclass of'. wd:Q3957 means 'town'.
        rdfs:label ?townName;
        wdt:P17 ?country. # wdt:P17 represents the country of the entity.
  ?country rdfs:label ?countryName.
  
  FILTER(LANG(?townName)='en') # Ensures we only get english languages results for ?townName
  FILTER(LANG(?countryName)='en') # Ensures we only get english languages results for ?countryName
}
LIMIT 100
```

This technical complexity ensures that SPARQL is not a suitable tool for the layperson and so is not suitable as the primary user interface for querying or browsing data in this project.

#### Natural Language Querying
Natural language query models such as [Sparklis](https://wiki.dbpedia.org/projects/sparklis) have been developed to aide users in querying linked-data graphs without the requirement of learning a technical querying languae such as SPARQL. 

requires learning particular phrases.
And [NLI-GO](http://patrickvanbergen.com/dbpedia/app/) - seems very poor though.
And [dbpedia chat](http://chat.dbpedia.org/) - pretty poor too

#### Faceted Search
* [Querying Heterogeneous Datasets on the Linked Data Web: Challenges, Approaches, and Trends](https://ieeexplore.ieee.org/abstract/document/6051410)

An entity-centric query model focuses on allowing the user to find a particular entity matching restrictive criteria. Th

Entity-centric matches well with spreadsheets, where each cell represents an individual entity.

" Search and query platforms should explore this complementary aspect with regard  to  heterogeneous  data  to  enable  users  to  switch  among  different  search  and  query  strategies" (https://ieeexplore.ieee.org/abstract/document/6051410)
* [Tabulator](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.620.5246&rep=rep1&type=pdf) - Discusses many different interfaces necessary for search to really work here.
* [Linked Data Query Wizard:A Novel Interface for Accessing SPARQL Endpoints](http://events.linkeddata.org/ldow2014/papers/ldow2014_paper_06.pdf) (worse but original link) - [Linked Data Query Wizard: A Tabular Interface for the Semantic Web](https://link.springer.com/chapter/10.1007/978-3-642-41242-4_19) - A seemingly sensible faceted search which results in a tabular output.

### Browsing 
> How will the user explore (and make use of) the data linked to the entity?

> How on earth will the user deal with the wealth of information surrounding high-arity entities like those on Wikidata?

* Graphs are poor representations of linked data [The Pathetic Fallacy of RDF](https://eprints.soton.ac.uk/262911/1/the_pathetic_fallacy_of_rdf-33.html) - Suggests that it's better to represent RDF data in a context-specific fashion which is best suited to the designed task. Suggests that graphs as the de facto way of representing RDF data is a poor choice. Focus vs context usability issues.
* [Original Focus + Context paper?](http://byu.danrolsenjr.org/cs656/Papers/FocusPlusContext.pdf)

## Integrating Spreadsheets with Linked-Data
Tools people have developed to annotate existing spreadsheets. Are there any standards which might be useful to use for Project Mwnci?

### RightField
[RightField](https://rightfield.org.uk/) seems to be the big boy allowing users to annotate (and do data domain restrictions on cell values). 

[RightField: Semantic enrichment of Systems Biology data using spreadsheets](https://ieeexplore.ieee.org/abstract/document/6404412) - Formally goes over use-cases for RightField and provides a critical analysis.

RightField Supports:

* Generation of spreadsheet templates to be used by experimental scientists. The spreadsheets are built using an application which imports ontologies and allows the author to specify that the domain of a cell's value is restricted to that defined by the imported ontology.
* It allows the experimental scientist to stick to the ontology and record experimental results without having to be aware of the existance of an ontology or RDF. 
* It provides functionality for extracting RDF data from these special spreadsheets which can then be integrated in to larger RDF data-stores (with SPARQL interfaces).

So it's a great tool for allowing individuals to generate RDF data but isn't a suitable tool for this project as it's geared more towards the capture of data, rather than the analysis of it.

RightField supports ontological annotation by linking the cell's domain restriction to a specific hidden sheet containing a lookup table mapping entity name to entity URI. This annotation storage mechanism is so heavily tied to the setup of RightField spreadsheets that it cannot be considered a portable standard.

### OntoMaton
[OntoMaton](https://github.com/ISA-tools/OntoMaton) - Tool for annotating values in *Google Sheets*. OntoMaton supports annotation in three formats: ISA-TAB, `Entity URI hyperlink` and `Entity Label (Entity URI hyperlink)`. It supports the ability to search through ontologies to find matching terms and then supports insertion in to spreadsheets. The entity search functionality is a primative interface which allows text-matching of individual terms within a pre-defined list of biologically relevant ontologies. It provides a tree-based interface for users to navigate down through the relevant ontologies to find the correctly matching term.

#### ISA-TAB Format
The ISA-TAB format was [developed](https://www.liebertpub.com/doi/pdfplus/10.1089/omi.2008.0019) as part of a drive to standardise data-formats in the biological *-omics* (transcriptomics, proteomics, (meta)genmoics, etc.) field. The ISA-TAB specification requires that each ontologically annotated term is represented by a group of three columns:

* A column containing a human-readable name or description.
* `Term Source REF` column specifying the ontology where this term is defined.
* `Term Source Accession Number` column specifying the entity URI.

A simple example of the layout can be seen below. Note that EFO represents the [Experimental Factor Ontology](https://www.ebi.ac.uk/ols/ontologies/efo) from which all of the terms have been retrieved.

|Element|Term Source REF|Term Source Accession Number|
|:-|:-|:-|
|Carbon|EFO|http://purl.obolibrary.org/obo/CHEBI_27594|
|Oxygen|EFO|http://purl.obolibrary.org/obo/CHEBI_15379|
|Silicon|EFO|http://purl.obolibrary.org/obo/CHEBI_27573|

### Spreadsheet to RDF Mapping
In order to ensure that Project Mwnci fits in to the existing RDF spreadsheet ecosystem, it is necessary to briefly survey RDF to spreadsheet export tools to investigate whether there are any de facto standards which Project Mwnci could integrate with.

The only de facto standard which multiple tools follow is to avoid generating RDF entity URIs for columns, rows or values which are defined by a valid URI. This helps avoid duplicating existing entities and preserves the linked nature of linked data. For example the following table with a reference to `foaf:nick` ought to continue to reference the foaf URI after conversion to RDF. For example:

|ID|First|Last|http://xmlns.com/foaf/0.1/nick|
|:-|:-|:-|:-|
|1|Alan|Turing|A-Dawg|

ought to be converted to RDF of the form (turtle syntax):

```clojure
@prefix mwnci: <http://some.domain/mwnci/legends>.
@prefix foaf: <http://xmlns.com/foaf/0.1/>.

mwnci:1 mwnci:First "Alan"^^xsd:string;
        mwnci:Last "Turing"^^xsd:string;
        foaf:nick "A-Dawg"^^xsd:string.
```

Spreadsheet to RDF tools which were surveyed include:
* [RDF123](https://link.springer.com/chapter/10.1007%2F978-3-540-88564-1_29) provides a mapping syntax which can be used to map arbitrary spreadsheet tables to RDF values. It appears to support the notion of retaining URIs as linked references as demonstrated above: ["If the label is not an RDF123 expression but a valid URI, we make it a resource; otherwise it is a literal"](https://link.springer.com/chapter/10.1007%2F978-3-540-88564-1_29).
* [XLWrap](https://link.springer.com/chapter/10.1007%2F978-3-642-04930-9_23) - A tool which provides a more expressive algebra than RDF123 to support mapping from representations more complex than simple tables. In addition, it provides a SPARQL endpoint for querying the data. It does not provide any explicit functionality for preserving linked URI records, but instead provides the user with the ability to specify URIs or leave them to be automatically generated. Thus, functionality which ensured that raw URIs could be placed into documents would support users in preserving linked URI records.
* [Any23](https://any23.apache.org/dev-csv-extractor.html) is an Apache project which supports the generation of RDF data from a variety of source bzdatatypes. When importing spreadsheets, it can automatically import [RFC 4180](http://www.ietf.org/rfc/rfc4180.txt) structured CSV files. It automatically retains URIs to avoid creating duplicate entities as described in the above example.
* The [RDF Spreadsheet Editor](https://pdfs.semanticscholar.org/02b0/b84b7609f9bf8ebab897c9db13d6813f730e.pdf) provides a web-based spreadsheet interface as a guide for generating RDF from a two dimensional table structure. It attempts to retain the linked nature of URIs by allowing the import of ontologies.


## Automatic Entity Linking - Future Plans
The topic of entity linking deals with the ability to provide a best-fit match between user input data and a data-set of known entities. It is a topic which deserves a literature review in it own light and cannot reasonably be addressed in depth as part of this research project. However, given the immense improvements in useability of the tooling which could be achieved by use of automated entity linking, it is a topic which needs to be discussed. The ultimate goal is to provide a tool which analyses the user's spreadsheet document or surrounding cells in order to guide the user in correctly linking their data to public RDF data. It must address problems of incomplete or inaccurate user input, words which have multiple meanings (polysemous words), multiple words which represent similar underlying concepts (synonymous words) and should also guide the user is selecting the most specific category which describes the contextually relatev entities in order to support exploration of similar data.

The two possible use-cases for this technology discussed in this section are:

* *primary use-case* - a user inputs entities in a free-text fashion, selects them together and asks the Mwnci tool to annotate the entities with matching entities. This would imply that the selected entities are all of the same category and so can be used for contextual disambiguation.
* *secondary use-case* - the tooling analyses the whole spreadsheet document and suggests categories of linked-data which may be relevant given topics inferred from the document.


### Free-text Entity Linkage
Much work has been performed in the area of free-text entity linkage and disambiguation. There are many tools which claim to analyse free-text and provide suitable annotations linked to known entities. These tools frequently use the surrouding textual context to provide disambiguation of polysemous terms. Tools of this kind include [DBPedia Spotlight](https://www.dbpedia-spotlight.org/) ([paper](http://oa.upm.es/8923/1/DBpedia_Spotlight.pdf)). Whilst these tools have been thoroughly researched their use in the context of spreadsheet documents may be limited due to their inability to consider structured data as well as the lack of prosaic text. However, the ease of use of tools such as the [DBPedia Spotlight API](https://www.dbpedia-spotlight.org/api) may provide a quick and easy route towards acceptable results. 

### HTML Table Entity Linkage
TODO: Discuss those tools out there which link HTML tables to knowledge graphs. They mostly seemed to 

### Topic Extraction
Topic extraction typically involves the application of the Latent Dirichlet Allocation (LDA) method to documents of unstructured text. It seeks to learn a pre-defined number of topics from these documents which best describe the contents of the articles. By learning the best topics to differentiate the set of documents, it is then possible to predict the topics contained in a user's active document. These predicted topics could be used to suggest appropriate data filters which might aide the user (*secondary use-case*) as well as providing help in entity disambiguation (*primary use-case*).

Given the frequently hierarchical nature of linked data and the sheer number of topics which large linked-data sources naturally contain, it may be of use to follow the approach taken in [Topic extraction using local graph centrality and semantic similarity](https://onlinelibrary.wiley.com/doi/full/10.1002/cpe.5054) which attempts to directly address these issues over the substantial WikiData data-set. Notably, the paper attempts to deal with the problem of "noisy categories" which can be roughly described as coincidental or entropy-poor categories which link many entities but not provide a contextual-appropriate filter.

## Microsoft's Project Yellow
[Project Yellow](https://www.microsoft.com/en-us/garage/wall-of-fame/new-data-types-in-excel/) is a Microsoft Office project designed to bring data from the Bing knowledge graph in to Microsoft Excel. It is currently limited to *Geography* and *Stocks* datatypes and allows users to reference up-to-date information related to stocks, towns, cities and other entities directly from cells in spreadsheets. [Preview of Stocks and Geography, New Data Types in Excel](https://techcommunity.microsoft.com/t5/Excel-Blog/Preview-of-Stocks-and-Geography-New-Data-Types-in-Excel/ba-p/176185) provides a summary of the available functionality. Notable functionality is summarised below:

* Ambiguous entities are presented to users from which a specific choice can be made. The current tool does not appear to make much use of the original entity disambiguation functionality which was part of the initial [Instafact project prototype](https://youtu.be/l9vtkRESvrw).
* Information about the entity can be viewed on a *card* inside the application which may go some way towards alleviating semantic errors in spreadsheets. 
* The Excel table interface allows the easy addition of related data by adding a column to the table. Related values are summarised when adding the column via a mechanism which shares similarities with the [CODE Linked Data Query Wizard (video)](https://youtu.be/0LZ87yj5jo8?t=76).
* One-to-many relationships are supported by the use of the new [dynamic array](https://techcommunity.microsoft.com/t5/Excel-Blog/Preview-of-Dynamic-Arrays-in-Excel/ba-p/252944) datatype which allows formulae to spill over into adjoining cells if many values are returned. Most importantly, this differs from the existing spill-over formula functionality in that the user can treat it like a regular formula without having to press `Ctrl-Shift-Enter` for correct evaluation. At the time of writing no equivalent of Microsoft Excel's dynamic array support in OpenOffice or LibreOffice has been announced. 
* Formulae are structure in a fashion which will be familiar to object-orientated programmers. The `=EntityCell.Property` (e.g. `=B2.Area` where B2 contains a linked entity) formula syntax provides a simple and user-friendly way to access properties. Unfortunately, implementing a similar syntax in OpenOffice/LibreOffice would likely require significant work to refactor the code which evaluates cell formulae. In addition, implementation of this syntax for Project Mwnci would remove the flexibility to look up property values from a different SPARQL end-point to the one used to look-up the entity.
* [Offline funtionality is supported](https://youtu.be/gbImc7A8_Q8?t=156) once the entity has been linked to the online data-source allowing users to go offline and continue working where connections are unreliable.

One key limitation of the Project Yellow approach is that [limited access](https://docs.microsoft.com/en-us/azure/cognitive-services/bing-Entities-Search/overview) to the knowledge graph driving the service will inevitably lead to divergence between Excel and rival desktop office solutions such as OpenOffice, LibreOffice and Apple Numbers. Further, it is uncertain how this information would be exported and stored in open document formats required by many Government departments. This siloed approach to data which relies on Microsoft Office's unparalleled market position will likely keep knowledge behind a paywall and may further entrench their dominant market position. Further, the walled garden approach to data will likely make it difficult for third parties to integrate their data-sets in to Microsoft Excel, thereby limiting the potential usefulness of the Project Yellow toolset. 

## Random notes

* Need to consider how to deal with **fall-back** where a label doesn't exist in the desired language.

["Queryable data is updated at least once per minute to keep synchronised with updates"](http://ceur-ws.org/Vol-2073/article-03.pdf)
