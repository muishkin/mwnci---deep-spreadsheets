# Project Mwnci

> "In many of the more relaxed civilizations on the Outer Eastern Rim of the Galaxy, the Hitch-Hiker's Guide has already supplanted the great Encyclopaedia Galactica as the standard repository of all knowledge and wisdom, for though it has many omissions and contains much that is apocryphal, or at least wildly inaccurate, it scores over the older, more pedestrian work in two important respects. First, it is slightly cheaper; and secondly it has the words DON'T PANIC inscribed in large friendly letters on its cover." - Douglas Adams, The Hitchhiker's Guide to the Galaxy

Spreadsheets serve a vital role in businesses, governments and laboratories across the modern world. They function as a user-friendly tool for the lay-programmer to run statistical analyses and implement logic which can be used to support scientific discoveries or to make major business and financial decisions. 

Unfortunately spreadsheets are far from perfect, being [error-prone](https://arxiv.org/pdf/1602.02601.pdf) and tending to become islands of isolated data seperate from larger collections. Project Mwnci aims to combat these issues by bringing the web's linked-data inside spreadsheets.

## What is Linked-Data?
Linked-data is data which:

* Uses HTTP URIs to uniquely identify each entity, object or concept.
* Uses [W3C standards](https://www.w3.org/standards/semanticweb/data) as the machine-readable way of querying the data.
* Includes links to other linked-data sources where necessary.

The idea of linked-data was [explored in 2006](https://www.w3.org/DesignIssues/LinkedData.html) by Tim Berners-Lee, the architect of the World Wide Web, as a way to turn the web into a decentralised machine-readable and queryable database representing humanity's knowledge. Today linked-data is made available by governments, companies and universities across the world.

## How will Linked-Data Improve the Spreadsheet Experience?
Project Mwnci aims to use linked-data to address the following three key issues which can make working with spreadsheets difficult:

* **isolated data islands** - spreadsheets tend to be isolated islands of data which rarely reference any values outside of their own file. Project Mwnci will allow users to reference data from a number of online data-sources such as [data derived from Wikipedia](https://www.wikidata.org) as well as [data published by the U.S. Government](https://www.data.gov/developers/semantic-web). This data will be available to the user without the need for them to invest in costly IT infrastructure, learn a data-query language like SQL or to make use of expensive custom-built extensions.
* **the wrong data** - spreadsheets can easily incorporate out-of-date, incorrect or inaccurate data which can be difficult and time-consuming to correct across across a large number of documents. Project Mwnci allows the user to reference data directly from inside the spreadsheet without having to copy values manually from various documents. This approach may reduce the likelihood of users inputting erroneous data. Most importantly, it will ensure that the latest available data is used every time they opens the document. 
* **ambiguity** - does "Washington" refer to the former U.S. President, the state or the city? Spreadsheet documents can often contain references which are understood only by the authors of the document. Project Mwnci will provide tools to support users in removing this ambiguity by specifying which entity they meant. These linked-data references can be looked up by anyone with an internet connection and often provide information in multiple languages.

## Aims
This project aims to create a LibreOffice/OpenOffice extension which provides the functionality necessary to reference linked-data entities and related values inside spreadsheets. The resulting extension should minimise the typically steep learning curve required to make use of linked-data and enable the user to work with data in the familiar spreadsheet paradigm. As such, users must be able to use common spreadsheet functionality when working with linked-data, e.g. referencing values in formulae as well as use of the fill-down/fill-across to populate a range of values. 

### Example Use-Cases
The following examples use-cases demonstrate the desired functionality and simplicity which the project should provide.

### Chemistry Student
A chemistry student wishes to create a spreadsheet to record the results of an experiment. In order to correctly analyse the results, the student needs to convert the mass of a known chemical in grams into the number of moles of that chemical present. With the help of the Mwnci extension, the student imports the molar mass of the known chemical from a chemical linked-data store and Avagadro's constant from a linked-data store holding physical constants. The student can then reference these linked-data cells and make the necessary calculations. 

This scenario ensures that the student does not incorrectly calculate (or copy) the molar mass of the chemical. The student also avoids incorrect input of Avagadro's constant. As part of this process, the student would have already annotated the chemical structure and would have access to a range of its known chemical properties.

Brief example of pulling a range of physical constants out of wikidata:
```sparql
SELECT *
WHERE {
  ?x wdt:P31/wdt:P279* wd:Q188248;
     rdfs:label ?xlab;
     wdt:P1181 ?xval.
  FILTER(LANG(?xlab)='en')
}
```

### Sales Worker - Estimating Demand
An enterprise is considering opening a new store but does not know which city/town to choose from their short-list. A sales worker creates a spreadsheet with a list of the short-listed towns and wants to estimate the demand present for the company's products. The sales worker can easily import information on the town's population, physical size and GDP per capita (where available) from Wikidata. Once the correct attributes have been selected for the first city, fill-down/fill-across can be used to populate the relevant values for the remaining cities. The sales worker can then make an analysis which informs the decision making process using the latest available data. The same analysis can be repeated a year later when the company plans to expand further.

# Links

* [Paper - Taxonomy of Spreadsheet Errors](https://arxiv.org/pdf/0805.4224.pdf)
* [Paper - Types of errors which affect business decisions](https://figshare.com/articles/Spreadsheet_Errors_and_Decision_Making_Evidence_from_Field_Interviews/6471929)
	* Seems to cast doubt on the utility of referencing from DB sources - people trust the value too much and avoid critical thinking [particularly when it came to the units of numerical results].
* [Paper - Modern [2016], talks about 23% of errors are mechanical & tries to suggest testing as the way forward](https://arxiv.org/pdf/1602.02601.pdf)
