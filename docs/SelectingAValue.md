# Selecting a Value
The *Value Search panel* allows the user to find information directly related to a given entity (cell). The below image shows an example where the directly related properties of the *G11* entity cell are displayed for user selection. This allows the user to reference related information in an alternative cell directly from their spreadsheet. Importantly, this information is included in a way that the data is updatable directly from the SPARQL source.

An example of the panel overview can be seen below: 

![Selecting a value related to the 'cheese' entity contained in cell G11 using the WikiData data-source.](./img/ValueSearch_Selection.png)

## The Subject Entity
The *subject* entity is the entity (cell) which we want to pull information about. Below we see the functionality which displays which cell and entity are selected:

![Information informing the user which cell (and hence Subject entity) we are viewing the properties of. In this instance cell G11 is selected which contains the 'Cheese' entity. The user is provided with a number of WikiData properties related to the 'Cheese' entity.](./img/ValueSearch_SelectedCellEntity.png)

It should be noted that the *Entity Cell* property tracks the cells previously selected by the user. It tracks the last selected cell which currently contains a URI. In addition to the currently selected cell's reference, it is possible to see the name of the entity that it holds.

## Selecting a Source
The source can be chosen in the same same way as described in [Selecting An Entity](./SelectingAnEntity.md). It is important to note that the data-source which is specified here need not be the same as the data-source used for entity lookup as a different data-source may hold the data you require about this entity. 

The same data-source selection is available as in the Entity Search panel and of course custom SPARQL endpoints can be specified:

![Using a custom SPARQL data-source in the ValueSearch.](./img/ValueSearch_CustomSource.png)

Note the **"Error: No Label Found"** value present in the above screenshot. This is a common feature of a numeber of data-sources where they do not always contain enough information to describe the predicates that they use. Where this error occurs, selection of the appropriate predicate (and hence value) should be made by careful consideration of the predicate's URI.

## Text Search
Free-text search can be performed in a similar way to that described in [Selecting An Entity](./SelectingAnEntity.md).

![Demonstration of a free-text filter applied to the properties linked to the 'cheese' entity in cell A1 on WikiData.](./img/ValueSearch_TextSearch.png)

## The Formula
Similarly to the *Entity Search panel* we see that the *Value Search panel*'s search parameters are stored on JSON inside the cell's formula. It should be noted that this functinality is likely to be removed or changed substantially in future Mwnci releases.

![The full sparql value function with panel search properties held in JSON format.](./img/ValueSearch_FullFormula.png)

Further, the formula can be simplied to remove the JSON parameters and other values which can be replaced by suitable defaults:

![The simplified formula without search parameters.](./img/ValueSearch_SimplifiedFormula.png)

The simplest form of the value selection formula is:

![The simplest formula possible. When the parameter specifying the predicate's language is not specified, it is taken to by the user's current locale language.](./img/ValueSearch_SimplestFormula.png)

### Exact Text Match
To provide additional functionality to aide the power user, it is possible to specify the name of the entity's property directly in a specified language:

![Example of matching on property name rather than the property predicate. This form of the formula cannot be set by the value search panel and is designed to simplify the process of value extraction for experienced users.](./img/ValueSearch_PropertyExactTextMatch.png)

It should be noted that this performs a case-insensitive exact match on the label of the entity's predicate (`image`) in the specified predicate language (`en`).

## Too Many Values
It is often the case that entities have properties with multiple values. In the case of the `cheese` entity we can see that the `image` property has multiple values associated with it:

![Demonstration of behaviour when there are too many values to be returned in a single cell. In this instance there are multiple images related to cheese.](./img/ValueSearch_TooManyEntities.png)

In this case, Mwnci returns an error message informing the user of this problem so that multiple values are brought to the user's attention rather than quietly hidden. However, it is possible for the user to manually override this behaviour by passing `TRUE()` to the optional `Allow Many Values` argument of the `sparqlValue` function. An example of this can be seen below where a comma seperated list of values is listed:

![Example of allowing multiple values to be returned for a property. Here we see a comma separated list of images associated with the 'cheese' entity mentioned earlier.](./img/ValueSearch_AllowMultipleValues.png)

## Solar System Example
Putting both the [entity selection](./SelectingAnEntity.md) and the [value selection](./SelectingAValue.md) processes together allows the user to build rich documents which reference live data from internet connected data-sources. An example of such a document is shown below where *planetary bodies* in column A have been selected using the *entity search panel*. Columns B, C and D have been populated by using the *value search panel* to populate the values for `Earth` and then using the standard spreadsheet *fill-down* functionality to populate the values for the remaining *planetary bodies*. 

![Example of the value search functionality used to display live mass, population and parent body for bodies in the solar system.](./img/ValueSearch_FillDownExample.png)