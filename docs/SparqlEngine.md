# Overview
The Mwnci `lib` project contains all of the code necessary to enable the [Office interface](../OfficeInterface) to communicate with remote SPARQL endpoints.

# Use of RDF4J
The project makes use of the [RDF4J](https://rdf4j.eclipse.org/) library. It provides [models](https://rdf4j.eclipse.org/documentation/sparqlbuilder/) and [repository services](https://rdf4j.eclipse.org/documentation/programming/repository/) to enable interaction with remote SPARQL repositories as well as allowing the creation of custom local SPARQL repositories from raw data. Use of this package abstracts the code necessary to interact with RDF repositories and simplifies the interaction with them.  The models are used throughout the project to provide a guided and structured way of composing queries from simpler statements. 

# ISparqlService + ISparqlProvider architecture.
This section discusses the role and relationships between the *ISparqlService* and *ISparqlProvider* interfaces which can be used to query SPARQL end-points when combined together.

# Consuming SPARQL Data
SPARQL data is consumed by combining an instance of the *ISparqlService* interface with an instance of *ISparqlProvider*.

Each instance of *ISparqlProvider* represents a different SPARQL data-source and the *ISparqlService* interface combines the outputs from an *ISparqlProvider* with standard templates to generate SPARQL queries which return consistent results across different end-points.

## *ISparqlService* interface
The *ISparqlService* interface specifies functions which enable the caller to look up and search SPARQL data-sources for entities and properties. Each function defined by the interface takes an instance of *ISparqlProvider* as an argument to allow the query to be tailored for a specific SPARQL end-point.

### Access
The *ISparqlService* may be accessed by request from the `cymru.mwnci.sparql.Mwnci` static class:

```java
ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);
```

Please see the code for further documentation on how to make use of the ISparqlService.

## *ISparqlProvider* Interface
The *ISparqlProvider* interface allows customisation of SPARQL queries for specific end-points. Different imeplementations of the *ISparqlProvider* interface allow the building of SPARQL queries which are semantically tailored and performance optimised for a particular end-point.

An instance of a given *ISparqlProvider* can be accessed from the `cymru.mwnci.sparql.Mwnci` static class, by name e.g.:

```java
ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider("WikiData")
    .orElseThrow(() -> new SparqlProviderNotFoundException("WikiData")); 
```

### *StandardSparqlProvider*
If there is no custom *ISparqlProvider* for a given SPARQL end-point, the *StandardSparqlProvider* can be instantiated for the end-point, e.g.:

```java
ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider("https://data.something.com/sparql")
    .orElseThrow(() -> new SparqlProviderNotFoundException("This should never happen. https://data.something.com/sparql")); 
```

## Searching for Entities
```java
ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);

ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider("WikiData");
        .orElseThrow(() -> new SparqlProviderNotFoundException("WikiData")); 

String searchText = "Delyth Evans";
String searchLanguage = "en"; // The language the search text is in and the entity label should be specified in.

// Empty string specifies no filter is to be applied.
String sparqlFilter = "";

int maxNumRecords = 10;

List<EntityWithUri> searchResults = sparqlService.searchForSubject(
        sparqlProvider, 
        searchText, 
        searchLanguage,
        sparqlFilter, 
        maxNumRecords
    );

EntityWithUri delythEvans = searchResults[0];
```
The `delythEvans` variable will contains an entity with URI representing [Delyth Evans](http://www.wikidata.org/entity/Q5254977).

### Applying Filters
Filters can be applied to the searches to enable users to better find entities.

```java
ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);

ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider("WikiData");
        .orElseThrow(() -> new SparqlProviderNotFoundException("WikiData")); 

String searchText = "Douglas Adams";
String searchLanguage = "en"; // The language the search text is in and the entity label should be specified in.

// The *custom filter*
// This sparqlFilter specifies that the subject's occupation is Author.
String sparqlFilter = "?subject wdt:P106 wd:Q6625963";

int maxNumRecords = 10;

List<EntityWithUri> searchResults = sparqlService.searchForSubject(
        sparqlProvider, 
        searchText, 
        searchLanguage,
        sparqlFilter, 
        maxNumRecords
    );

EntityWithUri douglasAdams = searchResults[0];
```
The `douglasAdams` variable will contains an entity with URI representing [Douglas Adams](http://www.wikidata.org/entity/Q42).

#### Using Suggested Filters
The `ISparqlProvider.getUserSubjectFilters` method provides a simple list of suggested filters which the user may select form to narrow their search. 

```java
ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider("WikiData");
        .orElseThrow(() -> new SparqlProviderNotFoundException("WikiData")); 

List<UserSubjectEntityFilter> suggestedFilters = sparqlProvider.getUserSubjectFilters();

// Not all providers provide anything beyond the standard 'Any' filter.
if (suggestedFilters.length > 1) {
    UserSubjectEntityFilter filterOne = suggestedFilters[1];
    
    String filterDisplayName = filterOne.getDisplayName();

    // This sparqlFilter value can be used to filter search queries.
    String sparqlFilter = filterOne.getSparqlStatement();
}

```

## Fetching an Entity's Label
An entity's URI can be mapped to an `EntityWithUri` object containing the entity's label using the `ISparqlService.getEntityForUri` method.

```java
ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);

ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider("WikiData");
        .orElseThrow(() -> new SparqlProviderNotFoundException("WikiData")); 

URI entityUri = URI.create("http://www.wikidata.org/entity/Q42");
String displayLanguage = "en"; // The language the entity label should be provided in.

try {
    EntityWithUri entityWithUri = sparqlService.getEntityForUri(
            sparqlProvider,
            entityUri,
            displayLanguage
        );

    String displayText = EntityHelpers.getDisplayTextForEntityWithUri(entityWithUri);

} catch (EntityNotFoundException ex) {
    // Could not find the entity.
} catch (TooManyValuesFoundException ex) {
    // Too many records or labels found for the entity.
}

```
The `displayText` variable will contain the value `Douglas Adams (http://www.wikidata.org/entity/Q42)`.

## Searching for Properties
The `ISparqlService.searchPropertiesForEntity` function can be used to search for the properties which are related to a given entity. This is primarily to allow the user to see and select the desired property from the available options.

```java
ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);

ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider("WikiData");
        .orElseThrow(() -> new SparqlProviderNotFoundException("WikiData")); 

URI subjectUri = URI.create("http://www.wikidata.org/entity/Q42"); // Entity that the predicate is about.

String propertyPredicate = "height"; // Search term to filter on. May be empty.
String predicateLanguage = "en"; // The language that the propertyPredicate is provided in.

String outputLanguage = "en"; // The language that the property's label will be displayed in.
int maxNumRecords = 10;

List<EntityWithUri> properties = sparqlService.searchPropertiesForEntity(
        sparqlDataProvider, 
        subjectUri,
        propertyPredicate, 
        predicateLanguage,
        outputLanguage, 
        maxNumRecords
    );
```
The `properties` variable will contain `EntityWithUri` objects relating to specific properties which the `subjectUri` has.

## Fetching a Property Value
To fetch the value held by an entity's property, the `ISparqlService.getPropertyValueForEntity` method can be used.

```java
ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);

ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider("WikiData");
        .orElseThrow(() -> new SparqlProviderNotFoundException("WikiData")); 

URI subjectUri = URI.create("http://www.wikidata.org/entity/Q42"); // Entity that the predicate is about.

String propertyPredicate = "height"; // Search term to filter on. Must be an exact match.
// Alternatively `propertyPredicate` can specify a property URI, e.g. `http://www.wikidata.org/prop/direct/P2048`

String predicateLanguage = "en"; // The language that the propertyPredicate is provided in.

String outputLanguage = "en"; // The language that the value will be displayed in.

boolean allowMultipleValues = false; // Set this to true to allow one-to-many results to be returned.
// If allowMultipleValues = true, the values are concatenated into a CSV string.

try {
    EntityProperty property = sparqlService.getPropertyValueForEntity(
            sparqlDataProvider, 
            subjectUri,
            propertyPredicate, 
            predicateLanguage,
            outputLanguage, 
            allowMultipleValues
        );

    if (property.getPropertyIsDate()) {
        XMLGregorianCalendar date = property.getDate();
        //...
    } else if (property.getPropertyIsEntity()) {
        EntityWithUri entityWithUri = property.getEntityWithUri();
        //...
    } else {
        // Object has a literal value.
        Object value = property.getValue();
        // Could be Double/String.
        String stringValue = value.toString();
        //...
    }
    
} catch (TooManyValuesFoundException ex) {
    // allowMultipleValues = false and we found that there were multiple values returned.
} catch (ValueNotFoundException ex) {
    // Could not find the `propertyPredicate` associated with the `subjectUri` 
}
```

## Mapping URIs to *ISparqlProvider*s
There may be situations where the developer has access to an entityURI but does not know which is the best *ISparqlProvider* to use to access related information. For this reason, the concept of a SPARQL provider [*owning*](#owning-a-uri) a URI enabled a reasonable decision to be made without too much effort. 

The `Mwnci.getOwningProviderForUri` method can be used to find the owning *ISparqlProvider* for a given URI. It should be noted that the method may not find a matching *ISparqlProvider* and so an `Optional<ISparqlProvider>` is returned.

```java
URI entityUri = URI.create("http://www.wikidata.org/entity/Q42");

Optional<ISparqlProvider> maybeProvider = Mwnci.getOwningProviderForUri(entityUri);

if (maybeProvider.isPresent()) {
    ISparqlProvider sparqlProvider = maybeProvider.get();
} else {
    // Couldn't find a SPARQL provider which owns the `entityUri`.
}
```

## Adding a new Data-Source
A new data-source can be added in an adhoc fashion by relying on the [*StandardSparqlProvider*](#the-standardsparqlprovider). This can often provide a quick and easy one-size-fits-all implementation which may work with the desired data-source. However, on occasion there will be semantic differences between the SPARQL data-source and the SPARQL generated by the *StandardSparqlProvider* which can mean that data cannot be extracted by the tool. In addition, there may be performance issues where the *StandardSparqlProvider* generates SPARQL which is not quick enough for a particular end-point. In both of these cases it is advisable to create a customised *ISparqlProvider* implementation which is specially designed to target the desired SPARQL end-point.

Implementing the *ISparqlProvider* interface is generally a case of extending the *StandardSparqlProvider* class and overriding methods where performance or semantic improvements can be applied.

An example of a semantic improvement on top of the *StandardSparqlProvider* can be seen in `cymru.mwnci.sparql.services.providers.EuropeanaProvider`. This provider uses the *Europenana* SPARQL end-point which makes use of the `dc:title` predicate rather than the standard `rdfs:label` when labelling an entity.
```java
@Override
public GraphPattern linkEntityToLabel(RdfSubject subject, Variable subjectLabel) {
    return subject.has(NS.DC.iri("title"), subjectLabel);
}
```

An example of a performance improvement over the *StandardSparqlProvider* can be seen in `cymru.mwnci.sparql.services.providers.WikiDataProvider` which overrides the `buildEntitySearchQuery` method to make use of the [mwapi](https://www.mediawiki.org/wiki/Wikidata_Query_Service/User_Manual/MWAPI) SPARQL extensions for more performant (and ordered) entity searching.


Once a new *ISparqlProvider* implementation has been created, it should be registered in the `getSparqlProviders()` static method in `cymru.mwnci.sparql.Mwnci`.

#### Owning a URI
An *ISparqlProvider* can *own* a URI. This means that the URI's primary source of data is provided by this SPARQL end-point and so allows the Mwnci extension to preferentially pull related data from the associated provider without having to guess which end-point might be the best fit.

We see that the Europeana provider owns the URIs with domains ending  `data.europeana.eu`:

```java
@Override
public boolean ownsUri(URI entityUri) {
    return entityUri.getHost()
            .toLowerCase()
            .endsWith("data.europeana.eu");
}
```

## *ISparqlService* vs *ISparqlProvider*
> Should code live in *ISparqlService* or be delegated to *ISparqlProvider*?

A short summary of the benefits of placing code in the *ISparqlService* versus *ISparqlProvider* can be seen below.

|Centralisation in *ISparqlService*|Delegation to *ISparqlProvider*s|
|:-|:-|
|For SPARQL generation where there are minimal or no semantic differences between SPARQL end-points or when tailoring the query for endpoint-specific performance is not an important consideration.|Used when semantics frequently differ between SPARQL end-points or when performance is closely related to the generated SPARQL graph pattern which must be tailored to specific end-points.|
|Can improve maintainability of code by reducing duplication.||
|Queries are consistently generated and so easier to reason about when switching between *ISparqlProvider* implementations.||
|Makes it harder for a developer of an *ISparqlProvider* to significantly break any implicit parts of the interface's contract.||

# Use of IOC
This project makes use of the [Guice](https://github.com/google/guice) inversion of control (IoC) framework. Use of IoC pushes the application towards the modular encapsulation of functionality in classes serving a singular purpose and the building of complex services in a compositional fashion from smaller modules. This architecture is designed to ensure that code is maintainable in the long term by reducing coupling and enabling components to be individually re-implemented where upgrades are required. Use of the *Guice* IoC framework also simplifies the lifetime-management of service instances, for instance by ensuring the singleton behaviour of each of the *ISparqlProvider* implementations.

Whilst there is currently limited benefit to the use of IoC in this small project, it has been implemented at this early stage to promote the development of [SOLID](https://en.wikipedia.org/wiki/SOLID) code through the lifetime of the project's development.

# Unit Testing
Most providers have their own unit tests found in `lib/src/test` which are used to test that the *ISparqlProvider* can be used to generate the necessary SPARQL to fulfill the basic functionality required. More complex testing of the *ISparqlService* is currently undertaken by the [unit/integration tests](./OfficeInterface.md#unit-testing) written for the [Office Interface](./OfficeInterface.md) project. This architecture avoids duplication of tests and saves time but does mean that any change to the *SPARQL Engine* project cannot be fully-tested in isolation. 

# Local SPARQL Test Repository
When [unit testing](#unit-testing), it is not particularly suitable to test against a live SPARQL end-point over which the Mwnci project has no control. It would be very easy for a particular end-point to change their schema or to delete or manipulate the data which is used for testing. It can also be difficult to find graph patterns which contain the behaviour which we want to test. For these reasons, a local SPARQL test repository is available when running in debug mode with the `MWNCI_DEBUG` environmental variable set to `true` and the `LOCAL_SPARQL_TEST_REPO` environmental variable pointing to the correct location of the `lib/localRepoData` folder. The local test repository will then be displayed as a SPARQL provider in the extension's user interface.

The `lib/localRepoData` contains files in turtle syntax containing the data which is loaded in to the local test repository. The corresponding *ISparqlProvider* can be found in `cymru.mwnci.lib.services.providers.LocalTestRepo`

