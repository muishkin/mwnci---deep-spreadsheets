# Selecting an Entity

Project Mwnci allows the user to assign an individual entity to a cell. This process is aided by the `Entity Search` sidebar panel included as part of the extension.

## What is an Entity Cell?
Entity cells are any cells which contain a single RDF entity URI. These are typically displayed in the `Entity Label (Entity URI)` form as shown in the following figure:

![A typical example of an entity cell with the `Entity Label (Entity URI)` form. This cell references 'cheese (https://www.wikidata.org/entity/Q10943)' on the WikiData data-source.](./img/EntityCell.png)

Other examples of entity cells could be cells including only the entity URI, for example:
> `https://www.wikidata.org/entity/Q10943` 

or a cell containing surrounding text, e.g. 
> `The Washington I refer to is of course http://www.wikidata.org/entity/Q594697 which can be understood further by accessing related information on WikiData`.

 Both of the preceeding are examples of cells which are interpreted as entity cells by the Mwnci extension.

## Using the Entity Search Side-bar Panel
The entity search side-bar panel is a tool which aides in linking a specific entity to a cell. The panel should automatically display the first time you load a spreadsheet after installation of the Mwnci extension. After first use, it can be shown and hidden by clicking the Mwnci side-bar icon. ![The Mwnci side-bar icon.](./img/MwnciSidebarIcon.png)

Upon opening a spreadsheet and (possibly) activating the Mwnci side-bar panel, the user should be presented with a view similar to that shown below:

![The view of an empty spreadsheet with the Mwnci entity search side-bar panel visible..](./img/EntitySearch_Empty.png)

### Choosing a Data-Source
Chosing a data-source is the first step towards selecting an entity. The data-source provides a limited set of data from which it will be possible to find the desired entity. Different data-sources contain information about different entities, but there can be some overlap where a source references an entity which is owned by a different data-source.

Mwnci requires that all data-sources support the [SPARQL 1.1 Standard](https://www.w3.org/TR/sparql11-query/). Provided with Mwnci is a small selection of SPARQL data-sources which have typically been optimised for use with Mwnci. The user's choice of data-source can be made using the `Source` combobox:

![Selection of a default data-source.](./img/EntitySearch_Sources.png)

Users may override the default selections by inserting the URL of a known sparql endpoint. It should be noted that not all SPARQL 1.1 datasources will automatically work with Mwnci and that some programmatic customisations may be required. An example using the `http://sparql.cancerdata.org/namespace/undefined/sparql` endpoint can be seen below:

![Providing a custom SPARQL data-source using the http://sparql.cancerdata.org/namespace/undefined/sparql end-point.](./img/EntitySearch_CustomSource.png)

### Selecting a Filter
Many SPARQL data-sources contain large repositories of data within which it can be hard to find the user's desired entity. To provide a simple solution to thsi problem, the filter box enables developers to specify filters which restrict the entities which are presented to the user. For example, the WikiData provider allows the user to choose between a small range of possible filters: 

![Filters](./img/EntitySearch_Filters.png)

In addition to these pre-approved filters, it is possible for the advanced user to provider a custom filter by presenting a custom SPARQL query statement. For instance when using the WikiData source, if the user inputs `?subject wdt:P31/wdt:P279* wd:Q5113` in to the filter combobox, they will see entities where the subject is (or is a sub-class of) a bird.

![Custom filter applied to the `WikiData` source using the `?subject wdt:P31/wdt:P279* wd:Q5113` custom filter to return entities which are (or are a sub-class of) 'bird'](./img/EntitySearch_CustomFilter.png)


### Searching by Text
In addition to filtering by pre-defined categories, a free-form text search is available. It should be noted that due to the limitations of the SPARQL standard, free-text searches may be very slow and return inappropriately ordered results. 

### Choosing an Entity
Once the appropriate search parameters have been set, the panel will automatically return a list of matching results. The result list presents the selection to the user by displaying the entity label, a description in order to aide disambiguation of entities with the same name, and the identifying URI. Once the user has found the particular entity required, they can insert it in to the currently selected cell by selecting the option with their mouse.

![The view when selecting the 'cheese' entity from WikiData with no filter applied.](./img/EntitySearch_Selected.png)

### The Formula
When using the entity search panel, the formula produced can be quite complicated. This is due to the fact that the search parameters are stored on JSON format inside the cell's formula to aide the user in refining the entity later in the spreadsheet development process. These JSON parameters will likely be deprecated after future development work has progressed.

![Full Formula containing JSON formatted search parameters.](./img/EntitySearch_Formula.png)

The JSON formatted parameters may be removed to leave the user with a cleaner and more understandable formula which can be amended and used in any way that a typical spreadsheet formula may be used:

![Simple Formula](./img/EntitySearch_CleanFormula.png)

Where the entity URI is owned by a built-in Mwnci data-source, it is possible to simplify the formula further to `=sparqlEntity("http://www.wikidata.org/entity/Q10943")`.

This formula forms the vital backbone allowing Mwnci to operate in the spreadsheet while allowing the user to continue to use traditional spreadsheet operations such as fill-down/fill-across. 

### Extracting the URI From a Cell
Where there is uncertainty as to whether the Mwnci tooling has extracted the URI correctly from a cell, it is possible to check which URI is matched by using the `sparqlEntityUri` function. e.g.
> `= sparqlEntityUri(A1)`

or
> `= sparqlEntityUri("http://www.wikidata.org/entity/Q10943 --- and another thing: http://www.wikidata.org/entity/Q594697")`

Will return the entity URI used for all Mwnci operations.
