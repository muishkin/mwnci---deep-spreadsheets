# Overview
Project Mwnci is a Java extension developed for Open/LibreOffice which provides access to live SPARQL linked-data sources through the use of custom-developed spreadsheet functions. The project is split in to two components, the *SPARQL Engine* and the *Office Interface*. These components are built and combined together using the [Gradle](https://gradle.org/)-driven [build system](./BuildSystem.md).

## SPARQL Engine
The [SPARQL Engine](./SparqlEngine.md) is a standalone Java library which enables an application to perform the following queries against a SPARQL data-source:

* A free-text search for entities by name.
* The look-up of an entity's label and description.
* The listing of properties relating to an entity.
* The lookup of the value of an entity's property.

The engine allows the above four queries to be performed against a range of built-in SPARQL end-points as well as attemping to query previously unknown SPARQL end-points. Queries are customised for performance and semantic configuration when targeting built-in SPARQL end-points to provide a satisfactory user experience. All queries use the end-user's current locale to decide on the appropriate language to use for search and display of labels and values.

The *SPARQL engine* project relies on [RDF4J](https://wiki.openoffice.org/wiki/JavaEclipseTuto#Components_creation_process) to handle serialisation/de-serialisation and communication with different SPARQL end-points.

## Office Interface
The [Office Interface](./OfficeInterface.md) is an Open/LibreOffice extension developed in Java which provides the necessary integrations to enable a spreadsheet user to access the data provided by the [SPARQL Engine](./SparqlEngine.md). The tools provided to bridge this gap as *Calc functions* to enable cells to reference data and a custom *sidebar* to help users find the data which needs referencing.

A good starting point for understanding how Libre/OpenOffice extensions are developed in Java can be found [here](https://wiki.openoffice.org/wiki/JavaEclipseTuto) on the OpenOffice wiki. Beware as documentation can often be out-of-date, incomplete or occasionally misleading.
