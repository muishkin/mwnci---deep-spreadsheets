# Overview
The *office interface* provides the functionality necessary to add the Mwnci tooling to Open/LibreOffice. It integrates with the office environment to add the Calc functions and graphical user interface (GUI) necessary to enable the user to interact with SPARQL data-sources. It should be noted that it does not itself interact with SPARQL data-sources but instead delegates all of the functionality to the [SPARQL Engine](./SparqlEngine.md). 

# IDL Files
The development of Open/LibreOffice extensions requires communication between the office application and the extension's run-time. Information must be passed in both directions and so interfaces must be specified to support this communication. In Open/LibreOffice the [UNO](https://wiki.openoffice.org/wiki/Uno/Article/Understanding_Uno) system is used to facilitate inter-process communication between office components and extensions written in different languages and executing in different processes. 

*IDL* files are the interface definitions for the UNO system which specify how communication may proceed between processes and languages. The IDL files are used to [generate language-specific interfaces](https://wiki.openoffice.org/wiki/JavaEclipseTuto#Components_creation_process) which can be used at compile-time to check that the communication contract is being correctly enforced. 

This project defines all Calc add-in functions in `interface/idl/cymru/mwnci/officeinterface/XMwnciInterface.idl` in order that they can be correctly executed in spreadsheets.

# Calc Functions
Calc functions are used as the primary vehicle for bringing SPARQL data in to the user's spreadsheet document. 

## Why Calc Functions?
The primary reason to use Calc functions is that they provide a convenient and standardised way to provide cells with data-values in a fashion that allows the user to use all of the standard spreadsheet functionality without difficulty. 

e.g. A user may wish to use fill-across/fill-down functionality to populate cells with SPARQL data in a fashion which is idiomatic to spreadsheets. Use of Calc functions enables Mwnci to easily handle this requirement.

## Asynchronous Function Results with XVolatileResult
Calls to SPARQL end-points can take significant amounts of time. In order to avoid blocking the loading of a spreadsheet while these occur, the Mwnci implementation of an [XVolatileResult](https://wiki.openoffice.org/wiki/Documentation/DevGuide/Spreadsheets/Variable_Results) is returned by all Mwnci Calc functions which make remote requests. The *XVolatileResult* interface allows the quick return of a value upon execution of the function and allows us to update the value once the background request has completed. When the *XVolatileResult*'s value is updated, the cells which are dependent on the updated cell's values are automatically re-evaluated by Calc.

This functionality may be used in the future to periodically update SPARQL values in a periodic fashion or upon user request.

### Caching
The Mwnci office interfaces caches all *XVolatileResult*s to reduce the number of requests made to remote end-points and to improve the speed of response where the user has previous referenced a property of entity. The code responsible for this can be seen inside the `cymru.mwnci.officeinterface.models.MwnciVolatileResult.getVolatileResult` static function. The cache is based on a unique string key which is formed by the combination of the input parameters to functions which call `getVolatileResult`. 

## General Exception Handling
All Calc functions throw the `com.sun.star.lang.WrappedTargetException` in order to handle any exceptions which the extension is unable to successfully handle. Exceptions which it is possible to handle typically result in an error message being written to the relevant cell to aide the user.


## Current Calc Functions
The definition of all current Mwnci Calc functions can be found in `interface/idl/cymru/mwnci/officeinterface/XMwnciInterface.idl` and the implementation of these can be found in `cymru.mwnci.officeinterface.MwnciInterfaceImpl`.

### SparqlEntityUri

```idl
string sparqlEntityUri(
    [in] string cellText
) raises (com::sun::star::lang::WrappedTargetException);
```

The *SparqlEntityUri* function takes a cell containing text and returns the matching URI used by all of the Mwnci tooling. It allows the user to verify that the correct URI is being extracted from a given cell and enables them to understand how the tooling copes with multiple URIs in the same cell. If there are multiple URIs in the same cell then the first matched URI is the one taken forwards. 

![Example of extracting an Entity URI from a cell using the *sparqlEntityUri* Calc function.](./img/SparqlEntityUri_Function.png)

### SparqlEntity
```idl
com::sun::star::sheet::XVolatileResult sparqlEntity(
    [in] string entityUri,
    [in] any maybeSparqlProvider,
    [in] any maybeSearchJson
) raises (com::sun::star::lang::WrappedTargetException);
```

The *SparqlEntity* function takes in an `entityUri`, an optional `maybeSparqlProvider` and an optional `maybeSearchJson`.

Its primary purpose is to take a URI representing an entity and return a human-readable label combined with . It achieves this by using the `maybeSparqlProvider` if specified, else it attempts to find a SPARQL Provider (*ISparqlProvider*) which [owns](./SparqlEngine.md#owning-a-uri) the URI. If neither of these conditions are matched then an error is returned.

The `maybeSearchJson` parameter is an optional parameter reserved for use with the *entity search panel*. If occupied, it holds the JSON parameters used in the search process. Note that since this property does not get updated when cells are moved or full-across/fill-down functionality is used this can become out of sync with the actual value displayed in the cell. This parameter is therefore a temporary measure and should not be relied upon in the long term of the project.

![An example of an Entity Cell using the `sparqlEntity` function.](./img/EntitySearch_SparqlProviderFound.png)

![An example of an Entity Cell using the `sparqlEntity` function. A suitable SPARQL provider cannot be found for this entity and none has been specified by the user.](./img/EntitySearch_NoSparqlProviderFound.png)

#### Entity Cell Format
All entity cells generated by Mwnci use the `Entity Label (Entity URI)` format. This textual representation was chosen as it enables the user to easily recognise that the cell contains a SPARQL entity at the same time as being aware of what the entity represents. This format also allows Mwnci functionality to extract the specific entity URI from the cell's text so that the user can extract data about the entity held by the cell.

Experiments were performed with passing [XCellRange](https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1table_1_1XCellRange.html)s as arguments to functions which enabled the URI to be hidden in the formula of the cell. It was decided against proceeding with this way of representing entities as it stopped users from being able to pass raw strings in to Mwnci Calc functions. This unnecessary restriction of the spreadsheet paradigm would likely cause frustration and confuse users and went against the project's aim to maximise utility by leveraging off the spreadsheet paradigm.

#### Handled Exceptions
The following exceptions are handled and return error messages to the user in the containing cell:

|Exception|Occurs|
|:-|:-|
|`cymru.mwnci.sparql.exceptions.SparqlProviderNotFoundException`|Where the relevant SPARQL provider cannot be found or created.|
|`cymru.mwnci.sparql.exceptions.EntityNotFoundException`|Where the given entity does not exist on the given SPARQL provider or else we can't find a label for it.|
|`cymru.mwnci.sparql.exceptions.TooManyValuesFoundException`|Multiple values exist for the entity or the label.|

### SparqlValue
```idl
com::sun::star::sheet::XVolatileResult sparqlValue(
    [in] string entityUri,
    [in] string sparqlProvider,
    [in] string propertyPredicate,
    [in] any maybeAllowManyValues,
    [in] any maybePredicateLanguage,
    [in] any maybeSearchJson
) raises (com::sun::star::lang::WrappedTargetException);
```

The *SparqlValue* function takes an entity, a SPARQL provider and a propertyPredicate as required inputs. It attempts to find the predicate which is linked to the entity on the given SPARQL provider. 

![Example of use of fill-down functionality to populate the mass & parent for a number of planetary bodies. The initial values for earth were manually selected and then fill-down was applied to all the cells below.](./img/ValueSearch_FillDownExample.png)

#### Parameters

The `entityUri` parameter enables the user to specify the URI or cell containing the URI of the entity which we should bring back properties about.

The `sparqlProvider` parameters enables the user to specify the SPARQL provider against which to execute this query. It accepts either the name of the provider (e.g. `WikiData`) or the direct URI of a SPARQL end-point.

The `propertyPredicate` parameter allows the user to specify *either* the URI of a given property *or* to specify the full name of the property. If the full name is provided then this parameter is considered in conjunction with `maybePredicateLanguage`. The property is returned for the entity specified by `entityUri`. An example of a full name could be to search for "Population" in language "en" against entityUri "http://www.wikidata.org/entity/Q48" (London).

The `maybeAllowManyValues` parameters allows the user to optionally override the default behaviour which returns an error when multiple matching properties are found. If this is set to `1` or `TRUE()` then multiple values will be returned in a comma-separated format.

The `maybePredicateLanguage` parameter allows the user to optionally specify the language under which the `propertyPredicate` property name matching is performed. This is to enable consistent property name matching when opened by users who's system locales specifiy different languages. If not specified it defaults to the users current locale language. 

The `maybeSearchJson` parameter is an optional parameter reserved for use with the *entity search panel*. If occupied, it holds the JSON parameters used in the search process. Note that since this property does not get updated when cells are moved or full-across/fill-down functionality is used this can become out of sync with the actual value displayed in the cell. This parameter is therefore a temporary measure and should not be relied upon in the long term of the project.

#### Handling One to Many Relationships
> *Literal values* are raw values such as the number `3` or the string `Hello, world`. *Entities* are object which have an identifying *URI*. The *sparqlValue* function may return *literal values* or *entities*.

![An application of *sparqlValue* where there are too many entities present.](./img/ValueSearch_TooManyEntities.png)

In the case where an entity, *A* is related to values *B* and *C* by the same property *P*, the *sparqlValue* function must make a decision about which value, if any, to return to the user. It should be noted that *B* and *C* can represent either *literal values* or represent other *entities*. Whilst this section considers the example of a one-to-two relationship, any higher arity is covered by the general principles discussed here, i.e. it applies to all one-to-N relationships where N is greater than one.

In the case where *B* and *C* represent entities, to display them to the user we would have to put them in the same cell in some format. The second entity could easily be missed by the user, especially considering that any further application of the Mwnci functionality would simply assume that the first entity in the cell is the one the user is choosing to reference. Given that there is no consistent ordering applied to the values *B* and *C*, placing multiple entities in the same cell could result in non-deterministic behaviour depending on the order that *B* and *C* are returned by the SPARQL provider. It is therefore a poor choice to return multiple entities by default. 

In the case where *B* and *C* are literal values, placing multiple values in the same column may easily be ignored by the user and could have confusing and ill-defined behaviour on any analysis performed by the user. It is also possible that multiple string values delivered in a comma-separated style could be mis-interpreted as a single value.

For these reasons Mwnci assumes that the user only wants to place one value in a given cell and will automatically display an error where multiple values are returned. However, for debugging and advanced use, the user is able to manually override this behaviour by providing a value of `1` or `TRUE()` to the `maybeAllowManyValues` parameter. With this parameter set, the function will return a comma-seperated list of the values which are available.

![An application of *sparqlValue* where one-to-many relationships are displayed.](./img/ValueSearch_AllowMultipleValues.png)


One might ask why these Calc functions do not permit the return of multiple values using the `Ctrl-Shift-Enter` activated [Array function](https://help.libreoffice.org/Calc/Array_Functions) return type. The reason for this is that the user does not know whether or not multiple values will be returned by the function and so must pre-emptively execute all formulas using the `Ctrl-Shift-Enter` functionality in order to ensure multiple values are outputted where provided. It also appears to be the behaviour of Calc to simply ignore multiple values where `Ctrl-Shift-Enter` has not been pressed. This means that if the user doesn't press `Ctrl-Shift-Enter`, there is no way for the function to realise this and warn the user that there are other missing values. This functionality has been tackled by the new [dynamic array](https://support.office.com/en-us/article/Dynamic-arrays-and-spilled-array-behavior-205c6b06-03ba-4151-89a1-87a7eb36e531) functionality added to Excel by Microsoft which is currently missing from Open/LibreOffice Calc.

#### Handled Exceptions
The following exceptions are handled and return error messages to the user in the containing cell:

|Exception|Occurs|
|:-|:-|
|`cymru.mwnci.sparql.exceptions.SparqlProviderNotFoundException`|Where the relevant SPARQL provider cannot be found or created.|
|`cymru.mwnci.sparql.exceptions.ValueNotFoundException`|Where the requested property cannot be found linked to the entity on the SPARQL end-point.|
|`cymru.mwnci.sparql.exceptions.TooManyValuesFoundException`|Multiple values exist for the entity or the label.|

## Calc Functions - Important Files
Where adding or modifying the definition of any Calc functions there are three places which must be altered:

|File|Purpose|
|:-|:-|
|interface/idl/cymru/mwnci/officeinterface/**XMwnciInterface.idl**|Contains the Open/LibreOffice language-independent IDL definitions which are required by the [UNO](https://wiki.openoffice.org/wiki/Uno) interopability system. This system handles all communication between the Office application and any extensions.|
|interface/source/cymru/mwnci/officeinterface/**MwnciInterfaceImpl.java**|Implements all of the function definitions specified in **XMwnciInterface.idl**.|
|interface/resources/**CalcAddIn.xcu**|This file defines which IDL functions are to be used as functions inside Calc. It also provides function and argument definitions which are visible to the user.|

If one of these files does not contain the most up-to-date definition of your function and its arguments then your function may not work or may provide misleading information to the end-user.

## Known Calc Function Bugs

### Programmatically Referencing Calc Add-In Functions in Formulae
[This bug report](https://bugs.documentfoundation.org/show_bug.cgi?id=54854) explains that Calc functions which are add-ins provided by an extension are often not evaluated correctly when a formula is programmatically inserted in to a cell. The work-around used in this project is to reference the Calc function name using the full-qualified name of the class that the function is implemeted on, in this case `=cymru.mwnci.officeinterface.MwnciInterfaceImpl.{function_name}(...)`.

### Programmatically Delimiting Calc Function Parameters
[This bug report](https://bugs.documentfoundation.org/show_bug.cgi?id=78929) explains that when a formula is programmatically inserted in to a cell, the locale-configured parameter delimiter (commonly `,`) is not correctly understood by the application and results in an error. The work-around used in this project is to use the `;` character to delimit all arguments in Calc functions. e.g. inserting `=power(2,3)` in to a cell would result in a cell error, however `=power(2;3)` is always correctly interpreted by the application.

# Graphical User Interface

## Repurposing the XDG Dialog System
The Open/LibreOffice extension development tooling only provides one way to visually design and develop *Graphical User Interfaces* (GUIs) which is the [XDG Dialog System](https://wiki.openoffice.org/wiki/Documentation/BASIC_Guide/Working_With_Dialogs) which was primarily designed to allow user of [OOBasic](https://wiki.openoffice.org/wiki/Documentation/BASIC_Guide) to create and launch dialogs without venturing outside of the Open/LibreOffice environment. The tooling allows the developer to place and name components visually on a dialog and then connect them up to actions using [protocol handler](https://wiki.openoffice.org/wiki/Documentation/DevGuide/WritingUNO/Protocol_Handler) functionality. It also provides functionality to aide in internationalisation of the GUI.

This project adapts the XDG tooling so that it can be used to populate both dialogs and panels. It does this by generating a dialog and then moving each control to the desired panel where it should be displayed. The system shuns the limiting protocol handler system in favour of a custom [RxJava](https://github.com/ReactiveX/RxJava)-based controller system for user interactivity.

Dialogs can be launched by calling the `cymru.mwnci.officeinterface.MwnciDialog.loadDialog()` static function. Panels can be initialised using the Open/LibreOffice [XUIElementFactory](https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1ui_1_1XUIElementFactory.html) functionality via `cymru.mwnci.officeinterface.factories.MwnciPanelFactory.createUIElement()`. 

### Controller Architecture
When dialogs or panels are created, an instance of `cymru.mwnci.officeinterface.controllers.IDialogController` or `cymru.mwnci.officeinterface.controllers.IController` must be respectively provided. All of the controls imported from the XDG dialog file are registered with the *IController*. Further, each control has a range of event listeners attached which are programmed to call the `IController.handleEvent` function when an event occurs. These two bits of functionality allow the controller to get and set values on the UI and also allow it to respond to user driven events such as button clicks and text entry. The *IDialogController* interface extends *IController* and adds functionality which allows interaction with the dialog primarily to allow it to close itself.

#### Reactive Controllers
The `cymru.mwnci.officeinterface.controllers.ReactiveBaseController` class provides the primary imeplementation of the *IController* interface. It uses the [RxJava](https://github.com/ReactiveX/RxJava) framework to allow for the creation of controllers which follow the *reactive* style of programming where code is executed in response to its dependencies changing. 

##### ReactiveBaseController
The `ReactiveBaseController` enables the user to subscribe to *Observable*s that track events which are triggered by user interaction with the application. The controller allows the developer to track events that happen to controls in the panel via the `onControlEvent` function and perform other basic actions such as clearing [ListBox](https://api.libreoffice.org/docs/idl/ref/servicecom_1_1sun_1_1star_1_1form_1_1control_1_1ListBox.html)es and [ComboBox](https://api.libreoffice.org/docs/idl/ref/servicecom_1_1sun_1_1star_1_1form_1_1component_1_1ComboBox.html)es. It satisfies the basic *IController* contract and allows derived classes to build on the basic functionality. The controller also has basic error handling functionality which arranges for a user-friendly error dialog to be launched.

##### ReactiveSpreadsheetBaseController
The `ReactiveSpreadsheetBaseController` extends `ReactiveBaseController` and adds functionality which is useful inside spreadsheet documents including the ability to track the current spreadsheet document which is open, the current sheet which is visible and the currently selected cell. This class makes extensive use of the Java [Optional](https://docs.oracle.com/javase/8/docs/api/java/util/Optional.html) functionality which enables the safe propagation and handling of cases where variables are null or unset. 

The class also attempts to keep track of whether the panel is currently visible or not so that SPARQL queries and expensive computation can be held until the panel is visible once more. This is made accessible through the `getPanelIsVisible()` function.

In addition, this class gives access to an instance of [XFunctionAccess](https://api.libreoffice.org/docs/idl/ref/interfacecom_1_1sun_1_1star_1_1sheet_1_1XFunctionAccess.html) to enable derived classes to execute Calc functions. This is used in the `SparqlValueSearchController.getHumanReadableCellRangeFromCellAddress` method to map between the [CellAddress](https://api.libreoffice.org/docs/idl/ref/structcom_1_1sun_1_1star_1_1table_1_1CellAddress.html) and *A1*-style notation for referencing cells.

## The Sidebar
Mwnci creates sidebar panels in order to aide users in the search for entities and related properties. Given the complexity and wide ranging scale of SPARQL data-sources such as [WikiData](https://www.wikidata.org), it is a necessity to allow users to explore the available data with the minimal effort required and without the strain of switching between applications or windows. For this reason two panels have been designed and created.

### Entity Search Panel
The panel was designed and developed to help the user search for and select entities from SPARQL sources. Once the search is complete, the current selected cell is populated with a formula which specifies which entity the cell references.

![The *entity search panel* which allows users to insert a particular entity in to a cell in the spreadsheet.](./img/EntitySearch_Empty.png)

The panel allows the user to select a SPARQL query source from those provided by `cymru.mwnci.sparql.Mwnci.getSparqlProviders`, select a filter provided by the corresponding *ISparqlProvider* and then apply a free-text search. Once the search results have been returned the user is free to select the entity which should be contained in a given cell.

The panel functionality was specially designed to make use of the combobox's free-text input functionality to allow the input of custom SPARQL end-points as well as [custom filters](./SparqlEngine.md#custom-filters). These values are passed to the SPARQL engine for [*ISparqlProvider* matching](./SparqlEngine.md#isparqlprovider-interface).

![The *entity search panel* being used with a custom SPARQL end-point which is not built-in to Mwnci.](./img/EntitySearch_CustomSource.png)

![The *entity search panel* being used with a custom filter which is not built in to the *ISparqlProvider* for WikiData.](./img/EntitySearch_CustomFilter.png)

#### Storing Search Parameters
Once a non-default search has been initiated, the search parameters are serialised to a JSON format and stored in the cell's formula. This allows the user to switch between cells and then to return to a previous search and complete or amend it. Once an entity has been selected, the cell is also populated with a call to the [sparqlEntity](#sparqlentity) function which ensures that the entity label and URI are placed prominently in the cell.

One of the drawbacks of storing the search parameters in JSON in the desired cell is that typical spreadsheet operations such as inserting/deleting rows, using fill-down/fill-across and copying and pasting cells can result in search parameters which reference different cells than the initial search actually used. Standard Calc function parameters are often updated by Calc when cell operations are performed, however this does not extend to the custom JSON parameters used here. 

##### Future Work
Future work should focus on obviating the need to store JSON search parameters and instead attempt to leverage the arguments maintained inside the calls to the Mwnci Calc functions. This approach would add complexity due to the need to cope with formulae inside arguments. For example `=sparqlEntity(A1 & "Q42", "WikiData")` may be the contents of a particular entity cell. While the `A1 & "Q42"` value is syntactically correct as an argument, it is not straight forward for the extension to get the resulting string value from this statement. A cursory web search has not shown a satisfactory solution to this problem.

#### Customisable Search Interface
Initial project plans called for search functionality which could be switched based upon the *ISparqlProvider* selected in order to aide users in filtering and finding the entities that they required. Allowing customised filter interfaces tailored to the underlying data-source would allow more complex queries to be performed which could provide more relevant search results to the user. This plan was dropped due to time constraints, however it is a plan which should be attempted in the future. 

### Value Search Panel
The *value search panel* was designed to support the user in selecting data values associated with an entity cell. 

![The *value search panel* which allows users to select data related to an entity cell.](./img/ValueSearch_Selection.png)

This panel operates in a similar fashion to the [entity search panel](#entity-search-panel) except that it tracks the last selected entity cell and does not allow the user to filter the returned properties except by free-text search. This panel does not allow filtering due to the relatively small number of properties that each entity is expected to have. This panel will likely not cope well and has not been tested where entities have hundreds or thousand of related properties.

The panel only shows directly related entities without any attempt to allow browsing of further related items. Initially a tree view of the related properties eminating from the selected entities was considered but was considered but disregarded as unnecessarily complicated for the initial proof of concept.

#### Tracking the Last Visited Entity Cell
The panel maintains an ordered history of the cells visited by the user and uses this to keep track of the last cell visited which contains an entity. The panel uses the last visited cell which currently contains an entity as the starting point for a user's search. This means that as soon as a user inserts an entity into a cell, the next cell they visit starts searching for related properties. This appears to be a convenient way to provide a user with relevant information to support their task. This functionality also ensures that if they remove an entity from a cell, we go back to the previously selected cell before that one which still contains an entity so that the extension continually provides the most relevant information we have.

#### One to Many Relationships
The panel itself does not attempt to account for one-to-many relationships between the entity and linked property values. The panel could be extended in future to warn the user where properties with multiple values are present and allow the user to override the default [sparqlValue function one-to-many](#handling-one-to-many-relationships) behaviour.

![Example fo the way that the *value search panel* handles one-to-many relationships.](./img/ValueSearch_TooManyEntities.png)

### Controller Architecture
The `cymru.mwnci.officeinterface.controllers.SparqlEntitySearchController` class is the controller which handles the state of the [entity search panel](#entity-search-panel). The `cymru.mwnci.officeinterface.controllers.SparqlValueSearchController` class is the controller which handles the state of the [value search panel](#value-search-panel). Both the `SparqlEntitySearchController` and `SparqlValueSearchController` classes inherit functionality from the `SparqlSearchBaseController` class which drives the functionality which is common to both panels and extends directly from the [`ReactiveSpreadsheetBaseController`](#reactivespreadsheetbasecontroller).

![The class hierarchy of controllers used in the office interface. Generated using JetBrains Intellij IDEA.](./img/controllerHierarchy.png)

<!-- #### SparqlSearchBaseController
![Public and protected methods in the SparqlSearchBaseController abstract class. Generated using JetBrains Intellij IDEA.](./img/SparqlSearchBaseController.png)

The *SparqlSearchBaseController* contains a series of abstract methods which allow the base-classes to customise the common functionality to apply to their respective models. -->

<!-- #### SparqlEntitySearchController
![Public and protected methods in the SparqlEntitySearchController class. Generated using JetBrains Intellij IDEA.](./img/SparqlEntitySearchController.png)

#### SparqlValueSearchController
![Public and protected methods in the SparqlValueSearchController class. Generated using JetBrains Intellij IDEA.](./img/SparqlValueSearchController.png) -->

##### Search Parameter Models
Each search panel has a model which holds the parameters which are relevant to its search. Both models extend from `BaseSearchParams` and contain specialisations for the subject search and property search panels respectively. 

The derived models are those that are serialised to JSON in cells. 

![Models holding search parameters for the search panels. Generated using JetBrains Intellij IDEA.](./img/SearchParamModels.png)

##### Responsibilities
The `SparqlSearchBaseController` is responsible for:

* Holding the *search parameters* for both panels.
* Ensuring all available SPARQL providers are displayed.
* Ensuring that the `SparqlDataSource` and the `SearchTerm` are kept in sync with user input.
* Triggering the search request when appropriate.
* Populating the search results.
* Keeping the user-selected result in sync with the `Entity` search parameters.
* Writing search parameters to cells.
* Handling errors in the *RxJava* system and displaying an appropriate error mesage.

The `SparqlEntitySearchController` is responsible for:

* Informing `SparqlSearchBaseController` what the search parameters in the selected cell are.
* Populating the [available entity filters](./SparqlEngine.md#using-suggested-filters) for user selection.
* Keeping the `EntityFilter` search parameter in sync with user input.
* Fetching results when requested by `SparqlSearchBaseController`.
* Informing `SparqlSearchBaseController` how to write search parameters back to a cell.

The `SparqlValueSearchController` is responsible for:

* Informing `SparqlSearchBaseController` what the search parameters in the selected cell are.
* Keeping the *last selected entity cell* (`SubjectCell`) in sync with the search parameters.
* Setting the `SearchLanguage` based on the user's current selected locale.
* Informing `SparqlSearchBaseController` how to write search parameters back to a cell.

##### Single Threaded Execution
All controllers are responsible for ensuring that all actions to update the search parameters and the user interface happen in *one thread* per panel using the `SparqlSearchBaseController.getStaticRxThreadForController` method. This is to ensure that the behaviour of the search panels is predictable and actions do not happen out-of-order. 

The only area where there is an **exception** to this single-threaded behaviour is when fetching SPARQL results. This can be a slow process and the user may wish to cancel (or ignore) a previous request in order to apply a faster or more relevant query. See uses of the `ioScheduler` field in `SparqlSearchBaseController` for more detail.

### Sidebar Configuration
Primary configuration of the sidebar panels is done in `interface/resources/Sidebar.xcu`. This file allows specification of which context the sidebar and panels are visible in. Mwnci ensures that the sidebar and panels are only visible when the user is editing a spreadsheet. The configuration file also allows us to specify the titles of the sidebar and panels when visible and a *factory* which can be used to instantiate the UI component which is placed in the sidebar panel. The *factory* must be registered in `interface/resources/Factories.xcu`.

# Office Service Registration
Services, including the implementation of `XMwnciInterface` and the sidebar [factory](#sidebar-configuration) must be registered with the Open/LibreOffice [UNO](https://wiki.openoffice.org/wiki/Uno) extension system. In order to do this, the fully-qualified class names must be listed in the `interface/source/cymru/mwnci/officeinterface/RegistrationHandler.classes` file. It should be noted that these services must by nature implement the [`__getComponentFactory`](https://wiki.openoffice.org/wiki/Documentation/DevGuide/WritingUNO/Providing_a_Single_Factory_Using_a_Helper_Method) and [`__writeRegistryServiceInfo`](https://wiki.openoffice.org/wiki/Documentation/DevGuide/WritingUNO/Write_Registration_Info_Using_a_Helper_Method) static methods necessary for registration.

# Protocol Handlers
[Protocol handlers](https://wiki.openoffice.org/wiki/Documentation/DevGuide/WritingUNO/Protocol_Handler) are used by Mwnci to broadcast events throughout the application. In this project the current use is to broadcast when SPARQL requests start and finish. This is used in the user interface to prevent unnecessary actions happening while the spreadsheet's SPARQL cells are being updated. It is also used in the project's [unit tests](#unit-testing) to ensure that tests are only executed once the spreadsheet's data have been loaded from the testing SPARQL repository.

The entirety of the protocol handler functionality can be found in the `cymru.mwnci.officeinterface.protocolhandler.MwnciProtocolHandler` class and the `interface/resources/ProtocolHandler.xcu` office configuration file. The `getForMwnciProtocolHandler` static function in the `cymru.mwnci.officeinterface.protocolhandler.MwnciStatusListener` class can be used to provide an observable which notifies when SPARQL queries start or stop processing.

# Poor Open/LibreOffice Documentation
It should be known that the existing documentation for developing LibreOffice and OpenOffice extensions is in some instances very poor. Through the course of developing this extension I have found out-of-date and contradictory documentation. Further, I have often found it necessary to search through the underlying [C++ source code](https://opengrok.libreoffice.org/) to gain an understanding of various points of interest.

# Unit Testing
A simple unit testing framework has been set-up to test the Calc functions made available by the extension. It makes use of the the [OpenOffice Bootstrap Connector](https://mvnrepository.com/artifact/org.openoffice/bootstrap-connector/0.1.1) library to launch an instance of Open/LibreOffice and open a given document. Once open, the test class listens to the state of SPARQL connections and makes a guess about when they have finished loading the document's data using tools described in the [protocol handler](#protocol-handlers) documentation. The values of cells are then compared against the expected values in order to ensure correct behaviour.

An example of such unit tests can be seen in `cymru.mwnci.officeinterface.tests.CalcFunctionTests` which tests the `interface/tests/cymru/mwnci/officeinterface/spreadsheets/LocalRepoTestSheet.ods ` spreadsheet file using the [local test repo](./SparqlEngine.md#local-sparql-test-repository). Unit tests were not written for the user interface due to time constraints.