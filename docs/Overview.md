# Project Mwnci

Project Mwnci is a project which aims to provide the ability to include live up-to-date date from SPARQL data-sources inside Open/LibreOffice Calc spreadsheets. 

![Example of a spreadsheet utilising the Project Mwnci extension.](./img/ValueSearch_FillDownExample.png)


For a more detailed background as well as aims of the project please click [here](../planning/BackgroundAimsObjectives.md). 


## Documentation
For brief documentation on the functionality of Project Mwnci see the following two documents:

* [Selecting an Entity](./SelectingAnEntity.md)
* [Seecting a Value](./SelectingAValue.md)

## Knowns/Unknowns

* Large spreadsheets have not been tested and multiplexing of requests has not been addressed. It's best to keep the size of the sheets small at this point in time, especially when opening previously-saved documents which make use of this functionality.
* I haven't been able to test this out in any other locale yet but opening a spreadsheet in a different locale should automatically translate entity labels and values where available. It would be great if someone could test this out for me, but it is likely to be plagued by incomplete data.
* The extension is currently only available in English, however this is (some) provision for expansion to other languages at a later point in time.
* There is currently no functionality to refresh the data periodically or on-demand. This will require a bit more development work.