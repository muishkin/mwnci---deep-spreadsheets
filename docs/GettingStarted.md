# System requirements

* Gradle 4.10.3
* Ant 
* Java 1.8 compatible SDK
* LibreOffice/OpenOffice installation with the corresponding SDK
    * unopkg - Must be on the user's `PATH`. Should come as part of the Libre/OpenOffice SDK.

**Ensure that all of the above are installed**. [This OpenOffice Wiki document](https://wiki.openoffice.org/wiki/JavaEclipseTuto) goes a reasonable way towards helping set up your computer to develop LibreOffice extensions.

# Initial Configuration
Edit [`interface/gradle.properties`](../BuildSystem/#configuring-gradleproperties) and set the following properties:

| Property | Description |
| :- | :- |
| sdk.dir | Set this to the path of your Open/LibreOffice SDK installation. |
| office.install.dir | Set this to the path of your Open/LibreOffice installation. |
| libreOfficeSdkJarDir | The directory containing Open/LibreOffice JARs. Likely to be `office.install.dir/program/classes/`. |

# Building & Packaging

> **Quick tip:** run `sh ./buildInstallLaunch.sh` from a Linux bash shell in the repository root directory to quickly build, install and launch Calc with the Mwnci extension installed.

The project is **built and packaged** by execution of the following command in the root of the project repository:

```
./gradlew packageOxt
```
This builds all relevant IDL and Java code and compiles the `MwnciInterface.oxt` extension file in `interface/dist`. 

This process can also be triggered by IDEs such as *JetBrains IntelliJ IDEA*.

# Installation
The `interface/dist/MwnciInterface.oxt` extension file can be installed manually via the [Extension Manager](https://wiki.documentfoundation.org/Documentation/HowTo/install_extension). 

It can also be **installed** automatically by *Gradle* using the `unopkg` command. 

```
./gradlew installOxt
```
This command will automatically uninstall any previous version of the Project Mwnci extension currently installed. *You must ensure that all instances of Libre/OpenOffice are closed before running this command*.

The **build, packaging and installation** process can be triggered by calling:

``` 
./gradlew build
```

*You must ensure that all instances of Libre/OpenOffice are closed before running this command*.

# Testing
Unit tests can be executed with the following command:

```
./gradlew test
```

# Debugging
In order to debug the extension in Libre/OpenOffice it is necessary to set the following [Java start parameters](https://help.libreoffice.org/6.1/en-US/text/shared/optionen/javaparameters.html):

* `-Xdebug`
* `-Xrunjdwp:transport=dt_socket,server=y,address=8000,suspend=n`

Once these properties have been set, it is possible to attach to the Java process after the extension has been loaded by the application.

# Logging
Logging in this project is performed using [log4j2](https://logging.apache.org/log4j/2.x/). The output is directed to both the console and the `mwnci.log` file in the application's current working directory. 

Error logging is performed on every handled exception and debug/trace logging is performed in an ad-hoc fashion throughout the application where it was used to help in the development of the extension. 