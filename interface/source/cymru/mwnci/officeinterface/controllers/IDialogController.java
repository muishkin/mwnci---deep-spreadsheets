package cymru.mwnci.officeinterface.controllers;

import com.sun.star.awt.XDialog2;

public interface IDialogController extends IController {
    void setDialog(XDialog2 dialog);
}
