package cymru.mwnci.officeinterface.controllers;

import com.sun.star.awt.XDialog2;
import com.sun.star.beans.XPropertySet;
import com.sun.star.uno.XComponentContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MessageBoxController extends ReactiveBaseController implements IDialogController {

    private static Logger logger = LogManager.getLogger(MessageBoxController.class);
    private final String m_message;
    private XDialog2 m_dialog;

    public MessageBoxController(XComponentContext componentContext, String message) {
        super(componentContext);
        m_message = message;
    }

    public void init() {
        try {
            XPropertySet label_message_propertySet = this.getPropertySetForControl("TextField_Message");
            label_message_propertySet.setPropertyValue("Text", m_message);

            this.onControlEvent("CommandButton_Close", EventTypes.ACTION_PERFORMED)
                    .subscribe(
                            event -> m_dialog.endDialog(0),
                            err -> logger.error(err.getMessage(), err)
                    );
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void setDialog(XDialog2 dialog) {
        m_dialog = dialog;
    }
}
