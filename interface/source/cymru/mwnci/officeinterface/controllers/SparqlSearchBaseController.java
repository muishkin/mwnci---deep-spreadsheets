package cymru.mwnci.officeinterface.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.star.awt.*;
import com.sun.star.beans.PropertyVetoException;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.lang.EventObject;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.table.CellAddress;
import com.sun.star.table.XCell;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.MwnciHelpers;
import cymru.mwnci.officeinterface.models.BaseSearchParams;
import cymru.mwnci.officeinterface.models.SearchParamsWithCell;
import cymru.mwnci.officeinterface.models.SearchResultsWithParams;
import cymru.mwnci.officeinterface.panels.MwnciDialogPanel;
import cymru.mwnci.sparql.Mwnci;
import cymru.mwnci.sparql.exceptions.SparqlProviderNotFoundException;
import cymru.mwnci.sparql.helpers.SparqlIriHelpers;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.models.SparqlServiceProvider;
import cymru.mwnci.sparql.services.Interfaces.ISparqlProvider;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * A controller which provides common functionality shared by both the Entity and Value search panels.
 *
 * @param <T> Derived type of BaseSearchParams
 */
public abstract class SparqlSearchBaseController<T extends BaseSearchParams> extends ReactiveSpreadsheetBaseController {

    private static Logger logger = LogManager.getLogger(SparqlSearchBaseController.class);

    /**
     * Watches the latest used SPARQL provider.
     */
    private static Subject<Optional<SparqlServiceProvider>> m_latestSparqlProvider = getLatestSparqlProviderSubject();

    /**
     * Do not access directly. Use getSearchParamsThreadSafe()
     */
    protected final Subject<SearchParamsWithCell<T>> m_searchParamsWithCell = BehaviorSubject.create();

    protected final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * The thread on which all RxJava operations should be performed for the current panel.
     */
    protected final Scheduler rxThread;
    /**
     * Let the IO run on seperate threads so we can ignore previous requests and supersede them with new requests.
     */
    protected final Scheduler ioScheduler = Schedulers.newThread();

    protected XControl listBox_results_control;
    protected XListBox listBox_results;
    protected XPropertySet listBox_results_propertySet;

    protected XControl comboBox_sparqlQuerySource_control;
    protected XComboBox comboBox_sparqlQuerySource;
    protected XPropertySet comboBox_sparqlQuerySource_propertySet;
    protected XPropertySet textField_searchText_propertySet;


    protected SparqlSearchBaseController(XComponentContext componentContext) {
        super(componentContext);
        rxThread = getStaticRxThreadForController();
    }

    private static Subject<Optional<SparqlServiceProvider>> getLatestSparqlProviderSubject() {
        Subject<Optional<SparqlServiceProvider>> sub = BehaviorSubject.create();

        sub.onNext(Optional.empty());

        return sub;
    }

    public void init() {
        try {
            initFunctionAccess();
            bindSearchParamsFromCell();
            watchWindowShowHide(MwnciDialogPanel.ROOT_PANEL_CONTROL_NAME);

            initSparqlQuerySourceComboBox();

            initSearchTextBox();
            initResultsListBox();

            // Ensure that we save any updated search params back to the cell.
            bindSearchParamsToCell();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Provides access to the static Scheduler for the derived controller's panel.
     * Ensures that all operations for the given panel are performed sequentially to ensure consistent state.
     *
     * @return Single threaded RxJava Scheduler.
     */
    protected abstract Scheduler getStaticRxThreadForController();

    /**
     * Extracts the current search parameters of derived type T from the specified cell which is currently selected.
     *
     * @param selectedCell The cell to extract the current search parameters from.
     * @return The SearchParams of derived type T extracted from the cell.
     */
    protected abstract SearchParamsWithCell<T> getSearchParamsFromCell(XCell selectedCell);

    /**
     * Method which converts SearchParams of derived type T in to a CALC formula string for serialisation to a cell.
     *
     * @param searchParams
     * @return
     * @throws Exception
     */
    protected abstract String getSerialisedSearchParamsFormula(T searchParams) throws Exception;

    /**
     * Returns a boolean which is used to decide when parameters are suitably populated to perform a search.
     *
     * @return A boolean which informs when SearchParams are suitably populated for a search request to be triggered.
     */
    protected abstract boolean updateResultsOnlyWhen(SearchParamsWithCell<T> paramsWithCell);

    /**
     * Takes an observable of the current search parameters and maps them to an observable combining the parameters
     * with the search results.
     *
     * @param searchParams The parameters to search on.
     * @return An object containing both the search parameters and the results for the search.
     */
    protected abstract Observable<List<EntityWithUri>> getSearchResultsWithParams(T searchParams);

    /**
     * Generates a new SearchParams instance of derived type T with default settings.
     *
     * @param selectedCell
     * @return new SearchParams instance of derived type T.
     */
    protected abstract T getNewParamsObject(XCell selectedCell);

    /**
     * Generates a new SearchParams instance of derived type T with default settings.
     * To be used when an error has occurred. KISS.
     *
     * @return new SearchParams instance of derived type T.
     */
    protected abstract T getNewParamsObjectForError();


    /**
     * Provides a string which is unique for each search. Allows us to decide whether a new search request should
     * be made at a given point in time or not.
     *
     * @param searchParams
     * @return A string which is unique for each search.
     */
    protected abstract String uniqueSearchParamIdentifier(T searchParams);

    /**
     * Access SearchParams observable via this method to ensure threadsafe reading/writing.
     * This serialises execution so we don't get out-of-order multi-threaded hell.
     * <p>
     * The returned observable also does not trigger until
     *
     * @return SearchParams observable.
     */
    protected Observable<T> getSearchParamsReadyThreadSafe() {
        return getSearchParamsWithCellThreadSafe()
                .map(searchParamsWithCell -> searchParamsWithCell.getSearchParams());
    }

    /**
     * Access SearchParams with CellAddress observable via this method to ensure threadsafe reading/writing.
     * This serialises execution so we don't get out-of-order multi-threaded hell.
     * <p>
     * The returned observable also does not trigger until
     *
     * @return (SearchParams & CellAddress) observable.
     */
    protected Observable<SearchParamsWithCell<T>> getSearchParamsWithCellThreadSafe() {
        Observable<SearchParamsWithCell<T>> searchParams = this.m_searchParamsWithCell
                .observeOn(rxThread)
                .subscribeOn(rxThread)
                .distinctUntilChanged();

        // Ensure that no SPARQL request occurs in the panel whilst it is hidden.
        return Observable.combineLatest(
                searchParams,
                this.getPanelIsVisible(),
                (a, b) -> new Object[]{a, b}
        )
                .filter(values -> {
                    // Only pass results forward when the panel is visible
                    boolean panelIsVisible = ((Boolean) values[1]).booleanValue();
                    return panelIsVisible;
                })
                .map(values -> (SearchParamsWithCell<T>) values[0])
                .observeOn(rxThread)
                .subscribeOn(rxThread);
    }

    /**
     * Provides a suggested SPARQL provider based on previous user selection (where available).
     *
     * @return A suggested SPARQL provider.
     */
    protected ISparqlProvider getSuggestedSparqlProvider() {
        try {
            SparqlServiceProvider suggestedProvider = getSuggestedSparqlProviderUserInfo();
            return Mwnci.getSparqlProvider(suggestedProvider.getServiceName());
        } catch (SparqlProviderNotFoundException ex) {
            logger.error(ex.getMessage(), ex);
            throw new RuntimeException(ex);
        }
    }

    /**
     * Provides a suggested SPARQL provider based on previous user selection (where available).
     *
     * @return A suggested SPARQL provider.
     */
    protected SparqlServiceProvider getSuggestedSparqlProviderUserInfo() {
        Optional<SparqlServiceProvider> maybeSparqlProvider = m_latestSparqlProvider
                .distinctUntilChanged()
                .firstElement()
                .blockingGet();

        return maybeSparqlProvider
                .orElseGet(() -> {
                    List<SparqlServiceProvider> availableProviders =
                            Mwnci.getSparqlServiceProviderUserInfo();
                    return availableProviders.get(0);
                });
    }

    /**
     * Ensures that suitable user changes to SearchParams are persisted to the relevant cell.
     */
    protected void bindSearchParamsToCell() {
        this.getSearchParamsWithCellThreadSafe()
                .filter(searchParamsWithCell -> {
                    T searchParams = searchParamsWithCell.getSearchParams();
                    return searchParams.getShouldbePersistedToCell()
                            && !searchParams.isEmpty();
                })
                .doOnNext(searchParamsWithCell -> {
                    logger.debug("Writing SearchParams to Cell");
                    // If nothing is present, don't set the formula. There is not point.
                    // Otherwise we have info we need to place inside the formula.
                    String formula = getSerialisedSearchParamsFormula(searchParamsWithCell.getSearchParams());

                    getCellByAddress(searchParamsWithCell.getCellAddress())
                            .ifPresent(cell -> cell.setFormula(formula));
                })
                .doOnError(err -> {
                    informUserOfError(err);
                    logErrorSetNewParams(err);
                    bindSearchParamsToCell();
                })
                .subscribe();
    }

    /**
     * Initialises the Results ListBox.
     * Ensures that search results are requested and the values populated.
     * Also ensures that user interaction with the ListBox updates the SearchParams in turn.
     */
    protected void initResultsListBox() {
        String controlName = "ListBox_Results";

        listBox_results_control = this.getControl(controlName);
        listBox_results = UnoRuntime.queryInterface(XListBox.class, listBox_results_control);
        listBox_results_propertySet = this.getPropertySetForControl(controlName);

        this.getSearchParamsWithCellThreadSafe()
                .filter(searchParamsWithCell -> updateResultsOnlyWhen(searchParamsWithCell))
                .distinctUntilChanged(searchParamsWithCell -> {
                    CellAddress cellAddress = searchParamsWithCell.getCellAddress();
                    String cellUniqueId =
                            "(" + cellAddress.Sheet + ", " + cellAddress.Column + ", " + cellAddress.Row + ") ";
                    return cellUniqueId + uniqueSearchParamIdentifier(searchParamsWithCell.getSearchParams());
                })
                .debounce(500, TimeUnit.MILLISECONDS)
                .doOnNext(searchParamsWithCell -> setListBoxLoading(listBox_results, listBox_results_propertySet))
                .switchMap(searchParamsWithCell ->
                        this.getSearchResultsWithParams(searchParamsWithCell.getSearchParams())
                                .map(results -> new SearchResultsWithParams<T>(searchParamsWithCell, results))
                )
                .subscribeOn(rxThread)
                .observeOn(rxThread)
                .doOnNext(searchParamsAndResults -> {
                    // Populate the search results inside the list box now.
                    clearListBoxItems(listBox_results);

                    populateListBoxWithResults(searchParamsAndResults, listBox_results, listBox_results_propertySet);
                })
                // Pull through any updated selection the user made about their selected entity.
                .switchMap(searchParamsAndResults ->
                        getUpdatedParamsOnUserSearchListBoxChange(searchParamsAndResults, listBox_results)
                )
                .distinctUntilChanged()
                .doOnNext(newSearchParams -> m_searchParamsWithCell.onNext(newSearchParams))
                .doOnError(err -> {
                    informUserOfError(err);
                    logErrorSetNewParams(err);
                    initResultsListBox();
                })
                .subscribe();
    }

    /**
     * Watches the currently selected cell and retrieves current SearchParams (or defaults) from the content.
     */
    protected void bindSearchParamsFromCell() {
        this.getMaybeCurrentlySelectedCell()
                .observeOn(rxThread)
                .subscribeOn(rxThread)
                .doOnNext(maybeSelectedCell -> {
                    SearchParamsWithCell<T> params = maybeSelectedCell
                            .map(selectedCell -> getSearchParamsFromCell(selectedCell))
                            // The cell really should exist, but let's cover all bases.
                            .orElseGet(() -> {
                                T newParams = getNewParamsObjectForError();
                                // Emergency value where we don't have a cell selected!
                                CellAddress cellAddress = new CellAddress((short) 0, 0, 0);

                                return new SearchParamsWithCell<T>(newParams, cellAddress);
                            });

                    m_searchParamsWithCell.onNext(params);
                })
                .doOnError(err -> {
                    informUserOfError(err);

                    logger.error(err.getMessage(), err);

                    bindSearchParamsToCell();
                })
                .subscribe();
    }

    /**
     * Initialises the Sparql Query Source ComboBox
     * Ensures that it is populated appropriately and that user interaction updates the SearchParams.
     */
    protected void initSparqlQuerySourceComboBox() {
        String controlName = "ComboBox_SparqlQuerySourceSelector";
        comboBox_sparqlQuerySource_control = this.getControl(controlName);
        comboBox_sparqlQuerySource = UnoRuntime.queryInterface(XComboBox.class, comboBox_sparqlQuerySource_control);

        comboBox_sparqlQuerySource_propertySet = getPropertySetForControl(controlName);

        // Initialise the items available from the drop-down.

        List<SparqlServiceProvider> sparqlServiceProviders = Mwnci.getSparqlServiceProviderUserInfo();

        List<String> sparqlQuerySourceLabels = sparqlServiceProviders
                .stream()
                .map(provider -> provider.getServiceDisplayName())
                .collect(Collectors.toList());

        // Now add the items we should have displayed.
        short i = 0;
        for (String label : sparqlQuerySourceLabels) {
            comboBox_sparqlQuerySource.addItem(label, i);
            i++;
        }

        // When the SparqlDataSource changes in searchParams.
        this.getSearchParamsWithCellThreadSafe()
                // Set the selected drop-down from the searchParams
                .doOnNext(searchParamsWithCell -> {
                    setSelectedSparqlDataSourceFromParams(sparqlServiceProviders, searchParamsWithCell);

                    T searchParams = searchParamsWithCell.getSearchParams();

                    // Keep track of the last used SPARQL provider.
                    if (searchParams.SparqlDataSource != null) {
                        m_latestSparqlProvider.onNext(
                                Optional.of(searchParams.SparqlDataSource)
                        );
                    }
                })
                // Update the searchParams with user selected item from drop-down
                .switchMap(searchParamsWithCell ->
                        getUpdatedSearchParamsOnSparqlSourceUserSelection(
                                controlName,
                                sparqlServiceProviders,
                                searchParamsWithCell
                        )
                )
                .doOnError(err -> {
                    informUserOfError(err);

                    logErrorSetNewParams(err);
                    initSparqlQuerySourceComboBox();
                })
                .subscribe(
                        newSearchParamsWithCell -> this.m_searchParamsWithCell.onNext(newSearchParamsWithCell),
                        err -> logger.error(err.getMessage(), err)
                );
    }

    protected void logErrorSetNewParams(Throwable err) {
        logger.error(err.getMessage(), err);

        try {
            T newParams = getNewParamsObjectForError();
            CellAddress emergencyCellAddress = new CellAddress((short) 0, 0, 0);
            m_searchParamsWithCell.onNext(
                    new SearchParamsWithCell<>(newParams, emergencyCellAddress)
            );
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    /**
     * Initialises the free-text Search TextBox.
     * Ensures that the value is populated from current SearchResults.
     * Also ensures that user interaction updates SearchResults.
     */
    protected void initSearchTextBox() {
        String controlName = "TextField_SearchText";
        textField_searchText_propertySet = this.getPropertySetForControl(controlName);

        this.getSearchParamsWithCellThreadSafe()
                // Ensure the UI matches the state we currently have
                .doOnNext(searchParamsWithCell ->
                        textField_searchText_propertySet.setPropertyValue("Text",
                                searchParamsWithCell.getSearchParams().SearchTerm)
                )
                // Update the SearchParams with updates from user input.
                .switchMap(searchParamsWithCell -> getUpdateSearchParamsOnUserSearchTextUpdate(controlName,
                        searchParamsWithCell)
                )
                .doOnError(err -> {
                    informUserOfError(err);
                    logErrorSetNewParams(err);
                    initSearchTextBox();
                })
                .subscribe(
                        newSearchParams -> m_searchParamsWithCell.onNext(newSearchParams),
                        err -> logger.error(err.getMessage(), err)
                );
    }

    protected void trySetSparqlDataSourceForCell(BaseSearchParams params, String existingCellText) {
        Optional<SparqlServiceProvider> providerInfo = SparqlIriHelpers.getEntityUriFromString(existingCellText)
                .flatMap(entityUri -> Mwnci.getSparqlProviderUserInfoForUri(entityUri));

        if (providerInfo.isPresent()) {
            params.SparqlDataSource = providerInfo.get();
        } else {
            params.SparqlDataSource = getSuggestedSparqlProviderUserInfo();
        }
    }

    @Override
    protected String getUserErrorMessage(Throwable err) {
        StringBuilder error = new StringBuilder("An error occurred. Your search will be reset to defaults.");

        error.append(System.lineSeparator());
        error.append(System.lineSeparator());

        error.append(err.getMessage());

        return error.toString();
    }

    private Observable<SearchParamsWithCell<T>> getUpdatedParamsOnUserSearchListBoxChange(
            SearchResultsWithParams<T> searchParamsAndResults, XListBox listBox) {

        Subject<SearchParamsWithCell<T>> newSearchParams = PublishSubject.create();
        XItemListener itemChangeListener = new XItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                try {
                    SearchResultsWithParams<T> resultsWithNewParams = searchParamsAndResults.deepCopy();

                    List<EntityWithUri> results = searchParamsAndResults
                            .getResults();

                    T newParams = resultsWithNewParams.getSearchParams();

                    short selectedItemPos = listBox.getSelectedItemPos();
                    EntityWithUri selectedItem;
                    if (results.isEmpty()) {
                        selectedItem = null;
                    } else {
                        selectedItem = results.get(selectedItemPos);
                    }

                    newParams.Entity = selectedItem;
                    newParams.setShouldBePersistedToCell(true);
                    newSearchParams.onNext(resultsWithNewParams);
                } catch (Exception ex) {
                    logger.error(ex.getMessage(), ex);
                }
            }

            @Override
            public void disposing(EventObject eventObject) {
                newSearchParams.hasComplete();
            }
        };

        listBox.addItemListener(itemChangeListener);
        newSearchParams.doOnComplete(() -> listBox.removeItemListener(itemChangeListener));

        // Now set the currently selected item.
        T params = searchParamsAndResults.getSearchParams();
        if (params.Entity != null) {
            URI knownEntityUri = params.Entity.getUri();
            Short entityPosition = getPositionOfKnownEntityUri(searchParamsAndResults, knownEntityUri);

            if (entityPosition != null) {
                listBox.selectItemPos(entityPosition, true);
            }
        }

        return newSearchParams;
    }

    private Observable<SearchParamsWithCell<T>> getUpdatedSearchParamsOnSparqlSourceUserSelection(
            String controlName, List<SparqlServiceProvider> sparqlServiceProviders,
            SearchParamsWithCell<T> searchParamsWithCell) {
        return this.getTextFieldValue(controlName)
                .debounce(500, TimeUnit.MILLISECONDS)
                .map(userSelectedDataSource -> {

                    SearchParamsWithCell<T> newParamsWithCell = searchParamsWithCell.deepCopy();

                    SparqlServiceProvider selectedProvider = getSparqlServiceProviderByName(
                            sparqlServiceProviders, userSelectedDataSource);

                    T newParams = newParamsWithCell.getSearchParams();

                    newParams.SparqlDataSource = selectedProvider;
                    newParams.setShouldBePersistedToCell(true);

                    return newParamsWithCell;
                });
    }

    private Observable<SearchParamsWithCell<T>> getUpdateSearchParamsOnUserSearchTextUpdate(
            String controlName, SearchParamsWithCell<T> searchParamsWithCell) {
        return this.getTextFieldValue(controlName)
                .debounce(500, TimeUnit.MILLISECONDS)
                .map(newSearchTerm -> {
                    SearchParamsWithCell<T> newParamsWithCell = searchParamsWithCell.deepCopy();
                    T newParams = newParamsWithCell.getSearchParams();
                    newParams.SearchTerm = newSearchTerm;
                    newParams.setShouldBePersistedToCell(true);

                    return newParamsWithCell;
                });
    }

    private void populateListBoxWithResults(SearchResultsWithParams<T> searchParamsAndResults,
                                            XListBox listBox, XPropertySet listBox_propertySet)
            throws UnknownPropertyException, PropertyVetoException, WrappedTargetException {

        short i = 0;
        List<EntityWithUri> results = searchParamsAndResults.getResults();
        for (EntityWithUri entityWithUri : results) {
            String displayText = MwnciHelpers.getDisplayTextForEntityWithUri(entityWithUri);
            listBox.addItem(displayText, i);
            i++;
        }

        if (results.isEmpty()) {
            listBox.addItem("No results", (short) 0);
        } else {
            listBox_propertySet.setPropertyValue("Enabled", true);
        }
    }

    private Short getPositionOfKnownEntityUri(SearchResultsWithParams searchParamsAndResults, URI knownEntityUri) {
        Short entityPosition = null;

        short i = 0;
        List<EntityWithUri> searchResults = searchParamsAndResults.getResults();
        for (EntityWithUri e : searchResults) {
            if (e.getUri().equals(knownEntityUri)) {
                entityPosition = i;
                break;
            }
            i++;
        }
        return entityPosition;
    }

    private SparqlServiceProvider getSparqlServiceProviderByName(List<SparqlServiceProvider> sparqlServiceProviders,
                                                                 String userSelectedDataSource) {
        for (SparqlServiceProvider provider : sparqlServiceProviders) {
            if (provider.getServiceDisplayName().equals(userSelectedDataSource)) {
                return provider;
            }
        }

        // We don't have the user selected sparql data-source, let's assume they've put in a SPARQL end-point URL.
        return new SparqlServiceProvider(userSelectedDataSource, userSelectedDataSource);
    }

    private void setListBoxLoading(XListBox listBox, XPropertySet listBox_propertySet)
            throws UnknownPropertyException, PropertyVetoException, WrappedTargetException {

        listBox_propertySet.setPropertyValue("Enabled", false);
        clearListBoxItems(listBox);
        listBox.addItem("Searching", (short) 0);
    }


    private void setSelectedSparqlDataSourceFromParams(List<SparqlServiceProvider> sparqlServiceProviders,
                                                       SearchParamsWithCell<T> searchParamsWithCell)
            throws UnknownPropertyException, PropertyVetoException, WrappedTargetException {
        SparqlServiceProvider selectedOption;
        if (searchParamsWithCell.getSearchParams().SparqlDataSource != null) {
            selectedOption = searchParamsWithCell.getSearchParams().SparqlDataSource;
        } else {
            // Choice un-set. Set the default option.
            selectedOption = sparqlServiceProviders.get(0);

            SearchParamsWithCell<T> newParamsWithCell = searchParamsWithCell.deepCopy();
            T newParams = newParamsWithCell.getSearchParams();

            newParams.SparqlDataSource = selectedOption;

            this.m_searchParamsWithCell.onNext(newParamsWithCell);
        }

        String selectedOptionText = selectedOption.getServiceDisplayName();
        comboBox_sparqlQuerySource_propertySet.setPropertyValue("Text", selectedOptionText);
    }

}
