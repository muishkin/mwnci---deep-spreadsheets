package cymru.mwnci.officeinterface.controllers;

import com.sun.star.awt.XControl;
import com.sun.star.script.AllEventObject;

public interface IController {

    /**
     * Handles an event sourced from the component given by componentName.
     * @param componentName
     * @param eventType
     * @param event
     */
    void handleEvent(String componentName, String eventType, AllEventObject event);

    /**
     * Registers a component with the controller. Provides the XControl so that values can be get/set.
     * @param componentName
     * @param control
     */
    void registerComponent(String componentName, XControl control);

    /**
     * To be called when the layout has been laid and the controller should initialise itself.
     */
    void init();
}
