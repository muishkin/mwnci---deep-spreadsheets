package cymru.mwnci.officeinterface.controllers;

import com.sun.star.beans.XPropertySet;
import com.sun.star.document.DocumentEvent;
import com.sun.star.document.XDocumentEventListener;
import com.sun.star.frame.GlobalEventBroadcaster;
import com.sun.star.frame.XModel;
import com.sun.star.lang.EventObject;
import com.sun.star.lang.XComponent;
import com.sun.star.reflection.InvocationTargetException;
import com.sun.star.script.AllEventObject;
import com.sun.star.script.XAllListener;
import com.sun.star.script.XEventAttacher2;
import com.sun.star.sheet.*;
import com.sun.star.table.CellAddress;
import com.sun.star.table.XCell;
import com.sun.star.table.XCellRange;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.MwnciHelpers;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public abstract class ReactiveSpreadsheetBaseController extends ReactiveBaseController {
    private static final Logger logger = LogManager.getLogger(ReactiveSpreadsheetBaseController.class);

    private final Subject<Optional<XSpreadsheetDocument>> m_currentSpreadsheetDocument = BehaviorSubject.create();
    private final Subject<Optional<XSpreadsheetView>> m_currentSpreadsheetView = BehaviorSubject.create();
    private final Subject<Optional<XCell>> m_currentSelectedCell = BehaviorSubject.create();
    protected XFunctionAccess m_functionAccess;
    private Subject<Boolean> m_panelIsVisible;

    protected ReactiveSpreadsheetBaseController(XComponentContext componentContext) {
        super(componentContext);

        // Wire up m_currentSpreadsheetDocument
        GlobalEventBroadcaster.create(this.m_componentContext)
                .addDocumentEventListener(new XDocumentEventListener() {
                    @Override
                    public void documentEventOccured(DocumentEvent documentEvent) {
                        logger.debug("documentEventOccurred of type " + documentEvent.EventName);
                        if (documentEvent.EventName.equals("OnFocus")) {
                            Object source = documentEvent.Source;

                            XSpreadsheetDocument spreadsheetDocument =
                                    UnoRuntime.queryInterface(XSpreadsheetDocument.class, source);

                            logger.debug("XSpreadsheetDocument focus caught. Triggering subject.");

                            m_currentSpreadsheetDocument.onNext(Optional.ofNullable(spreadsheetDocument));
                        } else if (documentEvent.EventName.equals("OnViewClosed")) {
                            logger.debug("XSpreadsheetDocument closed. Triggering subject.");
                            m_currentSpreadsheetDocument.onNext(Optional.empty());
                        }
                    }

                    @Override
                    public void disposing(EventObject eventObject) {
                        m_currentSpreadsheetDocument.hasComplete();
                    }
                });

        // Wire up m_currentSpreadsheetView
        this.m_currentSpreadsheetDocument
                .distinctUntilChanged()
                .map(maybeSpreadsheet -> maybeSpreadsheet.map(spreadsheet -> {
                    try {
                        logger.debug("Fetching XSpreadsheetView implementation.");
                        XModel spreadsheet_model = UnoRuntime.queryInterface(XModel.class, spreadsheet);
                        return UnoRuntime.queryInterface(XSpreadsheetView.class,
                                spreadsheet_model.getCurrentController());
                    } catch (java.lang.Exception e) {
                        logger.error(e.getMessage(), e);
                    }

                    return null;
                }))
                .subscribe(
                        maybeSpreadsheetView -> this.m_currentSpreadsheetView.onNext(maybeSpreadsheetView),
                        err -> logger.error(err.getMessage(), err)
                );

        // Wirte up m_currentSelectedCell
        Observable.combineLatest(
                this.m_currentSpreadsheetDocument, this.m_currentSpreadsheetView,
                (a, b) -> allOptionalValuesPresent(new Optional[]{a, b})
        )
                .distinctUntilChanged()
                .switchMap(maybeValues -> getMaybeCurrentSelectedCell(maybeValues))
                .subscribe(
                        maybeCurrentCell -> this.m_currentSelectedCell.onNext(maybeCurrentCell),
                        err -> logger.error(err.getMessage(), err)
                );
    }

    protected Observable<Optional<XSpreadsheetDocument>> getMaybeSpreadsheetDocument() {
        return m_currentSpreadsheetDocument
                .distinctUntilChanged();
    }

    protected Observable<Optional<XSpreadsheetView>> getMaybeSpreadsheetView() {
        return m_currentSpreadsheetView
                .distinctUntilChanged();
    }

    protected Observable<Optional<XCell>> getMaybeCurrentlySelectedCell() {
        return m_currentSelectedCell
                .distinctUntilChanged();
    }

    protected Optional<XCell> getCellByAddress(CellAddress cellAddress) {
        return this.m_currentSpreadsheetDocument.map(maybeCurrentSpreadsheetDocument ->
                maybeCurrentSpreadsheetDocument.flatMap(spreadSheetDocument -> {
                    String sheetName = spreadSheetDocument.getSheets()
                            .getElementNames()[0];
                    try {
                        Object spreadsheetO = spreadSheetDocument.getSheets()
                                .getByName(sheetName);

                        XCellRange spreadSheetCellRange = UnoRuntime.queryInterface(XCellRange.class, spreadsheetO);

                        XCell cell = spreadSheetCellRange.getCellByPosition(cellAddress.Column, cellAddress.Row);

                        return Optional.ofNullable(cell);
                    } catch (java.lang.Exception e) {
                        logger.error(e.getMessage(), e);
                    }

                    return Optional.empty();
                })
        ).blockingFirst();
    }

    protected Optional<XSpreadsheet> getMaybeCurrentActiveSheet() {
        return this.getMaybeSpreadsheetView()
                .map(maybeSpreadsheetView ->
                        maybeSpreadsheetView.map(spreadsheet_view -> spreadsheet_view.getActiveSheet())
                ).blockingFirst();
    }

    protected static boolean allOptionalParamsPresent(Optional... optionalValues) {
        for (Optional arg : optionalValues) {
            if (!arg.isPresent())
                return false;
        }

        return true;
    }

    protected static Optional<Object[]> allOptionalValuesPresent(Optional... optionalValues) {
        if (allOptionalParamsPresent(optionalValues)) {
            List<Object> values = new LinkedList<Object>();

            for (Optional arg : optionalValues) {
                values.add(arg.get());
            }

            return Optional.of(values.toArray());
        } else {
            return Optional.empty();
        }
    }

    protected Observable<Optional<XCell>> getMaybeCurrentSelectedCell(Optional<Object[]> maybeSpreadsheetInterfaces) {
        return maybeSpreadsheetInterfaces
                .map(spreadsheetInterfaces -> {
                    try {
                        logger.debug("getMaybeCurrentSelectedCell adding SelectionChanged listener.");
                        XSpreadsheetDocument ss = (XSpreadsheetDocument) spreadsheetInterfaces[0];
                        XSpreadsheetView sv = (XSpreadsheetView) spreadsheetInterfaces[1];

                        XComponent component = UnoRuntime.queryInterface(XComponent.class, sv);

                        Subject<Optional<XCell>> cellObservable = PublishSubject.create();

                        XModel spreadsheet_model = UnoRuntime.queryInterface(XModel.class, ss);

                        Object eventAttacher_obj = this.m_componentContext.getServiceManager()
                                .createInstanceWithContext(
                                        "com.sun.star.script.EventAttacher", this.m_componentContext);
                        final XEventAttacher2 eventAttacher = UnoRuntime.queryInterface(XEventAttacher2.class, eventAttacher_obj);

                        XAllListener actionListener = new XAllListener() {
                            @Override
                            public void firing(AllEventObject allEventObject) {
                                if (allEventObject.MethodName.equals("selectionChanged")) {
                                    try {
                                        logger.debug("SelectionChanged event fired. Fetching current cell.");
                                        Object cellO = spreadsheet_model.getCurrentSelection();
                                        XCell currentlySelectedCell = UnoRuntime.queryInterface(XCell.class, cellO);
                                        cellObservable.onNext(Optional.ofNullable(currentlySelectedCell));
                                    } catch (java.lang.Exception e) {
                                        logger.error(e.getMessage(), e);
                                        cellObservable.onNext(Optional.empty());
                                    }
                                }
                            }

                            @Override
                            public Object approveFiring(AllEventObject allEventObject)
                                    throws InvocationTargetException {
                                return null;
                            }

                            @Override
                            public void disposing(EventObject eventObject) {
                                cellObservable.hasComplete();
                            }
                        };

                        tryAttachListener(component, eventAttacher, actionListener);

                        return cellObservable.distinctUntilChanged();
                    } catch (java.lang.Exception e) {
                        logger.error(e.getMessage());
                        return null;
                    }
                })
                .orElseGet(() -> Observable.fromArray(Optional.empty()));
    }

    private void tryAttachListener(XComponent component, XEventAttacher2 eventAttacher, XAllListener actionListener) {
        try {
            eventAttacher.attachListener(
                    component,
                    actionListener,
                    null,
                    "XSelectionChangeListener",
                    ""
            );
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }


    protected String getCellAddress(Object cell) {
        try {
            XPropertySet cell_propertySet = UnoRuntime.queryInterface(XPropertySet.class, cell);
            return (String) cell_propertySet.getPropertyValue("AbsoluteName");
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return null;
    }

    /**
     * Watches the window and ensures that search results are not fetched when the window is hidden.
     *
     * @param controlNameToWatch, EventTypes.WINDOW_HIDDEN
     */
    protected void watchWindowShowHide(String controlNameToWatch) {
        m_panelIsVisible = BehaviorSubject.create();

        this.onControlEvent(controlNameToWatch, EventTypes.WINDOW_SHOWN)
                .subscribe(
                        e -> m_panelIsVisible.onNext(true),
                        err -> logger.error(err.getMessage(), err)
                );

        this.onControlEvent(controlNameToWatch, EventTypes.WINDOW_HIDDEN)
                .subscribe(
                        e -> m_panelIsVisible.onNext(false),
                        err -> logger.error(err.getMessage(), err)
                );
    }

    /**
     * Provides an observable triggered when the panel visibility changes.
     *
     * @return
     */
    protected Observable<Boolean> getPanelIsVisible() {
        return m_panelIsVisible
                .distinctUntilChanged();
    }

    protected void initFunctionAccess() throws java.lang.Exception {
        Object functionAccessO = this.m_componentContext
                .getServiceManager()
                .createInstanceWithContext("com.sun.star.sheet.FunctionAccess", m_componentContext);

        m_functionAccess = UnoRuntime.queryInterface(XFunctionAccess.class, functionAccessO);
    }

    protected Optional<String[]> getCurrentSpreadsheetNames() {
        return this.getMaybeSpreadsheetDocument()
                .map(
                        maybeSpreadsheetDocument -> maybeSpreadsheetDocument.map(
                                spreadsheetDocument -> spreadsheetDocument
                                        .getSheets()
                                        .getElementNames()
                        )
                )
                .blockingFirst();
    }

    protected Observable<Deque<CellAddress>> getCellSelectionHistory() {
        return this.getMaybeCurrentlySelectedCell()
                .filter(maybeCurrentlySelectedCell -> maybeCurrentlySelectedCell.isPresent())
                .map(maybeCurrentlySelectedCell -> maybeCurrentlySelectedCell.get())
                .scan(new ArrayDeque<>(), (previouslySelectedCellStack, newCell) -> {
                    // Keep a stack of previously selected cells and emit it every time a new cell is selected.
                    // We can then go through the list of previously selected cells until we find a cell which
                    // currently contains an entity.
                    XCellAddressable cellAddressable = UnoRuntime.queryInterface(XCellAddressable.class, newCell);
                    CellAddress newCellAddress = cellAddressable.getCellAddress();

                    boolean shouldAddCellToStack = previouslySelectedCellStack.isEmpty()
                            || !MwnciHelpers.cellAddressEquals(previouslySelectedCellStack.peek(), newCellAddress);

                    if (shouldAddCellToStack) {
                        previouslySelectedCellStack.push(newCellAddress);
                    }
                    return previouslySelectedCellStack;
                });
    }

    protected CellAddress getCellAddress(XCell selectedCell) {
        XCellAddressable cellAddressable = UnoRuntime.queryInterface(XCellAddressable.class, selectedCell);
        return cellAddressable.getCellAddress();
    }
}