package cymru.mwnci.officeinterface.controllers;

import com.sun.star.awt.XComboBox;
import com.sun.star.awt.XControl;
import com.sun.star.awt.XControlModel;
import com.sun.star.awt.XListBox;
import com.sun.star.beans.UnknownPropertyException;
import com.sun.star.beans.XPropertySet;
import com.sun.star.frame.XDesktop2;
import com.sun.star.frame.theDesktop;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.script.AllEventObject;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.MwnciDialog;
import cymru.mwnci.officeinterface.MwnciHelpers;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public abstract class ReactiveBaseController implements IController {
    private static final Logger logger = LogManager.getLogger(ReactiveBaseController.class);

    private HashMap<String, XControl> m_mapNameToXControl = new HashMap<String, XControl>();
    private HashMap<String, HashMap<String, PublishSubject<AllEventObject>>> m_mapControlNameToMapEventTypeToSubject =
            new HashMap<String, HashMap<String, PublishSubject<AllEventObject>>>();

    protected XDesktop2 Desktop;

    protected final XComponentContext m_componentContext;

    protected ReactiveBaseController(XComponentContext componentContext) {
        this.m_componentContext = componentContext;
        Desktop = theDesktop.get(this.m_componentContext);

    }

    public void registerComponent(String componentName, XControl control) {
        this.m_mapNameToXControl.put(componentName, control);
    }

    protected XControl getControl(String controlName) {
        return this.m_mapNameToXControl.get(controlName);
    }

    /**
     * Register a function to be run when a control element fires an eventType.
     *
     * @param controlName The control element which is the subject of the event.
     * @param eventType   The event type.
     * @return
     */
    protected Observable<AllEventObject> onControlEvent(String controlName, String eventType) {

        if (!m_mapControlNameToMapEventTypeToSubject.containsKey(controlName))
            m_mapControlNameToMapEventTypeToSubject
                    .put(controlName, new HashMap<String, PublishSubject<AllEventObject>>());

        HashMap<String, PublishSubject<AllEventObject>> mapEventTypeToAction =
                m_mapControlNameToMapEventTypeToSubject.get(controlName);

        if (!mapEventTypeToAction.containsKey(eventType)) {
            PublishSubject<AllEventObject> eventSubject = PublishSubject.create();
            mapEventTypeToAction.put(eventType, eventSubject);
        }

        PublishSubject<AllEventObject> subject = mapEventTypeToAction.get(eventType);
        return subject.debounce(10, TimeUnit.MILLISECONDS);
    }

    public void handleEvent(String controlName, String eventType, AllEventObject event) {
        logger.trace(controlName + ": " + eventType);

        if (!m_mapControlNameToMapEventTypeToSubject.containsKey(controlName))
            return;

        HashMap<String, PublishSubject<AllEventObject>> mapEventTypeToAction =
                m_mapControlNameToMapEventTypeToSubject.get(controlName);

        if (!mapEventTypeToAction.containsKey(eventType))
            return;

        try {
            mapEventTypeToAction.get(eventType)
                    .onNext(event); // Got to pass some object through.
        } catch (java.lang.Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    protected Observable<String> getTextFieldValue(String controlName) {
        XControl textField_control = this.getControl(controlName);
        XControlModel textField_controlModel = textField_control.getModel();
        XPropertySet textField_propertySet = UnoRuntime.queryInterface(XPropertySet.class, textField_controlModel);

        return this.onControlEvent(controlName, EventTypes.TEXT_CHANGED)
                .map(event -> (String) textField_propertySet.getPropertyValue("Text"));
    }

    protected void HandleRxError(Throwable err) {
        logger.error(err.getMessage(), err);
    }

    protected XPropertySet getPropertySetForControl(String controlName) {
        XControl control = this.getControl(controlName);
        XControlModel controlModel = control.getModel();
        return UnoRuntime.queryInterface(XPropertySet.class, controlModel);
    }

    protected void clearComboBoxItems(XComboBox comboBox) {
        short existingItemCount = comboBox.getItemCount();
        comboBox.removeItems((short) 0, existingItemCount);
    }

    protected void clearListBoxItems(XListBox listBox) {
        short existingItemCount = listBox.getItemCount();
        listBox.removeItems((short) 0, existingItemCount);
    }


    protected Observable<Optional<String>> getComboBoxValueUserInput(String controlName, XPropertySet comboBox_propertySet)
            throws WrappedTargetException, UnknownPropertyException
    {
        String initialValue = (String)comboBox_propertySet.getPropertyValue("Text");
        Optional<String> maybeInitialValue = mapStringValueToMaybeValue(initialValue);

        Observable<Optional<String>> rawTextObservable = this.getTextFieldValue(controlName)
                .map(this::mapStringValueToMaybeValue)
                .startWith(maybeInitialValue);

        return rawTextObservable;
    }

    protected Optional<String> mapStringValueToMaybeValue(String value) {
        Optional<String> maybeValue;
        if (value == null || value.isEmpty()) {
            maybeValue = Optional.empty();
        } else {
            maybeValue = Optional.of(value);
        }

        return maybeValue;
    }

    protected String getOutputLanguage() throws Exception {
        return MwnciHelpers.getOutputLanguage(m_componentContext);
    }

    protected void launchMessageModal(String message) {
        try {
            MwnciDialog.loadDialog(
                    "/resources/dialogs/messageBox/MessageBox.xdl",
                    this.m_componentContext,
                    new MessageBoxController(this.m_componentContext, message)
            );
        } catch (java.lang.Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    protected String getUserErrorMessage(Throwable err) {
        return "Error: " + err.getMessage();
    }

    protected void informUserOfError(Throwable err) {
        String errorMessage = getUserErrorMessage(err);
        launchMessageModal(errorMessage);
    }
}