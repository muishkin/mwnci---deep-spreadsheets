package cymru.mwnci.officeinterface.controllers;

/**
 * A cymru.mwnci.officeinterface.tests.helper class containing some of the known event types which can be fired by some simple XControls.
 */
public class EventTypes {
    public static final String ACTION_PERFORMED = "actionPerformed";
    public static final String FOCUS_GAINED = "focusGained";
    public static final String FOCUS_LOST = "focusLost";
    public static final String KEY_PRESSED = "keyPressed";
    public static final String KEY_RELEASED = "keyReleased";
    public static final String MOUSE_DRAGGED = "mouseDragged";
    public static final String MOUSE_ENTERED = "mouseEntered";
    public static final String MOUSE_EXITED = "mouseExited";
    public static final String MOUSE_MOVED = "mouseMoved";
    public static final String MOUSE_PRESSED = "mousePressed";
    public static final String MOUSE_RELEASED = "mouseReleased";
    public static final String TEXT_CHANGED = "textChanged";
    public static final String WINDOW_HIDDEN = "windowHidden";
    public static final String WINDOW_SHOWN = "windowShown";

    private EventTypes() {
    }
}
