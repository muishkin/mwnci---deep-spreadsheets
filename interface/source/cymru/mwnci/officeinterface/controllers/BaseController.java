package cymru.mwnci.officeinterface.controllers;

import com.sun.star.awt.XControl;
import com.sun.star.script.AllEventObject;
import cymru.mwnci.officeinterface.exceptions.DuplicateRequestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;

public abstract class BaseController implements IController {

    private static final Logger logger = LogManager.getLogger(BaseController.class);

    private HashMap<String, XControl> m_mapNameToXControl = new HashMap<>();
    private HashMap<String, HashMap<String, Runnable>> m_mapControlNameToMapEventTypeToAction =
            new HashMap<>();

    public void registerComponent(String componentName, XControl control) {
        this.m_mapNameToXControl.put(componentName, control);
    }

    protected XControl getControl(String controlName) {
        return this.m_mapNameToXControl.get(controlName);
    }

    /**
     * Register a function to be run when a control element fires an eventType.
     * @param controlName The control element which is the subject of the event.
     * @param eventType The event type.
     * @param funToRun The function to fun.
     * @throws DuplicateRequestException When attempting to register for an existing (controlName, eventType) config.
     */
    protected void onControlEvent(String controlName, String eventType, Runnable funToRun)
            throws DuplicateRequestException {

        if (!m_mapControlNameToMapEventTypeToAction.containsKey(controlName))
            m_mapControlNameToMapEventTypeToAction.put(controlName, new HashMap<String, Runnable>());

        HashMap<String, Runnable> mapEventTypeToAction = m_mapControlNameToMapEventTypeToAction.get(controlName);

        if (mapEventTypeToAction.containsKey(eventType))
            throw new DuplicateRequestException();

        mapEventTypeToAction.put(eventType, funToRun);
    }

    public void handleEvent(String controlName, String eventType, AllEventObject event) {
        if (!m_mapControlNameToMapEventTypeToAction.containsKey(controlName))
            return;

        HashMap<String, Runnable> mapEventTypeToAction = m_mapControlNameToMapEventTypeToAction.get(controlName);

        if (!mapEventTypeToAction.containsKey(eventType))
            return;

        try {
            mapEventTypeToAction.get(eventType)
                    .run();
        }catch(Exception e){
            logger.error(e.getMessage(), e);
        }
    }
}