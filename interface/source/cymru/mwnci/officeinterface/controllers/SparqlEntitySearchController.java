package cymru.mwnci.officeinterface.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.star.awt.XComboBox;
import com.sun.star.awt.XControl;
import com.sun.star.beans.XPropertySet;
import com.sun.star.table.CellAddress;
import com.sun.star.table.XCell;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.MwnciHelpers;
import cymru.mwnci.officeinterface.MwnciInterfaceImpl;
import cymru.mwnci.officeinterface.models.SearchParamsWithCell;
import cymru.mwnci.officeinterface.models.SubjectSearchParams;
import cymru.mwnci.sparql.Mwnci;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.models.UserSubjectEntityFilter;
import cymru.mwnci.sparql.services.Interfaces.ISparqlProvider;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SparqlEntitySearchController extends SparqlSearchBaseController<SubjectSearchParams> {

    /**
     * We need to ensure that anything which writes to this.m_searchParams runs on this single thread
     * to ensure that the order of execution is predictable.
     * <p>
     * You should both ObserveOn & SubscribeOn this thread.
     */
    public static final Scheduler RX_THREAD = Schedulers.single();

    /**
     * Component names:
     * <p>
     * Label_SourceSelector
     * ComboBox_SparqlQuerySourceSelector
     * <p>
     * Label_EntityFilter
     * ComboBox_EntityFilter
     * <p>
     * Label_SearchBox
     * TextField_SearchText
     * <p>
     * ListBox_Results
     */

    private static Logger logger = LogManager.getLogger(SparqlEntitySearchController.class);

    /**
     * Search data will be inputted in the formula:
     * `="<VISIBLE_ENTITY_INFO>" & T(n("<SEARCH_DATA_JSON>"))`
     * <p>
     * This will give the user access to the visible entity information whilst keeping the search settings
     * available for future searching.
     */
    private static final Pattern SEARCH_DATA_PATTERN = Pattern.compile(
            "^=\\s*.*\\s*SPARQLENTITY\\(.*?\\)\\s*&\\s*T\\(n\\(\\\"(.*?)\\\"\\)\\)\\s*$",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.UNICODE_CHARACTER_CLASS
    );

    private static Subject<Optional<UserSubjectEntityFilter>> m_latestFilterForSparqlSource =
            getLatestFilterForSparqlSourceObservable();

    public SparqlEntitySearchController(XComponentContext componentContext) {
        super(componentContext);
    }

    private static Subject<Optional<UserSubjectEntityFilter>> getLatestFilterForSparqlSourceObservable() {
        Subject<Optional<UserSubjectEntityFilter>> sub = BehaviorSubject.create();

        sub.onNext(Optional.empty());

        return sub;
    }

    @Override
    public void init() {
        try {
            // Pull any saved search params from the currently selected cell.
            super.init();

            initEntityFilterComboBox();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    @Override
    protected Scheduler getStaticRxThreadForController() {
        return RX_THREAD;
    }

    @Override
    protected Observable<List<EntityWithUri>> getSearchResultsWithParams(
            SubjectSearchParams searchParams) {
        return Observable.fromCallable(() -> {
            ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider(
                    searchParams.SparqlDataSource.getServiceName());

            ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);

            return sparqlService.searchForSubject(
                    sparqlProvider,
                    searchParams.SearchTerm,
                    getOutputLanguage(),
                    searchParams.EntityFilter.getSparqlStatement(),
                    200
            );
        })
                .observeOn(ioScheduler)
                .subscribeOn(ioScheduler);
    }

    protected boolean updateResultsOnlyWhen(SearchParamsWithCell<SubjectSearchParams> paramsWithCell) {
        SubjectSearchParams searchParams = paramsWithCell.getSearchParams();
        return searchParams.SparqlDataSource != null
                && searchParams.EntityFilter != null;
    }


    protected SearchParamsWithCell<SubjectSearchParams> getSearchParamsFromCell(XCell selectedCell) {
        CellAddress selectedCellAddress = getCellAddress(selectedCell);

        try {
            String formula = selectedCell.getFormula();
            if (formula != null && !formula.isEmpty()) {
                formula = formula.replace("\"\"", "\"");

                Matcher matcher = SEARCH_DATA_PATTERN.matcher(formula);
                if (matcher.matches()) {
                    String searchParamsJson = matcher.group(1);

                    SubjectSearchParams params = objectMapper.readValue(searchParamsJson, SubjectSearchParams.class);
                    params.setShouldBePersistedToCell(true);
                    return new SearchParamsWithCell<>(params, selectedCellAddress);
                }
            }

        } catch (java.lang.Exception e) {
            logger.error(e.getMessage(), e);
        }

        SubjectSearchParams params = getNewParamsObject(selectedCell);
        return new SearchParamsWithCell<>(params, selectedCellAddress);
    }

    protected String uniqueSearchParamIdentifier(SubjectSearchParams searchParams) {
        StringBuilder id = new StringBuilder();

        if (searchParams.SparqlDataSource != null)
            id.append(searchParams.SparqlDataSource.getServiceName());

        id.append("/");

        if (searchParams.SearchTerm != null)
            id.append(searchParams.SearchTerm);

        id.append("/");

        if (searchParams.EntityFilter != null)
            id.append(searchParams.EntityFilter.getSparqlStatement());

        return id.toString();
    }


    @Override
    protected SubjectSearchParams getNewParamsObject(XCell selectedCell) {
        SubjectSearchParams params = new SubjectSearchParams();

        // Pick up the existing cell text and use it as a search term.
        String existingCellText = MwnciHelpers.getTextFromCell(selectedCell);
        params.SearchTerm = existingCellText;

        params.SparqlDataSource = getSuggestedSparqlProviderUserInfo();

        m_latestFilterForSparqlSource
                .blockingFirst()
                .ifPresent(filter -> params.EntityFilter = filter);

        trySetSparqlDataSourceForCell(params, existingCellText);

        return params;
    }

    protected SubjectSearchParams getNewParamsObjectForError() {
        return new SubjectSearchParams();
    }

    protected String getSerialisedSearchParamsFormula(SubjectSearchParams searchParams) throws JsonProcessingException {
        String displayTextFormula;
        if (searchParams.Entity == null) {
            displayTextFormula = "\"Incomplete Search\"";
        } else {
            // Need to use a fully qualified function name here else we encounter the following bug:
            // https://bugs.documentfoundation.org/show_bug.cgi?id=54854
            String sparqlEntityQualifiedFunc = MwnciInterfaceImpl.IMPLEMENTATION_NAME + ".SPARQLENTITY";
            displayTextFormula = sparqlEntityQualifiedFunc
                    + "(\"" + searchParams.Entity.getUri()
                    // need to use ';' as param delimeter
                    // else we run in to bug https://bugs.documentfoundation.org/show_bug.cgi?id=78929
                    + "\"; \"" +
                    searchParams.SparqlDataSource.getServiceName() + "\")";
        }
        String searchParamsJson = objectMapper.writeValueAsString(searchParams)
                .replace("\"", "\"\"");

        StringBuilder formulaSb = new StringBuilder("=");

        formulaSb.append(displayTextFormula);

        formulaSb.append(" & ");

        formulaSb.append("T(N(\"" + searchParamsJson + "\"))");

        return formulaSb.toString();
    }


    private void initEntityFilterComboBox() {
        String controlName = "ComboBox_EntityFilter";
        XControl comboBox_entityFilter_control = this.getControl(controlName);
        XComboBox comboBox_entityFilter =
                UnoRuntime.queryInterface(XComboBox.class, comboBox_entityFilter_control);

        XPropertySet comboBox_entityFilter_propertySet = getPropertySetForControl(controlName);

        List<UserSubjectEntityFilter> availableEntityFilters = new LinkedList<>();
        // Set the available drop-down options when the SPARQL data-source changes.
        Observable<SearchParamsWithCell<SubjectSearchParams>> reactToDataSourceChange =
                this.getSearchParamsWithCellThreadSafe()
                        .distinctUntilChanged(searchParamsWithCell ->
                                searchParamsWithCell.getSearchParams().SparqlDataSource)
                        .doOnNext(searchParamsWithCell -> {
                            this.clearComboBoxItems(comboBox_entityFilter);

                            SubjectSearchParams searchParams = searchParamsWithCell.getSearchParams();
                            if (searchParams.SparqlDataSource != null) {
                                String sparqlProviderName = searchParams.SparqlDataSource.getServiceName();

                                availableEntityFilters.clear();

                                ISparqlProvider sparqlProvider =
                                        Mwnci.getSparqlProvider(sparqlProviderName);
                                List<UserSubjectEntityFilter> sparqlProviderFilters =
                                        sparqlProvider.getUserSubjectFilters();

                                availableEntityFilters.addAll(sparqlProviderFilters);

                                short i = 0;
                                for (UserSubjectEntityFilter entityFilter : availableEntityFilters) {
                                    comboBox_entityFilter.addItem(entityFilter.getDisplayName(), i);
                                    i++;
                                }

                                // Set the default selected option.
                                UserSubjectEntityFilter defaultOption = availableEntityFilters
                                        .get(0);

                                String defaultOptionText = defaultOption.getDisplayName();
                                comboBox_entityFilter_propertySet.setPropertyValue("Text", defaultOptionText);

                                SearchParamsWithCell<SubjectSearchParams> newParamsWithCell =
                                        searchParamsWithCell.deepCopy();
                                SubjectSearchParams newParams = newParamsWithCell.getSearchParams();
                                newParams.EntityFilter = defaultOption;

                                this.m_searchParamsWithCell.onNext(newParamsWithCell);
                            }
                        });

        Observable<SubjectSearchParams> reactToEntityFilterChange = this.getSearchParamsReadyThreadSafe()
                .distinctUntilChanged(searchParams -> searchParams.EntityFilter)
                .doOnNext(searchParams -> {
                    // Keep m_latestFilterForSparqlSource up to date with the selected option.
                    m_latestFilterForSparqlSource.onNext(
                            Optional.ofNullable(searchParams.EntityFilter)
                    );

                    // Keep the Combobox up to date with the selection.
                    if (searchParams.EntityFilter == null) {
                        comboBox_entityFilter_propertySet.setPropertyValue("Text", "");
                    } else {
                        comboBox_entityFilter_propertySet.setPropertyValue("Text",
                                searchParams.EntityFilter.getDisplayName());
                    }
                });

        Observable<SearchParamsWithCell<SubjectSearchParams>> keepWatchOfEntityFilterChanges =
                this.getSearchParamsWithCellThreadSafe()
                        // Update the searchParams with user selected item from drop-down
                        .switchMap(searchParamsWithCell ->
                                this.getTextFieldValue(controlName)
                                        .debounce(500, TimeUnit.MILLISECONDS)
                                        .map(userSelectedEntityFilter -> {
                                            SearchParamsWithCell<SubjectSearchParams> newParamsWithCell =
                                                    searchParamsWithCell.deepCopy();

                                            SubjectSearchParams newParams = newParamsWithCell.getSearchParams();

                                            // Map the displayName back to the filter so it can be persisted.
                                            UserSubjectEntityFilter newFilter = null;
                                            for (UserSubjectEntityFilter filter : availableEntityFilters) {
                                                if (filter.getDisplayName().equals(userSelectedEntityFilter)) {
                                                    newFilter = filter;
                                                    break;
                                                }
                                            }

                                            if (newFilter == null) {
                                                newFilter = new UserSubjectEntityFilter(
                                                        userSelectedEntityFilter,
                                                        userSelectedEntityFilter
                                                );
                                            }

                                            newParams.EntityFilter = newFilter;
                                            newParams.setShouldBePersistedToCell(true);

                                            return newParamsWithCell;
                                        })
                        )
                        .doOnNext(newParamsWithCell -> this.m_searchParamsWithCell.onNext(newParamsWithCell));

        Observable.combineLatest(
                reactToDataSourceChange, reactToEntityFilterChange, keepWatchOfEntityFilterChanges,
                (a, b, c) -> new Object[]{a, b, c}
        )
                .doOnError(err -> {
                    informUserOfError(err);

                    logErrorSetNewParams(err);
                    initEntityFilterComboBox();
                })
                .subscribe();
    }
}
