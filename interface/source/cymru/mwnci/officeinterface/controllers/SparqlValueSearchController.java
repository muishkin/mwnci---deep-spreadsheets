package cymru.mwnci.officeinterface.controllers;

import com.sun.star.beans.XPropertySet;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.sheet.XSpreadsheet;
import com.sun.star.table.CellAddress;
import com.sun.star.table.XCell;
import com.sun.star.table.XCellRange;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.MwnciHelpers;
import cymru.mwnci.officeinterface.MwnciInterfaceImpl;
import cymru.mwnci.officeinterface.exceptions.CellNotFoundException;
import cymru.mwnci.officeinterface.exceptions.SheetsNotFoundException;
import cymru.mwnci.officeinterface.exceptions.UriNotPresentException;
import cymru.mwnci.officeinterface.models.PropertySearchParams;
import cymru.mwnci.officeinterface.models.SearchParamsWithCell;
import cymru.mwnci.officeinterface.protocolhandler.MwnciStatusListener;
import cymru.mwnci.sparql.Mwnci;
import cymru.mwnci.sparql.exceptions.EntityNotFoundException;
import cymru.mwnci.sparql.exceptions.TooManyValuesFoundException;
import cymru.mwnci.sparql.helpers.SparqlIriHelpers;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.services.Interfaces.ISparqlProvider;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URI;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Optional.empty;

public class SparqlValueSearchController extends SparqlSearchBaseController<PropertySearchParams> {

    /**
     * Component names:
     * <p>
     * Label_SubjectCellRef
     * TextField_SubjectCellRef
     * Label_SubjectCellRefFeedback
     * <p>
     * Label_SourceSelector
     * ComboBox_SparqlQuerySourceSelector
     * <p>
     * Label_SearchBox
     * TextField_SearchText
     * <p>
     * ListBox_Results
     */

    private static Logger logger = LogManager.getLogger(SparqlValueSearchController.class);

    /**
     * We need to ensure that anything which writes to this.m_searchParams runs on this single thread
     * to ensure that the order of execution is predictable.
     * <p>
     * You should both ObserveOn & SubscribeOn this thread.
     */
    public static final Scheduler RX_THREAD = Schedulers.single();

    /**
     * Search data will be inputted in the formula:
     * `=sparqlValue(Cell/Entity ref, SparqlProvider, Property, PropertySearchLanguage)`
     * <p>
     * This will give the user access to the visible entity information whilst keeping the search settings
     * available for future searching.
     */
    private static final Pattern SEARCH_DATA_PATTERN = Pattern.compile(
            "^=.*?sparqlValue\\s*\\((.*?[,;]){5}\\s*\"(.*?)\"\\s*\\)\\s*$",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.UNICODE_CHARACTER_CLASS
    );

    private XPropertySet textField_subjectCellRef_propertySet;
    private Subject<Optional<XCell>> m_maybeLastSelectedSparqlEntityCell;
    private MwnciStatusListener m_mwnciStatusListener;

    public SparqlValueSearchController(XComponentContext componentContext) {
        super(componentContext);
    }

    @Override
    public void init() {
        try {
            super.init();
            initSubjectCellRefTextBox();
            watchLastVisitedSparqlEntityCell();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private void watchLastVisitedSparqlEntityCell() {
        try {
            m_maybeLastSelectedSparqlEntityCell = BehaviorSubject.create();
            m_maybeLastSelectedSparqlEntityCell.onNext(empty());

            m_mwnciStatusListener = MwnciStatusListener.getForMwnciProtocolHandler(this.m_componentContext);

            Observable<Deque<CellAddress>> previouslySelectedCells = getCellSelectionHistory();

            Observable<Boolean> mwnciRequestsHaveStopped = m_mwnciStatusListener
                    .getRequestAreActiveObservable()
                    .observeOn(rxThread)
                    .subscribeOn(rxThread)
                    .filter(requestsAreActive -> !requestsAreActive);

            Observable.combineLatest(previouslySelectedCells, mwnciRequestsHaveStopped, (a, b) -> new Object[]{a, b})
                    .observeOn(rxThread)
                    .subscribeOn(rxThread)
                    .map(values -> {
                        // this gets triggered when the selected cell changes or when the value in the selected cell
                        // changes.
                        logger.debug("Processing entity cell update check.");
                        Deque<CellAddress> previouslySelectedCellStack = (Deque<CellAddress>) values[0];

                        // Deque used so that iteration proceeds from most recent selected value first.
                        for (CellAddress cellAddress : previouslySelectedCellStack) {
                            Optional<XCell> maybeCell = getCellByAddress(cellAddress);

                            if (!maybeCell.isPresent())
                                continue;

                            XCell cell = maybeCell.get();

                            String cellText = MwnciHelpers.getTextFromCell(cell);
                            Optional<URI> maybeCellUri = SparqlIriHelpers.getEntityUriFromString(cellText);

                            if (maybeCellUri.isPresent()) {
                                return Optional.of(cell);
                            }
                        }

                        return Optional.ofNullable((XCell) null);
                    })
                    .distinctUntilChanged()
                    .doOnError(err -> {
                        informUserOfError(err);

                        logger.error(err.getMessage(), err);

                        watchLastVisitedSparqlEntityCell();
                    })
                    .subscribe(
                            maybeEntityCell -> m_maybeLastSelectedSparqlEntityCell.onNext(maybeEntityCell),
                            err -> logger.error(err.getMessage(), err)
                    );
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    private void initSubjectCellRefTextBox() {
        String controlName = "TextField_SubjectCellRef";
        textField_subjectCellRef_propertySet = this.getPropertySetForControl(controlName);

        XPropertySet label_subjectCellRefFeedback_PropertySet =
                this.getPropertySetForControl("Label_SubjectCellRefFeedback");

        this.getSearchParamsWithCellThreadSafe()
                .doOnNext(searchParamsWithCell -> {
                    PropertySearchParams searchParams = searchParamsWithCell.getSearchParams();
                    if (searchParams.SubjectCell == null) {
                        label_subjectCellRefFeedback_PropertySet.setPropertyValue(
                                "Label",
                                "Invalid Reference"
                        );
                        textField_subjectCellRef_propertySet.setPropertyValue("Text", "");
                    } else {
                        String cellRef = getHumanReadableCellRangeFromCellAddress(searchParams.SubjectCell);
                        textField_subjectCellRef_propertySet.setPropertyValue("Text", cellRef);

                        setSubjectEntityHelperLabel(label_subjectCellRefFeedback_PropertySet, searchParams.SubjectCell);
                    }
                })
                .switchMap(searchParamsWithCell ->
                        this.getTextFieldValue(controlName)
                                .debounce(500, TimeUnit.MILLISECONDS)
                                .map(cellRef -> {
                                    SearchParamsWithCell<PropertySearchParams> newParamsWithCell =
                                            searchParamsWithCell.deepCopy();

                                    PropertySearchParams newParams = newParamsWithCell.getSearchParams();

                                    // Map from user input to CellAddress
                                    try {
                                        newParams.SubjectCell = getCellAddressFromUserRefInput(cellRef);
                                    } catch (java.lang.Exception ex) {
                                        newParams.SubjectCell = null;
                                    }

                                    newParams.setShouldBePersistedToCell(true);

                                    return newParamsWithCell;
                                })
                )
                .doOnError(err -> {
                    informUserOfError(err);

                    logErrorSetNewParams(err);
                    initSubjectCellRefTextBox();
                })
                .subscribe(
                        newParams -> this.m_searchParamsWithCell.onNext(newParams),
                        err -> logger.error(err.getMessage(), err)
                );
    }

    /**
     * Sets a helper label in the UI to inform the user which entity the listed properties are associated with.
     *
     * @param label_PropertySet Label property set on which the label will be updated.
     * @param subjectCell       The cell currently under investigation.
     * @throws Exception
     */
    private void setSubjectEntityHelperLabel(XPropertySet label_PropertySet, CellAddress subjectCell)
            throws Exception {

        ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);

        // Issue #3 Get the entity label and stick it in the below label field.
        Optional<URI> maybeUri = getCellByAddress(subjectCell)
                .map(cell -> MwnciHelpers.getTextFromCell(cell))
                .flatMap(displayText -> SparqlIriHelpers.getEntityUriFromString(displayText));

        ISparqlProvider sparqlProvider = maybeUri
                .flatMap(uri -> Mwnci.getSparqlProviderforUri(uri))
                .orElseGet(() -> getSuggestedSparqlProvider());

        if (maybeUri.isPresent()) {
            URI uri = maybeUri.get();

            String language = getOutputLanguage();
            try {
                EntityWithUri entityWithUri = sparqlService.getEntityForUri(
                        sparqlProvider, uri, language);

                label_PropertySet.setPropertyValue(
                        "Label", entityWithUri.getLabel());
            } catch (EntityNotFoundException ex) {
                label_PropertySet.setPropertyValue("Label", "Could not find entity.");
            } catch (TooManyValuesFoundException ex) {
                label_PropertySet.setPropertyValue("Label", "Too many entities found.");
            }
        } else {
            label_PropertySet.setPropertyValue("Label", "No entity found.");
        }
    }

    private CellAddress getCellAddressFromUserRefInput(String cellRef)
            throws IndexOutOfBoundsException, CellNotFoundException {
        Optional<XSpreadsheet> maybeCurrentSpreadsheet = this.getMaybeCurrentActiveSheet();

        if (!maybeCurrentSpreadsheet.isPresent()) {
            throw new CellNotFoundException(cellRef);
        }

        XSpreadsheet currentSpreadsheet = maybeCurrentSpreadsheet.get();

        XCellRange cellRange = currentSpreadsheet.getCellRangeByName(cellRef);
        XCell cell = cellRange.getCellByPosition(0, 0);
        return getCellAddress(cell);
    }

    private String getHumanReadableCellRangeFromCellAddress(CellAddress cellAddress) throws Exception {

        Optional<String[]> maybeSheetNames = getCurrentSpreadsheetNames();

        if (!maybeSheetNames.isPresent()) {
            throw new SheetsNotFoundException();
        }

        String[] sheetNames = maybeSheetNames.get();

        // Documentation: https://wiki.openoffice.org/wiki/Documentation/How_Tos/Calc:_ADDRESS_function
        List<Object> params = new LinkedList<>();
        params.add(cellAddress.Row + 1);
        params.add(cellAddress.Column + 1);
        params.add(4); // Mode = 4 specifies to return the cell address as a "relative" A1 reference.
        params.add(1); // use A1 style rather than RC.

        String sheetName = sheetNames[cellAddress.Sheet];

        XSpreadsheet currentSheet = this.getMaybeCurrentActiveSheet()
                .get();

        XPropertySet currentSheet_propertySet = UnoRuntime.queryInterface(XPropertySet.class, currentSheet);
        String currentSheetName = (String) currentSheet_propertySet.getPropertyValue("CodeName");

        if (!currentSheetName.equals(sheetName)) {
            // If the current sheet isn't the one this cell is in, include it as a param to the function.
            // This helps ensure that the references only contain the necessary information.
            params.add(sheetName);
        }

        Object resultO = m_functionAccess.callFunction(
                "ADDRESS",
                params.toArray()
        );
        return resultO.toString();
    }

    @Override
    protected Scheduler getStaticRxThreadForController() {
        return RX_THREAD;
    }

    @Override
    protected Observable<List<EntityWithUri>> getSearchResultsWithParams(
            PropertySearchParams searchParams) {
        return Observable.fromCallable(() -> {
            ISparqlService sparqlService = Mwnci.getService(ISparqlService.class);
            ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider(
                    searchParams.SparqlDataSource.getServiceName());

            URI subjectUri = getSubjectUri(searchParams);

            List<EntityWithUri> results = sparqlService.searchPropertiesForEntity(
                    sparqlProvider,
                    subjectUri,
                    searchParams.SearchTerm,
                    searchParams.SearchLanguage,
                    getOutputLanguage(),
                    200
            );

            return results;
        })
                .observeOn(ioScheduler)
                .subscribeOn(ioScheduler);
    }


    private URI getSubjectUri(PropertySearchParams searchParams) throws CellNotFoundException, UriNotPresentException {
        Optional<XCell> maybeSpecifiedCell = this.getCellByAddress(searchParams.SubjectCell);
        if (!maybeSpecifiedCell.isPresent()) {
            throw new CellNotFoundException(searchParams.SubjectCell);
        }

        XCell subjectCell = maybeSpecifiedCell.get();
        String subjectCellTextContents = MwnciHelpers.getTextFromCell(subjectCell);
        Optional<URI> maybeSubjectUri = SparqlIriHelpers.getEntityUriFromString(subjectCellTextContents);

        if (!maybeSubjectUri.isPresent()) {
            throw new UriNotPresentException();
        }

        return maybeSubjectUri.get();
    }

    @Override
    protected String getSerialisedSearchParamsFormula(PropertySearchParams searchParams) throws Exception {
        StringBuilder formula = new StringBuilder("=");

        String fullyQualifiedFunctionName = MwnciInterfaceImpl.IMPLEMENTATION_NAME + ".sparqlValue";

        formula.append(fullyQualifiedFunctionName + "(");

        String subjectCellRef = searchParams.SubjectCell == null
                ? ""
                : getHumanReadableCellRangeFromCellAddress(searchParams.SubjectCell);

        formula.append(subjectCellRef);
        formula.append("; ");

        formula.append("\"" + searchParams.SparqlDataSource.getServiceName() + "\"");
        formula.append("; ");

        if (searchParams.Entity == null) {
            // Place the search term in to the method as a method of storage
            // Escape any quotation marks.
            formula.append("\"\"");
        } else {
            formula.append("\"" + searchParams.Entity.getUri() + "\"");
        }

        formula.append("; ");

        // Whether to allow one-to-many results param goes here. Leave as default.

        formula.append("; ");

        formula.append("\"" + searchParams.SearchLanguage + "\"");
        formula.append("; ");

        String modelJson = this.objectMapper.writeValueAsString(searchParams);
        // Escape quotation marks inside the model JSON.
        modelJson = modelJson.replace("\"", "\"\"");

        formula.append("\"" + modelJson + "\"");

        formula.append(") ");

        return formula.toString();
    }

    @Override
    protected boolean updateResultsOnlyWhen(SearchParamsWithCell<PropertySearchParams> searchParamsWithCell) {
        PropertySearchParams searchParams = searchParamsWithCell.getSearchParams();

        return searchParams.SparqlDataSource != null
                && searchParams.SubjectCell != null
                && searchParams.SearchLanguage != null && !searchParams.SearchLanguage.isEmpty();
    }

    @Override
    protected SearchParamsWithCell<PropertySearchParams> getSearchParamsFromCell(XCell selectedCell) {
        CellAddress selectedCellAddress = getCellAddress(selectedCell);

        try {
            String formula = selectedCell.getFormula();

            Matcher matcher = SEARCH_DATA_PATTERN.matcher(formula);
            if (matcher.matches()) {
                String modelJson = matcher.group(2);
                modelJson = modelJson.replace("\"\"", "\"");

                PropertySearchParams params = objectMapper.readValue(modelJson, PropertySearchParams.class);
                return new SearchParamsWithCell<>(params, selectedCellAddress);
            }
        } catch (java.lang.Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        PropertySearchParams params = getNewParamsObject(selectedCell);
        return new SearchParamsWithCell<>(params, selectedCellAddress);
    }

    protected String uniqueSearchParamIdentifier(PropertySearchParams searchParams) {
        StringBuilder id = new StringBuilder();

        if (searchParams.SubjectCell != null) {
            // Include the subject entityURI as part of the unique identifier where present.
            getCellByAddress(searchParams.SubjectCell)
                    .map(cell -> MwnciHelpers.getTextFromCell(cell))
                    .flatMap(cellText -> SparqlIriHelpers.getEntityUriFromString(cellText))
                    .ifPresent(uri -> id.append(uri.toString()));
        }

        id.append("/");

        if (searchParams.SparqlDataSource != null)
            id.append(searchParams.SparqlDataSource.getServiceName());

        id.append("/");

        if (searchParams.SearchTerm != null)
            id.append(searchParams.SearchTerm);

        id.append("/");

        if (searchParams.SearchLanguage != null)
            id.append(searchParams.SearchLanguage);

        return id.toString();
    }


    @Override
    protected PropertySearchParams getNewParamsObject(XCell selectedCell) {
        PropertySearchParams params = new PropertySearchParams();

        try {
            params.SearchLanguage = getOutputLanguage();
        } catch (Exception ex) {
            params.SearchLanguage = "en";
            logger.error(ex.getMessage(), ex);
        }

        // if there is a previously selected entity cell, we should use that as our starting point.
        this.m_maybeLastSelectedSparqlEntityCell
                .blockingFirst()
                .ifPresent(lastSelectedCell -> {
                    params.SubjectCell = getCellAddress(lastSelectedCell);

                    String existingCellText = MwnciHelpers.getTextFromCell(lastSelectedCell);
                    trySetSparqlDataSourceForCell(params, existingCellText);
                });

        return params;
    }

    protected PropertySearchParams getNewParamsObjectForError() {
        return new PropertySearchParams();
    }
}
