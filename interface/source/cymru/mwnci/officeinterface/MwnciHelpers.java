package cymru.mwnci.officeinterface;

import com.sun.star.awt.XControl;
import com.sun.star.awt.XControlModel;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.container.XNameAccess;
import com.sun.star.deployment.XPackageInformationProvider;
import com.sun.star.lang.Locale;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XLocalizable;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.script.XAllListener;
import com.sun.star.script.XEventAttacher2;
import com.sun.star.sheet.XFunctionAccess;
import com.sun.star.table.CellAddress;
import com.sun.star.table.XCell;
import com.sun.star.text.XText;
import com.sun.star.uno.AnyConverter;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.controllers.IController;
import cymru.mwnci.sparql.helpers.EntityHelpers;
import cymru.mwnci.sparql.models.EntityWithUri;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.datatype.XMLGregorianCalendar;

public class MwnciHelpers {

    private static final Logger logger = LogManager.getLogger(MwnciHelpers.class);

    private static final String[] EVENT_TYPES = new String[]{
            "XAccessibleEventListener",
            "XAnimationListener",
            "XGridColumnListener",
            "XGridDataListener",
            "XGridSelectionListener",
            "XTabPageContainerListener",
            "XTreeDataModelListener",
            "XTreeEditListener",
            "XTreeExpansionListener",
            "XActionListener",
            "XActivateListener",
            "XAdjustmentListener",
            "XDockableWindowListener",
            "XEnhancedMouseClickHandler",
            "XFocusListener",
            "XItemListener",
            "XItemListListener",
            "XKeyHandler",
            "XKeyListener",
            "XMenuListener",
            "XMouseClickHandler",
            "XMouseListener",
            "XMouseMotionHandler",
            "XMouseMotionListener",
            "XPaintListener",
            "XSpinListener",
            "XStyleChangeListener",
            "XTabListener",
            "XTextListener",
            "XTopWindowListener",
            "XVclContainerListener",
            "XWindowListener",
            "XPropertiesChangeListener",
            "XPropertyChangeListener",
            "XPropertySetInfoChangeListener",
            "XPropertyStateChangeListener",
            "XVetoableChangeListener",
            "XChartDataChangeEventListener",
            "XBackendChangesListener",
            "XContainerListener",
            "XClipboardListener",
            "XDragGestureListener",
            "XDragSourceListener",
            "XDropTargetListener",
            "XDocumentEventListener",
            "XEventListener",
            "XStorageChangeListener",
            "XUndoManagerListener",
            "XConfigurationChangeListener",
            "XStateChangeListener",
            "XTransactionListener",
            "XListEntryListener",
            "XFilterControllerListener",
            "XSubmissionVetoListener",
            "XFormComponentValidityListener",
            "XValidityConstraintListener",
            "XApproveActionListener",
            "XChangeListener",
            "XConfirmDeleteListener",
            "XDatabaseParameterListener",
            "XDeleteListener",
            "XErrorListener",
            "XFormControllerListener",
            "XGridControlListener",
            "XInsertListener",
            "XLoadListener",
            "XPositioningListener",
            "XResetListener",
            "XRestoreListener",
            "XSubmitListener",
            "XUpdateListener",
            "XBorderResizeListener",
            "XDispatchResultListener",
            "XFrameActionListener",
            "XLayoutManagerListener",
            "XLoadEventListener",
            "XSessionManagerListener",
            "XStatusListener",
            "XTerminateListener",
            "XTitleChangeListener",
            "XDataTransferEventListener",
            "XStreamListener",
            "XDictionaryEventListener",
            "XDictionaryListEventListener",
            "XLinguServiceEventListener",
            "XConnectionListener",
            "XShapeEventListener",
            "XVBAScriptListener",
            "XAllListener",
            "XEngineListener",
            "XScriptListener",
            "XCopyTableListener",
            "XDatabaseAccessListener",
            "XDatabaseRegistrationsListener",
            "XRowsChangeListener",
            "XRowSetApproveListener",
            "XRowSetChangeListener",
            "XSQLErrorListener",
            "XRowSetListener",
            "XActivationEventListener",
            "XRangeSelectionChangeListener",
            "XRangeSelectionListener",
            "XResultListener",
            "XJobListener",
            "XCommandInfoChangeListener",
            "XContentEventListener",
            "XDynamicResultSetListener",
            "XRemoteContentProviderChangeListener",
            "XDialogClosedListener",
            "XFilePickerListener",
            "XContextChangeEventListener",
            "XUIConfigurationListener",
            "XUIFunctionListener",
            "XChangesListener",
            "XCloseListener",
            "XFlushListener",
//            "XModeChangeApproveListener", // Causes introspection exceptions.
            "XModeChangeListener",
            "XModifyListener",
            "XRefreshListener",
            "XPrintableListener",
            "XPrintJobListener",
            "XSelectionChangeListener"
    };
    public static final int CALC_MIN_DATE_YEAR = 1583;
    public static final int CALC_MAX_DATE_YEAR = 9956;

    private MwnciHelpers() {
    }

    /**
     * Retrieves the package cymru.mwnci.officeinterface.tests.base file URL for the cymru.mwnci.officeinterface package.
     *
     * @param context The current context.
     * @return returns the file:// URL for the cymru.mwnci.officeinterface.tests.base of this package.
     * @throws NoSuchElementException
     * @throws WrappedTargetException
     */
    public static String getPackageBaseUrl(XComponentContext context)
            throws NoSuchElementException, WrappedTargetException {
        XPackageInformationProvider xPIP = getXPackageInformationProvider(context);
        return xPIP.getPackageLocation("cymru.mwnci.officeinterface");
    }

    /**
     * Retrieves the XPackageInformationProvider officeinterface for a given context.
     * Allows you to find information such as what the root directory of this package is (so you can find resources).
     *
     * @param context Context to get the XPackageInformationProvider from.
     * @return
     * @throws NoSuchElementException
     * @throws WrappedTargetException
     */
    private static XPackageInformationProvider getXPackageInformationProvider(XComponentContext context) throws NoSuchElementException, WrappedTargetException {
        XNameAccess nameAccess = UnoRuntime.queryInterface(XNameAccess.class, context);
        Object oPIP = nameAccess.getByName("/singletons/com.sun.star.deployment.PackageInformationProvider");
        return UnoRuntime.queryInterface(XPackageInformationProvider.class, oPIP);
    }

    /**
     * A method to get the list of control names from an XControlModel.
     *
     * @param controlModel The model.
     * @return The list of control names.
     */
    public static String[] getControlNames(XControlModel controlModel) {
        XNameAccess controlModel_nameAccess = UnoRuntime.queryInterface(XNameAccess.class, controlModel);
        return controlModel_nameAccess.getElementNames();
    }


    public static void setEventHandlers(XComponentContext componentContext, IController controller, XMultiComponentFactory serviceManager, String controlName, XControl control) throws Exception {

        Object actionListener_obj = serviceManager.createInstanceWithContext(
                "cymru.mwnci.officeinterface.MwnciEventHandler", componentContext);

        setControllerOnEventHandler(controller, (MwnciEventHandler) actionListener_obj);

        XAllListener actionListener = UnoRuntime.queryInterface(XAllListener.class, actionListener_obj);

        Object eventAttacher_obj = serviceManager.createInstanceWithContext(
                "com.sun.star.script.EventAttacher", componentContext);
        final XEventAttacher2 eventAttacher = UnoRuntime.queryInterface(XEventAttacher2.class, eventAttacher_obj);

        // helperInfo - Passes through the '.Helper' property to the event handler.
        String helperInfo = controlName;
        for (String eventType : EVENT_TYPES) {
            try {
                eventAttacher.attachListener(
                        control,
                        actionListener,
                        helperInfo,
                        eventType,
                        ""
                );
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
    }

    private static void setControllerOnEventHandler(IController controller, MwnciEventHandler actionListener_obj) {
        MwnciEventHandler actionListener_Mwnci = actionListener_obj;
        actionListener_Mwnci.setController(controller);
    }


    /**
     * Gets the SPARQL language string specifying which language output should be displayed in.
     *
     * @return
     */
    public static String getOutputLanguage(XComponentContext context) throws Exception {
        Locale locale = getLocale(context);
        return locale.Language;
    }

    public static Locale getLocale(XComponentContext context) throws Exception {

        XMultiComponentFactory serviceManager = context.getServiceManager();
        Object configurationProviderO = serviceManager.createInstanceWithContext(
                "com.sun.star.configuration.ConfigurationProvider", context);

        XLocalizable localizable = UnoRuntime.queryInterface(XLocalizable.class, configurationProviderO);

        return localizable.getLocale();
    }


    /**
     * Magic function which returns the displayed text from a given cell.
     *
     * @param cell The cell to get the text from.
     * @return Returns a string containing the text displayed to the user in a cell.
     */
    public static String getTextFromCell(XCell cell) {
        XText cellText = UnoRuntime.queryInterface(XText.class, cell);

        return cellText
                .createTextCursor()
                .getString();
    }

    public static String getDisplayTextForEntityWithUri(EntityWithUri entityWithUri) {
        return EntityHelpers.getDisplayTextForEntityWithUri(entityWithUri);
    }

    public static boolean cellAddressEquals(CellAddress a, CellAddress b) {
        return (a == null && b == null) ||
                (
                        a != null
                                && b != null
                                && a.Sheet == b.Sheet
                                && a.Column == b.Column
                                && a.Row == b.Row
                );
    }

    /**
     * Defaults to false is it cannot match a boolean.
     *
     * @param maybeBoolean
     * @return
     */
    public static boolean getBooleanFromUnoMaybeBoolean(Object maybeBoolean) {
        boolean value;

        if (AnyConverter.isBoolean(maybeBoolean)) {
            value = AnyConverter.toBoolean(maybeBoolean);
        } else if (AnyConverter.isDouble(maybeBoolean)) {
            value = AnyConverter.toDouble(maybeBoolean) > 0;
        } else if (AnyConverter.isString(maybeBoolean)) {
            value = Boolean.parseBoolean(AnyConverter.toString(maybeBoolean));
        } else {
            value = false;
        }
        return value;
    }


    public static Object mapXmlGregorianCalendarToCalcFormat(XFunctionAccess m_functionAccess,
                                                             XMLGregorianCalendar date) {
        /**
         */
        String dateTimeType = date.getXMLSchemaType()
                .getLocalPart()
                .toLowerCase();

        return getHumanReadableDateTimeString(date, dateTimeType);
        /**
         * WARNING
         *
         * Below is an attempt to use the 'proper' date/time system CALC supplied.
         * It ends up being a nightmare is makes it almost impossible to consistently format the holding-cell
         * properly. Maybe someone else will have better luck with it in the future?
         *
         *
         * See the following links for understanding of date/time representation in CALC:
         *
         * https://wiki.openoffice.org/wiki/Documentation/How_Tos/Calc:_Date_&_Time_functions
         * https://wiki.openoffice.org/wiki/Documentation/How_Tos/Calc:_DATE_function
         * https://wiki.openoffice.org/wiki/Documentation/How_Tos/Calc:_TIME_function
         */
        /*
        try {
            double dateTimeVal = 0;
            Object[] args;

            if (dateTimeType.contains("date")) {
                int year = date.getYear();
                if (year < CALC_MIN_DATE_YEAR || year > CALC_MAX_DATE_YEAR) {
                    return getHumanReadableDateTimeString(date, dateTimeType);
                }

                // Get date part of the date-time value.
                args = new Object[]{date.getYear(), date.getMonth(), date.getDay()};
                Object dateVal = m_functionAccess.callFunction("DATE", args);
                dateTimeVal += AnyConverter.toDouble(dateVal);
            }

            if (dateTimeType.contains("time")) {
                // Get time part of the date-time value.
                args = new Object[]{date.getHour(), date.getMinute(), date.getSecond()};
                Object timeVal = m_functionAccess.callFunction("TIME", args);
                dateTimeVal += AnyConverter.toDouble(timeVal);
            }

            if (dateTimeVal < 0){
                // CALC does some weird stuff.
                return getHumanReadableDateTimeString(date, dateTimeType);
            }

            return dateTimeVal;
        } catch (NoSuchElementException e) {
            logger.error(e.getMessage(), e);
        }

        return 0d;
        */
    }

    private static Object getHumanReadableDateTimeString(XMLGregorianCalendar date, String dateTimeType) {
        StringBuilder dtOutput = new StringBuilder();

        if (dateTimeType.contains("date")) {
            dtOutput.append(ensureNumDigitsPrinted(date.getYear(), 4));
            dtOutput.append("-");
            dtOutput.append(ensureNumDigitsPrinted(date.getMonth(), 2));
            dtOutput.append("-");
            dtOutput.append(ensureNumDigitsPrinted(date.getDay(), 2));
            dtOutput.append(" ");
        }

        if (dateTimeType.contains("time")) {
            dtOutput.append(ensureNumDigitsPrinted(date.getHour(), 2));
            dtOutput.append(":");
            dtOutput.append(ensureNumDigitsPrinted(date.getMinute(), 2));
            dtOutput.append(":");
            dtOutput.append(ensureNumDigitsPrinted(date.getSecond(), 2));
        }

        return dtOutput.toString();
    }

    /**
     * Provides a string with the necessary number of digits.
     *
     * @param value
     * @param minNumDigits
     * @return
     */
    private static String ensureNumDigitsPrinted(Number value, int minNumDigits) {
        String s = value.toString();
        int len = s.length();
        if (len < minNumDigits) {
            int leadingZerosRequired = minNumDigits - len;
            for (int i = 0; i < leadingZerosRequired; i++) {
                s = "0" + s;
            }
        }

        return s;
    }
}
