package cymru.mwnci.officeinterface;

import com.sun.star.awt.XControl;
import com.sun.star.awt.XDialog;
import com.sun.star.awt.XDialogProvider2;
import com.sun.star.awt.XUnoControlDialog;
import com.sun.star.container.NoSuchElementException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.controllers.IController;
import cymru.mwnci.officeinterface.controllers.IDialogController;

public class MwnciDialog {

    /**
     * Loads an Open/LibreOffice dialog (.xdg) and displays it as a dialog.
     *
     * @param relativePath Path relative to the package root.
     * @param context      The context for the modal to be created with.
     * @param controller Handler for actions which are triggered by the UI.
     * @throws NoSuchElementException
     * @throws WrappedTargetException
     * @throws Exception
     */
    public static void loadDialog(String relativePath, XComponentContext context, IDialogController controller)
            throws Exception {

        XUnoControlDialog dialog = getDialog(relativePath, context);

        registerDialogComponentsWithController(controller, dialog);

        setEventHandlers(context, controller, dialog);

        controller.setDialog(dialog);
        controller.init();

        dialog.execute();
    }

    private static void setEventHandlers(XComponentContext context, IController controller, XUnoControlDialog dialog) throws Exception {
        XMultiComponentFactory serviceManager = context.getServiceManager();

        String[] controlNames = MwnciHelpers.getControlNames(dialog.getModel());
        for(String controlName : controlNames){
            XControl control = dialog.getControl(controlName);
            MwnciHelpers.setEventHandlers(context, controller, serviceManager, controlName, control);
        }
    }

    private static void registerDialogComponentsWithController(IController controller, XUnoControlDialog dialog) {
        String[] dialogControlNames = MwnciHelpers.getControlNames(dialog.getModel());

        for(String controlName : dialogControlNames){
            XControl control = dialog.getControl(controlName);
            controller.registerComponent(controlName, control);
        }
    }

    public static XUnoControlDialog getDialog(String relativePath, XComponentContext context) throws Exception {
        String packageUrl = MwnciHelpers.getPackageBaseUrl(context);
        String dialogUiUrl = packageUrl + relativePath;

        XDialogProvider2 dialogProvider = getXDialogProvider2(context);
        XDialog xDialog = dialogProvider.createDialog(dialogUiUrl);

        return UnoRuntime.queryInterface(XUnoControlDialog.class, xDialog);
    }

    /**
     * Gets and XDialogProvider for the given context. Allows you to launch dialogs.
     *
     * @param context Context in which to launch the dialog.
     * @return
     * @throws Exception
     */
    private static XDialogProvider2 getXDialogProvider2(XComponentContext context) throws Exception {
        XMultiComponentFactory serviceManager = context.getServiceManager();
        Object oDialogProvider = serviceManager.createInstanceWithContext("com.sun.star.awt.DialogProvider2", context);
        return UnoRuntime.queryInterface(XDialogProvider2.class, oDialogProvider);
    }

}
