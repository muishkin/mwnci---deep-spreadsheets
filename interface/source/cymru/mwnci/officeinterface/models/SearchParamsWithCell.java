package cymru.mwnci.officeinterface.models;

import com.sun.star.table.CellAddress;
import cymru.mwnci.officeinterface.MwnciHelpers;

public class SearchParamsWithCell<T extends BaseSearchParams> {
    private T m_searchParams;
    private CellAddress m_cellAddress;

    protected SearchParamsWithCell() {
    }

    public SearchParamsWithCell(T searchParams, CellAddress cellAddress) {
        m_searchParams = searchParams;
        m_cellAddress = cellAddress;
    }

    public T getSearchParams() {
        return m_searchParams;
    }

    public void setSearchParams(T value) {
        m_searchParams = value;
    }

    public CellAddress getCellAddress() {
        return m_cellAddress;
    }

    public void setCellAddress(CellAddress value) {
        m_cellAddress = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SearchParamsWithCell))
            return false;

        SearchParamsWithCell other = (SearchParamsWithCell) obj;

        return MwnciHelpers.cellAddressEquals(this.m_cellAddress, other.m_cellAddress)
                && this.m_searchParams.equals(other.m_cellAddress);
    }

    @Override
    public int hashCode() {
        int hash = ~0;

        hash *= m_searchParams.hashCode();
        hash *= m_cellAddress.Sheet * m_cellAddress.Column * m_cellAddress.Row;

        return hash;
    }

    public <U extends SearchParamsWithCell<T>> void deepCopyPropertiesTo(U other) {
        CellAddress cellAddressCopy = m_cellAddress == null
                ? null
                : new CellAddress(m_cellAddress.Sheet, m_cellAddress.Column, m_cellAddress.Row);

        other.setCellAddress(cellAddressCopy);
        other.setSearchParams((T) m_searchParams.deepCopy());
    }

    public SearchParamsWithCell<T> deepCopy() {
        SearchParamsWithCell<T> copy = new SearchParamsWithCell<>();

        this.deepCopyPropertiesTo(copy);

        return copy;
    }

//    public SearchParamsWithCell<T> deepCopy() {
//        SearchParamsWithCell<T> copy = new SearchParamsWithCell<>();
//
//        this.deepCopyPropertiesTo(copy);
//
//        return copy;
//    }
}
