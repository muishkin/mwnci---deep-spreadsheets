package cymru.mwnci.officeinterface.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.star.table.CellAddress;
import cymru.mwnci.officeinterface.MwnciHelpers;

public class PropertySearchParams extends BaseSearchParams<PropertySearchParams> {

    public String SearchLanguage = "";
    public CellAddress SubjectCell = null;

    @Override
    @JsonIgnore
    public boolean isEmpty() {
        return super.isEmpty();
    }

    /**
     * Deep clone implementation.
     *
     * @return
     */
    public PropertySearchParams deepCopy() {
        PropertySearchParams other = new PropertySearchParams();

        super.copyBasePropertiesForClone(other);

        other.SearchLanguage = this.SearchLanguage;
        other.SubjectCell = this.SubjectCell == null
                ? null
                : new CellAddress(this.SubjectCell.Sheet, this.SubjectCell.Column, this.SubjectCell.Row);

        return other;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PropertySearchParams)) {
            return false;
        }

        PropertySearchParams other = (PropertySearchParams) obj;

        return super.equals(other)
                && MwnciHelpers.cellAddressEquals(this.SubjectCell, other.SubjectCell)
                && this.SearchLanguage.equals(other.SearchLanguage);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCodeBase();

        if (this.SubjectCell != null) {
            hash *= this.SubjectCell.Sheet;
            hash *= this.SubjectCell.Column;
            hash *= this.SubjectCell.Row;
        }

        hash *= this.SearchLanguage.hashCode();

        return hash;
    }
}
