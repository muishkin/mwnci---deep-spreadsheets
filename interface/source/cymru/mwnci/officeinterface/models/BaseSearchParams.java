package cymru.mwnci.officeinterface.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cymru.mwnci.sparql.helpers.ObjectHelpers;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.models.IMwnciDeepCopy;
import cymru.mwnci.sparql.models.SparqlServiceProvider;

public abstract class BaseSearchParams<T extends BaseSearchParams> implements IMwnciDeepCopy<T> {
    public SparqlServiceProvider SparqlDataSource = null;
    public EntityWithUri Entity = null;
    public String SearchTerm = "";
    protected Boolean m_shouldBePersistedToCell = false;

    public abstract T deepCopy();

    @JsonIgnore
    public boolean getShouldbePersistedToCell() {
        return this.m_shouldBePersistedToCell;
    }

    @JsonIgnore
    public void setShouldBePersistedToCell(boolean shouldBePersistedToCell) {
        this.m_shouldBePersistedToCell = shouldBePersistedToCell;
    }

    public boolean isEmpty() {
        return this.SparqlDataSource == null
                && this.Entity == null
                && this.SearchTerm.isEmpty();
    }

    protected boolean equals(BaseSearchParams other){
        return ObjectHelpers.equals(this.SparqlDataSource, other.SparqlDataSource)
                && ObjectHelpers.equals(this.Entity, other.Entity)
                && this.m_shouldBePersistedToCell.equals(other.m_shouldBePersistedToCell)
                && this.SearchTerm.equals(other.SearchTerm);
    }

    @Override
    public abstract boolean equals(Object other);

    @Override
    public abstract int hashCode();

    protected int hashCodeBase(){
        int hash = ~0;

        if (this.SparqlDataSource != null)
            hash *= this.SparqlDataSource.hashCode();

        hash *= this.m_shouldBePersistedToCell.hashCode();

        if (this.Entity != null) {
            hash *= this.Entity.hashCode();
        }

        if (this.SearchTerm != null) {
            hash *= this.SearchTerm.hashCode();
        }

        return hash;
    }

    protected void copyBasePropertiesForClone(T other){
        other.SparqlDataSource = this.SparqlDataSource == null ? null : this.SparqlDataSource.deepCopy();
        other.Entity = this.Entity == null ? null : this.Entity.deepCopy();
        other.m_shouldBePersistedToCell = this.m_shouldBePersistedToCell;
        other.SearchTerm = this.SearchTerm;
    }
}
