package cymru.mwnci.officeinterface.models;

import com.sun.star.sheet.ResultEvent;
import com.sun.star.sheet.XResultListener;
import com.sun.star.sheet.XVolatileResult;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class MwnciVolatileResult<T> implements XVolatileResult {

    private static final Logger logger = LogManager.getLogger(MwnciVolatileResult.class);
    private static final ConcurrentHashMap<String, MwnciVolatileResult> m_volatileResults = new ConcurrentHashMap<>();

    private final Set<XResultListener> m_resultListeners = Collections.newSetFromMap(
            new ConcurrentHashMap<>());

    private T m_value;
    private Observable<T> m_result;

    private MwnciVolatileResult() {
    }

    /**
     * A run-time cache of all `XVolatileResult` instances.
     * <p>
     * This is unfortunately necessary since the `XResultListener.modified` event triggered doesn't pass the new
     * result to the cell, instead it triggers the sheet to request that the function execute again.
     * WHY?! :(
     *
     * @param <T>
     * @param callingFunction
     * @param callingFunctionArgumentsIdentifier
     * @param asyncCall
     * @param initialValue
     * @return
     */
    public static <T> MwnciVolatileResult<T> getVolatileResult(
            String callingFunction, String callingFunctionArgumentsIdentifier, Function<T, T> asyncCall, T initialValue) {
        String identifier = callingFunction + "/" + callingFunctionArgumentsIdentifier;

        if (!m_volatileResults.containsKey(identifier)) {
            MwnciVolatileResult<T> res = new MwnciVolatileResult<>();

            // Keep a record of this result for later use.
            m_volatileResults.put(identifier, res);

            Observable<T> asyncSubscription = Observable.fromArray(initialValue)
                    .map(asyncCall)
                    .subscribeOn(Schedulers.io());

            res.init(asyncSubscription, initialValue);
        }

        return m_volatileResults.get(identifier);
    }

    private void init(Observable<T> result, T initialValue) {


        this.setValue(initialValue);
        m_result = result;

        m_result.distinctUntilChanged()
                .firstElement()
                .subscribe(
                        r -> this.setValue(r),
                        err -> logger.error(err.getMessage(), err)
                );
    }

    public void addResultListener(XResultListener xResultListener) {

        this.m_resultListeners.add(xResultListener);
        ResultEvent resultEvent = getResultEvent();
        xResultListener.modified(resultEvent);
    }

    public void removeResultListener(XResultListener xResultListener) {
        this.m_resultListeners.remove(xResultListener);
    }

    private void setValue(T value) {
        this.m_value = value;
        final ResultEvent resultEvent = getResultEvent();

        m_resultListeners.forEach(listener -> listener.modified(resultEvent));
    }

    private ResultEvent getResultEvent() {
        return new ResultEvent(this, m_value);
    }

}
