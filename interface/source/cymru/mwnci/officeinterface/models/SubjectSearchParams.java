package cymru.mwnci.officeinterface.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cymru.mwnci.sparql.helpers.ObjectHelpers;
import cymru.mwnci.sparql.models.UserSubjectEntityFilter;

public class SubjectSearchParams extends BaseSearchParams<SubjectSearchParams> {
    public UserSubjectEntityFilter EntityFilter = null;

    @JsonIgnore
    @Override
    public boolean isEmpty() {
        return super.isEmpty()
                && this.EntityFilter == null;
    }

    @JsonIgnore
    public SubjectSearchParams deepCopy() {
        SubjectSearchParams other = new SubjectSearchParams();

        super.copyBasePropertiesForClone(other);

        other.EntityFilter = this.EntityFilter == null ? null : this.EntityFilter.deepCopy();

        return other;
    }

    @JsonIgnore
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SubjectSearchParams)) {
            return false;
        }

        SubjectSearchParams other = (SubjectSearchParams) obj;

        return super.equals(other)
                && ObjectHelpers.equals(this.EntityFilter, other.EntityFilter);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCodeBase();

        if (this.EntityFilter != null) {
            hash *= this.EntityFilter.hashCode();
        }

        return hash;
    }
}
