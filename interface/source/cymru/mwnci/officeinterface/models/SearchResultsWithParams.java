package cymru.mwnci.officeinterface.models;

import com.sun.star.table.CellAddress;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.models.IMwnciDeepCopy;

import java.util.List;
import java.util.stream.Collectors;

public class SearchResultsWithParams<T extends BaseSearchParams>
        extends SearchParamsWithCell<T> implements IMwnciDeepCopy<SearchResultsWithParams<T>> {
    private final List<EntityWithUri> m_results;

    public <U extends SearchParamsWithCell<T>> SearchResultsWithParams(U searchParamsWithCell,
                                                                       List<EntityWithUri> results) {
        searchParamsWithCell.deepCopyPropertiesTo(this);

        this.m_results = results;
    }

    public SearchResultsWithParams(List<EntityWithUri> results, T params, CellAddress cellAddress) {
        super(params, cellAddress);

        this.m_results = results;
    }

    public List<EntityWithUri> getResults() {
        return this.m_results;
    }

    public SearchResultsWithParams<T> deepCopy() {
        List<EntityWithUri> results = m_results
                .stream()
                .map(e -> e.deepCopy())
                .collect(Collectors.toList());

        SearchResultsWithParams<T> resultsWithParams = new SearchResultsWithParams<T>(this, results);

        return resultsWithParams;
    }
}
