package cymru.mwnci.officeinterface.panels;

import com.sun.star.accessibility.XAccessible;
import com.sun.star.awt.*;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.lib.uno.helper.ComponentBase;
import com.sun.star.ui.LayoutSize;
import com.sun.star.ui.XSidebarPanel;
import com.sun.star.ui.XToolPanel;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.MwnciDialog;
import cymru.mwnci.officeinterface.MwnciHelpers;
import cymru.mwnci.officeinterface.controllers.IController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MwnciDialogPanel extends ComponentBase implements XToolPanel, XSidebarPanel, XComponent {

    private static final Logger logger = LogManager.getLogger(MwnciDialogPanel.class);
    public static final String ROOT_PANEL_CONTROL_NAME = "rootPanel";

    private XWindowPeer m_parentWindowPeer;
    private XWindow m_parentWindow;
    private XWindowPeer m_windowPeer;
    private XWindow m_window;

    private XWindow m_container_window;
    private XControlContainer m_container_controlContainer;
    private XControl m_container_control;

    private final static int MIN_WIDTH = 370;
    private final static int MIN_HEIGHT = 380;
    private int m_height;
    private int m_width;
    private Rectangle m_parentWindowSize;

    public MwnciDialogPanel(
            final XWindow parentWindow,
            final String dialogXdgRelativeUrl,
            final XComponentContext componentContext,
            final IController uiHandler) throws Exception {
        try {

            this.m_parentWindow = parentWindow;
            this.m_parentWindowPeer = UnoRuntime.queryInterface(XWindowPeer.class, parentWindow);

            loadControlsFromXdgDialogConfig(dialogXdgRelativeUrl, componentContext, uiHandler);

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw ex; // Re-throw.
        }
    }

    private void loadControlsFromXdgDialogConfig(
            String dialogXdgRelativeUrl,
            XComponentContext componentContext,
            IController controller) throws Exception {
        XMultiComponentFactory serviceManager = componentContext.getServiceManager();
        XToolkit toolkit = this.m_parentWindowPeer.getToolkit();

        XUnoControlDialog dialog = MwnciDialog.getDialog(dialogXdgRelativeUrl, componentContext);

        setChildWindowAndContainer(componentContext, serviceManager, toolkit);

        moveControlsFromDialogToNewContainer(componentContext, controller, serviceManager, dialog, toolkit);

        setContainerPeer(toolkit, dialog);

        setWindowsVisibleEnabled();

        controller.init();
    }

    private void setContainerPeer(XToolkit toolkit, XUnoControlDialog dialog) {
        m_container_control.setModel(dialog.getModel());
        m_container_control.createPeer(toolkit, this.m_windowPeer);
    }

    private void setWindowsVisibleEnabled() {
        m_container_window.setEnable(true);
        m_container_window.setVisible(true);

        m_parentWindow.setVisible(true);
        m_parentWindow.setEnable(true);

        m_window.setVisible(true);
        m_window.setEnable(true);

        m_container_window.setEnable(true);
        m_container_window.setVisible(true);
    }

    /**
     * Slightly hacky method which takes the controls from the UnoDialog and places
     * them inside the new UnoControlContainer which has been created.
     * <p>
     * Relies on disposal of the old XControls. i.e. the modal will no longer function.
     *
     * @param componentContext
     * @param controller
     * @param serviceManager
     * @param dialog
     * @param toolkit
     * @throws Exception
     */
    private void moveControlsFromDialogToNewContainer(
            XComponentContext componentContext,
            IController controller,
            XMultiComponentFactory serviceManager,
            XUnoControlDialog dialog,
            XToolkit toolkit) throws Exception {
        String[] dialogControlNames = MwnciHelpers.getControlNames(dialog.getModel());

        for (String controlName : dialogControlNames) {
            XControl control = dialog.getControl(controlName);
            XControlModel control_controlModel = control.getModel();

            dialog.removeControl(control);

            // Dispose MUST be called in order that the windowPeer property is unset.
            control.dispose();

            // The model is now empty, we must set it once more.
            control.setModel(control_controlModel);

            m_container_controlContainer.addControl(controlName, control);

            // Create peer is an important step which actually binds the control to the window.
            control.createPeer(toolkit, this.m_windowPeer);

            controller.registerComponent(controlName, control);
            MwnciHelpers.setEventHandlers(componentContext, controller, serviceManager, controlName, control);
        }

        controller.registerComponent(ROOT_PANEL_CONTROL_NAME, m_container_control);
        MwnciHelpers.setEventHandlers(componentContext, controller, serviceManager, ROOT_PANEL_CONTROL_NAME,
                m_container_control);

    }

    private void setChildWindowAndContainer(
            XComponentContext componentContext,
            XMultiComponentFactory serviceManager,
            XToolkit toolkit) throws Exception {

        m_parentWindowSize = this.m_parentWindow.getPosSize();

        m_width = m_parentWindowSize.Width;
        m_width = Math.max(m_width, MIN_WIDTH);

        m_height = m_parentWindowSize.Height / 2; // Two panels should be displayable at the same time.
        m_height = Math.max(m_height, MIN_HEIGHT);

        WindowDescriptor windowDescriptor = new WindowDescriptor();
        windowDescriptor.Type = WindowClass.CONTAINER;
        windowDescriptor.WindowServiceName = "";
        windowDescriptor.Parent = m_parentWindowPeer;
        windowDescriptor.ParentIndex = -1;
        windowDescriptor.Bounds = new Rectangle(0, 0, m_width, m_height);
        windowDescriptor.WindowAttributes =
                WindowAttribute.SIZEABLE | WindowAttribute.MOVEABLE
                        | WindowAttribute.NODECORATION;


        this.m_windowPeer = toolkit.createWindow(windowDescriptor);
        this.m_window = UnoRuntime.queryInterface(XWindow.class, this.m_windowPeer);

        Object newContainer_obj = serviceManager.createInstanceWithContext(
                "com.sun.star.awt.UnoControlContainer", componentContext);

        m_container_window = UnoRuntime.queryInterface(XWindow.class, newContainer_obj);
        m_container_control = UnoRuntime.queryInterface(XControl.class, newContainer_obj);
        m_container_controlContainer = UnoRuntime.queryInterface(XControlContainer.class, newContainer_obj);
    }

    public LayoutSize getHeightForWidth(int i) {
        LayoutSize layoutSize = new LayoutSize();

        layoutSize.Minimum = MIN_HEIGHT;
        layoutSize.Maximum = m_parentWindowSize.Height;
        layoutSize.Preferred = m_height;

        return layoutSize;
    }

    public int getMinimalWidth() {
        return MIN_WIDTH;
    }

    public XWindow getWindow() {
        return m_window;
    }

    public XAccessible createAccessible(XAccessible xAccessible) {
        return null;
    }
}
