package cymru.mwnci.officeinterface;

import com.sun.star.frame.XFrame;
import com.sun.star.ui.UIElementType;
import com.sun.star.ui.XUIElement;
import cymru.mwnci.officeinterface.panels.MwnciDialogPanel;

public class MwnciUIElement implements XUIElement {

    private final String m_resourceUrl;
    private final MwnciDialogPanel m_panel;

    public MwnciUIElement(final String resourceUrl, MwnciDialogPanel panel){
        this.m_resourceUrl = resourceUrl;
        this.m_panel = panel;
    }

    public XFrame getFrame() {
        return null;
    }

    public String getResourceURL() {
        return m_resourceUrl;
    }

    public short getType() {
        return UIElementType.TOOLPANEL;
    }

    public Object getRealInterface() {
        return m_panel;
    }
}
