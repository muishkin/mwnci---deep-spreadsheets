package cymru.mwnci.officeinterface.factories;

import com.sun.star.awt.XWindow;
import com.sun.star.beans.PropertyValue;
import com.sun.star.lang.IllegalArgumentException;
import com.sun.star.lang.XServiceInfo;
import com.sun.star.lang.XSingleComponentFactory;
import com.sun.star.lib.uno.helper.Factory;
import com.sun.star.registry.XRegistryKey;
import com.sun.star.ui.XUIElement;
import com.sun.star.ui.XUIElementFactory;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.MwnciServiceBase;
import cymru.mwnci.officeinterface.MwnciUIElement;
import cymru.mwnci.officeinterface.controllers.SparqlEntitySearchController;
import cymru.mwnci.officeinterface.controllers.SparqlValueSearchController;
import cymru.mwnci.officeinterface.panels.MwnciDialogPanel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Optional;

public class MwnciPanelFactory extends MwnciServiceBase implements XServiceInfo, XUIElementFactory {

    private static final Logger logger = LogManager.getLogger(MwnciPanelFactory.class);

    public static final String m_serviceName = "cymru.mwnci.officeinterface.factories.MwnciPanelFactory";
    private static final String m_implementationName = MwnciPanelFactory.class.getName();
    private static final String[] m_serviceNames = {m_serviceName};

    private final XComponentContext m_componentContext;

    public MwnciPanelFactory(XComponentContext componentContext) {
        super(m_implementationName, m_serviceNames);

        this.m_componentContext = componentContext;
    }


    public static XSingleComponentFactory __getComponentFactory(String sImplementationName) {
        XSingleComponentFactory xFactory = null;

        if (sImplementationName.equals(m_implementationName))
            xFactory = Factory.createComponentFactory(MwnciPanelFactory.class, m_serviceNames);
        return xFactory;
    }

    public static boolean __writeRegistryServiceInfo(XRegistryKey xRegistryKey) {
        return Factory.writeRegistryServiceInfo(m_implementationName,
                m_serviceNames,
                xRegistryKey);
    }

    public XUIElement createUIElement(String resourceUrl, PropertyValue[] propertyValues) {
        logger.info("Rendering XDG Dialog: " + resourceUrl);
        try {
            Optional<XWindow> maybeParentWindow = Arrays.stream(propertyValues)
                    .filter(p -> p.Name.equals("ParentWindow"))
                    .findFirst()
                    .map(p -> UnoRuntime.queryInterface(XWindow.class, p.Value));

            if (!maybeParentWindow.isPresent()) {
                throw new IllegalArgumentException("Unable to find matching ParentWindow in PropertyValues.");
            }

            XWindow parentWindow = maybeParentWindow.get();

            if (resourceUrl.endsWith("/MwnciSubjectSearchPanel")) {
                return new MwnciUIElement(
                        resourceUrl,
                        new MwnciDialogPanel(
                                parentWindow,
                                "/resources/dialogs/sparqlEntitySearch/SparqlEntitySearch.xdl",
                                m_componentContext,
                                new SparqlEntitySearchController(this.m_componentContext)
                        )
                );
            } else if (resourceUrl.endsWith("/MwnciObjectValueSearchPanel")) {
                return new MwnciUIElement(
                        resourceUrl,
                        new MwnciDialogPanel(
                                parentWindow,
                                "/resources/dialogs/sparqlValueSearch/SparqlValueSearch.xdl",
                                m_componentContext,
                                new SparqlValueSearchController(this.m_componentContext)
                        )
                );
            }

            throw new IllegalArgumentException("Unable to find matching resource for URL '" + resourceUrl + "'.");
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return null;
    }
}
