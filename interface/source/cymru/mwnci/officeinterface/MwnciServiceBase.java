package cymru.mwnci.officeinterface;

import com.sun.star.lang.XServiceInfo;
import com.sun.star.lib.uno.helper.WeakBase;

public abstract class MwnciServiceBase extends WeakBase implements XServiceInfo {

    private final String[] m_supportedServices;
    private final String m_implementationName;

    protected MwnciServiceBase(String implementationName, String[] supportedServices){
        m_implementationName = implementationName;
        m_supportedServices = supportedServices;
    }

    public String getImplementationName() {
        return m_implementationName;
    }

    public boolean supportsService(String s) {
        int len = m_supportedServices.length;

        for (int i = 0; i < len; i++) {
            if (s.equals(m_supportedServices[i]))
                return true;
        }

        return false;
    }

    public String[] getSupportedServiceNames() {
        return m_supportedServices;
    }
}
