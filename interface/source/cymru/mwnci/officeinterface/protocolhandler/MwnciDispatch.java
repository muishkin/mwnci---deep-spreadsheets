package cymru.mwnci.officeinterface.protocolhandler;

import com.sun.star.beans.PropertyValue;
import com.sun.star.frame.FeatureStateEvent;
import com.sun.star.frame.XDispatch;
import com.sun.star.frame.XStatusListener;
import com.sun.star.lang.XSingleComponentFactory;
import com.sun.star.lib.uno.helper.Factory;
import com.sun.star.registry.XRegistryKey;
import com.sun.star.uno.XComponentContext;
import com.sun.star.util.URL;
import cymru.mwnci.officeinterface.MwnciServiceBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class MwnciDispatch extends MwnciServiceBase implements XDispatch {

    public static final String IMPLEMENTATION_NAME = MwnciDispatch.class.getName();
    private static final String[] m_supportedServices = new String[] {
            IMPLEMENTATION_NAME
    };

    private final XComponentContext m_context;

    private static final Logger logger = LogManager.getLogger(MwnciDispatch.class);

    private URL m_mainUrl;
    private Object m_state;

    private final Set<XStatusListener> m_statusListeners = new HashSet<>();

    public MwnciDispatch(XComponentContext componentContext) {
        super(IMPLEMENTATION_NAME, m_supportedServices);

        this.m_context = componentContext;

    }

    public void init(URL mainUrl, Object state){
        this.m_mainUrl = mainUrl;
        this.m_state = state;
    }

    public void setState(Object state){
        m_state = state;
        dispatch(this.m_mainUrl, new PropertyValue[0]);
    }

    public void dispatch(URL url, PropertyValue[] propertyValues) {
        logger.debug("Dispatch called for url = " + url.Complete);
        FeatureStateEvent event = getFeatureStateEvent(url);
        this.m_statusListeners.forEach(listener -> listener.statusChanged(event));
    }

    private FeatureStateEvent getFeatureStateEvent(URL url) {
        FeatureStateEvent event = new FeatureStateEvent();

        event.FeatureURL = url;
        event.FeatureDescriptor = "Something";
        event.IsEnabled = true;
        event.Source = this;

        event.State = m_state;

        return event;
    }

    public void addStatusListener(XStatusListener xStatusListener, URL url) {
        this.m_statusListeners.add(xStatusListener);

        // Need to reply with current state immediately upon subscription.
        xStatusListener.statusChanged(getFeatureStateEvent(url));
    }

    public void removeStatusListener(XStatusListener xStatusListener, URL url) {
        this.m_statusListeners.remove(xStatusListener);
    }

    public static XSingleComponentFactory __getComponentFactory(String sImplementationName) {
        XSingleComponentFactory xFactory = null;

        if (sImplementationName.equals(IMPLEMENTATION_NAME))
            xFactory = Factory.createComponentFactory(MwnciDispatch.class, m_supportedServices);
        return xFactory;
    }

    public static boolean __writeRegistryServiceInfo(XRegistryKey xRegistryKey) {
        return Factory.writeRegistryServiceInfo(IMPLEMENTATION_NAME,
                m_supportedServices,
                xRegistryKey);
    }

}
