package cymru.mwnci.officeinterface.protocolhandler;

import com.sun.star.frame.DispatchDescriptor;
import com.sun.star.frame.XDispatch;
import com.sun.star.frame.XDispatchProvider;
import com.sun.star.lang.XSingleComponentFactory;
import com.sun.star.lib.uno.helper.Factory;
import com.sun.star.registry.XRegistryKey;
import com.sun.star.uno.XComponentContext;
import com.sun.star.util.URL;
import cymru.mwnci.officeinterface.MwnciServiceBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ConcurrentHashMap;

public class MwnciProtocolHandler extends MwnciServiceBase
        implements XDispatchProvider {

    public static final String IMPLEMENTATION_NAME = MwnciProtocolHandler.class.getName();
    private static final String[] m_supportedServices = new String[]{
            IMPLEMENTATION_NAME
    };

    public static final URL URL_SPARQL_QUERY_STATE = getUrl("mwnci://sparqlQueryState");

    private static final Logger logger = LogManager.getLogger(MwnciProtocolHandler.class);

    private final XComponentContext m_context;

    private static final ConcurrentHashMap<String, XDispatch> m_mapDispatchIdToDispatch = new ConcurrentHashMap<>();

    public MwnciProtocolHandler(XComponentContext componentContext) {
        super(IMPLEMENTATION_NAME, m_supportedServices);

        m_context = componentContext;
    }

    public static XSingleComponentFactory __getComponentFactory(String sImplementationName) {
        XSingleComponentFactory xFactory = null;

        if (sImplementationName.equals(IMPLEMENTATION_NAME))
            xFactory = Factory.createComponentFactory(MwnciProtocolHandler.class, m_supportedServices);
        return xFactory;
    }

    public static boolean __writeRegistryServiceInfo(XRegistryKey xRegistryKey) {
        return Factory.writeRegistryServiceInfo(IMPLEMENTATION_NAME,
                m_supportedServices,
                xRegistryKey);
    }

    public XDispatch queryDispatch(URL url, String sTarget, int nFlags) {
        try {
            if (url.Main.startsWith("mwnci://")) {

                if(sTarget == null)
                    sTarget = "";

                String dispatchId = url.Main + sTarget;
                // Make sure we cache the dispatchers to make it easy to subscribe to changes and broadcast them.
                if(!m_mapDispatchIdToDispatch.containsKey(dispatchId)) {
                    logger.error("Creating new MwnciDispatch instance for dispatchId = " + dispatchId);
                    MwnciDispatch newDispatch = new MwnciDispatch(this.m_context);
                    newDispatch.init(url, null);

                    m_mapDispatchIdToDispatch.put(dispatchId, newDispatch);
                }

                return m_mapDispatchIdToDispatch.get(dispatchId);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return null;
    }

    public XDispatch[] queryDispatches(DispatchDescriptor[] dispatchDescriptors) {
        XDispatch[] dispatches = new XDispatch[dispatchDescriptors.length];

        int i = 0;
        for(DispatchDescriptor descriptor : dispatchDescriptors){
            dispatches[i] = queryDispatch(descriptor.FeatureURL, descriptor.FrameName, descriptor.SearchFlags);

            i++;
        }

        return dispatches;
    }

    private static URL getUrl(String url) {
        URL u = new URL();

        u.Main = url;
        u.Complete = url;

        return u;
    }
}
