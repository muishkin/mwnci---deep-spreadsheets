package cymru.mwnci.officeinterface.protocolhandler;

import com.sun.star.frame.FeatureStateEvent;
import com.sun.star.frame.XDispatch;
import com.sun.star.frame.XDispatchProvider;
import com.sun.star.frame.XStatusListener;
import com.sun.star.lang.EventObject;
import com.sun.star.lang.XSingleComponentFactory;
import com.sun.star.lib.uno.helper.Factory;
import com.sun.star.registry.XRegistryKey;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.MwnciServiceBase;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class MwnciStatusListener extends MwnciServiceBase implements XStatusListener {

    public static final String IMPLEMENTATION_NAME = MwnciStatusListener.class.getName();
    private static final String[] m_supportedServices = new String[] {
            IMPLEMENTATION_NAME
    };

    private static final Logger logger = LogManager.getLogger(MwnciStatusListener.class);

    private final Subject<Boolean> m_requestsAreActive = BehaviorSubject.create();

    private final List<Function<FeatureStateEvent, Boolean>> m_functionsToRun =
            new LinkedList<Function<FeatureStateEvent, Boolean>>();

    public MwnciStatusListener() {
        super(IMPLEMENTATION_NAME, m_supportedServices);
    }

    public static XSingleComponentFactory __getComponentFactory(String sImplementationName) {
        XSingleComponentFactory xFactory = null;

        if (sImplementationName.equals(IMPLEMENTATION_NAME))
            xFactory = Factory.createComponentFactory(MwnciStatusListener.class, m_supportedServices);
        return xFactory;
    }

    public static boolean __writeRegistryServiceInfo(XRegistryKey xRegistryKey) {
        return Factory.writeRegistryServiceInfo(IMPLEMENTATION_NAME,
                m_supportedServices,
                xRegistryKey);
    }

    public void onStatusChanged(Function<FeatureStateEvent, Boolean> funToRun){
        this.m_functionsToRun.add(funToRun);
    }

    public static MwnciStatusListener getForMwnciProtocolHandler(XComponentContext context) throws Exception {
        Object protocolHandlerObj = context.getServiceManager()
                .createInstanceWithContext(MwnciProtocolHandler.IMPLEMENTATION_NAME, context);

        XDispatchProvider protocolHandler = UnoRuntime.queryInterface(XDispatchProvider.class, protocolHandlerObj);

        XDispatch dispatch = protocolHandler.queryDispatch(
                MwnciProtocolHandler.URL_SPARQL_QUERY_STATE, null, 0);

        MwnciStatusListener listener = new MwnciStatusListener();
        dispatch.addStatusListener(listener, MwnciProtocolHandler.URL_SPARQL_QUERY_STATE);

        return listener;
    }

    public void statusChanged(FeatureStateEvent featureStateEvent) {
        this.m_functionsToRun.forEach(fun -> fun.apply(featureStateEvent));

        boolean status = featureStateEvent.State == null ? false : (Boolean) featureStateEvent.State;
        this.m_requestsAreActive.onNext(status);
        logger.debug("Status changed to " + status);
    }

    public Observable<Boolean> getRequestAreActiveObservable() {
        return this.m_requestsAreActive
                .debounce(500, TimeUnit.MILLISECONDS);
    }

    public void disposing(EventObject eventObject) {
        this.m_requestsAreActive.hasComplete();
    }
}
