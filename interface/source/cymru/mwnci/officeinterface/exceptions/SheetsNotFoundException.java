package cymru.mwnci.officeinterface.exceptions;

import cymru.mwnci.sparql.exceptions.MwnciException;

public class SheetsNotFoundException extends MwnciException {

    public SheetsNotFoundException() {
        super("Unable to find sheets in current SpreadsheetDocument");
    }

}
