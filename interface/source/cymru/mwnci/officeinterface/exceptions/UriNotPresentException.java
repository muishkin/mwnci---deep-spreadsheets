package cymru.mwnci.officeinterface.exceptions;

import cymru.mwnci.sparql.exceptions.MwnciException;

public class UriNotPresentException extends MwnciException {

    public UriNotPresentException() {
        super("Could not find URI.");
    }
}
