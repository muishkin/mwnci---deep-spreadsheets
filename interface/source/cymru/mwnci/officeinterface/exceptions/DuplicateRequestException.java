package cymru.mwnci.officeinterface.exceptions;

import cymru.mwnci.sparql.exceptions.MwnciException;

public class DuplicateRequestException extends MwnciException {

    public DuplicateRequestException() {
        super("Duplicate request made.");
    }
}
