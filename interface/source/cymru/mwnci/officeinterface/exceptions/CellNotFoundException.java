package cymru.mwnci.officeinterface.exceptions;

import com.sun.star.table.CellAddress;
import cymru.mwnci.sparql.exceptions.MwnciException;

public class CellNotFoundException extends MwnciException {

    public CellNotFoundException(String cellAddress) {
        super("Unable to find cell with address: " + cellAddress);
    }

    public CellNotFoundException(CellAddress cellAddress) {
        super("Unable to find cell with address: (Sheet, Col, Row) = ("
                + cellAddress.Sheet + "," + cellAddress.Column + "," + cellAddress.Row + ")");
    }
}
