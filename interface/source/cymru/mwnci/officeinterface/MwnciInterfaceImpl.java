package cymru.mwnci.officeinterface;

import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.lang.XSingleComponentFactory;
import com.sun.star.lib.uno.helper.Factory;
import com.sun.star.registry.XRegistryKey;
import com.sun.star.sheet.XFunctionAccess;
import com.sun.star.sheet.XVolatileResult;
import com.sun.star.uno.AnyConverter;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.exceptions.UriNotPresentException;
import cymru.mwnci.officeinterface.models.MwnciVolatileResult;
import cymru.mwnci.officeinterface.protocolhandler.MwnciDispatch;
import cymru.mwnci.officeinterface.protocolhandler.MwnciProtocolHandler;
import cymru.mwnci.sparql.Mwnci;
import cymru.mwnci.sparql.exceptions.MwnciException;
import cymru.mwnci.sparql.exceptions.SparqlProviderNotFoundException;
import cymru.mwnci.sparql.exceptions.ValueNotFoundException;
import cymru.mwnci.sparql.helpers.SparqlIriHelpers;
import cymru.mwnci.sparql.models.EntityProperty;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.services.Interfaces.ISparqlProvider;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;
import io.reactivex.functions.Function;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.Closeable;
import java.io.IOException;
import java.net.URI;
import java.util.Optional;


public final class MwnciInterfaceImpl extends MwnciServiceBase
        implements cymru.mwnci.officeinterface.XMwnciInterface {
    private static final Logger logger = LogManager.getLogger(MwnciInterfaceImpl.class);
    public static final String INITIAL_LOADING_VALUE = "Loading...";
    public static final String NOT_FOUND_IDENTIFIER = "NotFound";

    private static Object m_numActiveSparqlQueries_lock = new Object();
    /**
     * You MUST acquire a lock on m_numActiveSparqlQueryes_Lock before reading/writing to this Integer.
     */
    private static Integer m_numActiveSparqlQueries = 0;

    private final XComponentContext m_context;
    public static final String IMPLEMENTATION_NAME = MwnciInterfaceImpl.class.getName();
    private static final String[] m_serviceNames = {IMPLEMENTATION_NAME};

    private final ISparqlService m_sparqlService;
    private MwnciDispatch m_sparqlQueryStateDispatch;
    private XFunctionAccess m_functionAccess;

    public MwnciInterfaceImpl(XComponentContext context) {
        super(IMPLEMENTATION_NAME, m_serviceNames);

        m_context = context;
        m_sparqlService = Mwnci.getService(ISparqlService.class);

        try {
            initSparqlQueryStateDispatch();
            initFunctionAccess();
        } catch (java.lang.Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    protected void initFunctionAccess() throws java.lang.Exception {
        Object functionAccessO = this.m_context
                .getServiceManager()
                .createInstanceWithContext("com.sun.star.sheet.FunctionAccess", m_context);

        m_functionAccess = UnoRuntime.queryInterface(XFunctionAccess.class, functionAccessO);
    }


    private void initSparqlQueryStateDispatch() throws Exception {
        XMultiComponentFactory serviceManager = m_context.getServiceManager();

        MwnciProtocolHandler protocolHandler = (MwnciProtocolHandler) serviceManager.createInstanceWithContext(
                MwnciProtocolHandler.IMPLEMENTATION_NAME, m_context);

        m_sparqlQueryStateDispatch = (MwnciDispatch) protocolHandler.queryDispatch(
                MwnciProtocolHandler.URL_SPARQL_QUERY_STATE, null, 0);
        m_sparqlQueryStateDispatch.setState(false);
    }

    /**
     * Calc Add-in Function
     * <p>
     * Allows the user to extract the entity URI from a SPARQL cell.
     *
     * @param cellText The text from a cell containing a URI.
     * @return Returns the cell's entity URI.
     * @throws IndexOutOfBoundsException Thrown if the cellRange is not of the appropriate size.
     */
    public String sparqlEntityUri(String cellText) throws WrappedTargetException {
        try {
            Optional<URI> maybeUri = SparqlIriHelpers.getEntityUriFromString(cellText);
            if (maybeUri.isPresent()) {
                return maybeUri.get().toString();
            }

            throw new UriNotPresentException();
        } catch (java.lang.Exception e) {
            logger.error(e.getMessage(), e);
            throw new WrappedTargetException(e);
        }
    }

    private Function<Object, Object> getPropertyLookupAsyncFunc(
            Optional<URI> maybeEntityUri, String sparqlProviderName, String propertyPredicate,
            String predicateLanguage, boolean allowOneToManyValues) {
        return previousValue -> {
            logger.traceEntry();

            Object valueOut;
            try (Closeable sparqlQueryWatch = registerSparqlQuery()) {
                try {
                    if (!maybeEntityUri.isPresent()) {
                        throw new UriNotPresentException();
                    }

                    ISparqlProvider sparqlProvider = Mwnci.getSparqlProvider(sparqlProviderName);

                    EntityProperty propertyValue = m_sparqlService.getPropertyValueForEntity(
                            sparqlProvider,
                            maybeEntityUri.get(),
                            propertyPredicate, predicateLanguage,
                            MwnciHelpers.getOutputLanguage(m_context),
                            allowOneToManyValues
                    );

                    if (propertyValue.getPropertyIsEntity()) {
                        EntityWithUri entityWithUri = propertyValue.getEntityWithUri();
                        valueOut = MwnciHelpers.getDisplayTextForEntityWithUri(entityWithUri);
                    } else if (propertyValue.getPropertyIsDate()) {
                        XMLGregorianCalendar date = propertyValue.getDate();
                        valueOut = MwnciHelpers.mapXmlGregorianCalendarToCalcFormat(m_functionAccess, date);
                    } else {
                        valueOut = propertyValue.getValue();
                    }
                } catch (ValueNotFoundException e) {
                    valueOut = "";
                } catch (MwnciException e) {
                    logger.error(e.getMessage(), e);
                    valueOut = "Error: " + e.getMessage();
                }
            }
            return logger.traceExit(valueOut);
        };
    }

    /**
     * Must be called when starting a SPARQL query.
     * <p>
     * Aides in counting the number of SPARQL queries which remain open.
     *
     * @return A closable to aide in automatically marking the query as complete.
     */
    private Closeable registerSparqlQuery() {
        synchronized (m_numActiveSparqlQueries_lock) {
            if (m_numActiveSparqlQueries == 0) {
                // Notify listeners that SPARQL queries are now on-going.
                m_sparqlQueryStateDispatch.setState(true);
            }

            m_numActiveSparqlQueries++;
        }

        return new Closeable() {
            public void close() throws IOException {
                sparqlQueryComplete();
            }
        };
    }

    /**
     * Must be called when finishing a SPARQL query. Even if an exception is thrown.
     */
    private void sparqlQueryComplete() {
        synchronized (m_numActiveSparqlQueries_lock) {
            m_numActiveSparqlQueries--;

            if (m_numActiveSparqlQueries == 0) {
                // Notify listeners that no SPARQL queries are currently on-going.
                m_sparqlQueryStateDispatch.setState(false);
            }
        }
    }

    private Function<String, String> getEntityLookupAsyncFunc(Optional<URI> maybeEntityUri,
                                                              Optional<String> maybeSparqlProvider) {
        return previousValue -> {
            logger.traceEntry();
            String valueOut;

            try (Closeable sparqlQueryWatch = registerSparqlQuery()) {
                try {
                    if (!maybeEntityUri.isPresent()) {
                        throw new UriNotPresentException();
                    }

                    ISparqlProvider sparqlProvider;

                    if (maybeSparqlProvider.isPresent()) {
                        sparqlProvider = Mwnci.getSparqlProvider(maybeSparqlProvider.get());
                    } else if (maybeEntityUri.isPresent()) {
                        URI entityUri = maybeEntityUri.get();
                        sparqlProvider = Mwnci.getSparqlProviderforUri(entityUri)
                                .orElseThrow(() -> new SparqlProviderNotFoundException(entityUri));
                    } else {
                        throw new SparqlProviderNotFoundException("No provider name or URI provided.");
                    }

                    EntityWithUri entityWithUri = m_sparqlService.getEntityForUri(
                            sparqlProvider,
                            maybeEntityUri.get(),
                            MwnciHelpers.getOutputLanguage(m_context));

                    valueOut = MwnciHelpers.getDisplayTextForEntityWithUri(entityWithUri);

                } catch (MwnciException e) {
                    logger.error(e.getMessage(), e);
                    valueOut = "Error: " + e.getMessage();
                }
            }
            return logger.traceExit(valueOut);
        };
    }

    /**
     * Calc Add-in Function
     * <p>
     * Allows the user to reference an entity by its URI directly.
     *
     * @param entityUri           A string containing the entityUri which is to be de-referenced against the sparqlProvider.
     * @param maybeSparqlProvider The name (or URI) of the provider used to de-reference this entity.
     * @param maybeSearchJson     Optional parameter, if populated then it contains searchJson.
     * @return
     */
    public XVolatileResult sparqlEntity(String entityUri, Object maybeSparqlProvider, Object maybeSearchJson) throws WrappedTargetException {
        try {
            Optional<String> sparqlProvider = AnyConverter.isString(maybeSparqlProvider)
                    ? Optional.ofNullable(AnyConverter.toString(maybeSparqlProvider))
                    : Optional.empty();

            return getEntityLookupVolatile(entityUri, sparqlProvider);
        } catch (java.lang.Exception e) {
            logger.error(e.getMessage(), e);
            throw new WrappedTargetException(e);
        }
    }

    /**
     * Calc Add-in Function
     * <p>
     * Allows the user to reference a property of an entity directly.
     *
     * @param entityUri              String input which must contain one (and only one) entityURI.
     * @param sparqlProvider         Either the name of a SPARQL provider registered with Mwnci or the URI of a SPARQL endpoint
     *                               which will function with the standard ISparqlProvider implementation.
     * @param propertyPredicate      The label of a given predicate OR a valid SPARQL graph pattern statement, e.g.
     *                               `?subject wdt:P123 ?value`.
     * @param maybePredicateLanguage If the propertyPredicate param passed is a label, the language which should be used to
     *                               filter the predicates on.
     * @param maybeSearchJson        Optional parameter (hopefully) which can contain the JSON search params.
     * @return
     * @throws WrappedTargetException
     */
    public XVolatileResult sparqlValue(String entityUri, String sparqlProvider, String propertyPredicate,
                                       Object maybeAllowManyValues, Object maybePredicateLanguage,
                                       Object maybeSearchJson) throws WrappedTargetException {
        try {
            String predicateLanguage = AnyConverter.isString(maybePredicateLanguage)
                    ? AnyConverter.toString(maybePredicateLanguage)
                    : null;

            if (predicateLanguage == null || predicateLanguage.isEmpty())
                predicateLanguage = MwnciHelpers.getOutputLanguage(m_context);

            boolean allowOneToManyValues = MwnciHelpers.getBooleanFromUnoMaybeBoolean(maybeAllowManyValues);

            return getEntityPropertyLookupVolatile(entityUri, sparqlProvider, propertyPredicate,
                    predicateLanguage, allowOneToManyValues);
        } catch (java.lang.Exception e) {
            logger.error(e.getMessage(), e);
            throw new WrappedTargetException(e);
        }
    }

    private XVolatileResult getEntityPropertyLookupVolatile(
            String entityUri, String sparqlProvider, String propertyPredicate, String predicateLanguage,
            boolean allowOneToManyValues) {
        Optional<URI> maybeEntityUri = SparqlIriHelpers.getEntityUriFromString(entityUri);

        Function<Object, Object> asyncCall = getPropertyLookupAsyncFunc(
                maybeEntityUri, sparqlProvider, propertyPredicate, predicateLanguage, allowOneToManyValues);

        String argsIdentifier;
        if (maybeEntityUri.isPresent()) {
            argsIdentifier = sparqlProvider + "->" + maybeEntityUri.get().toString()
                    + "->" + propertyPredicate + "->" + predicateLanguage + "->" + allowOneToManyValues;
        } else {
            argsIdentifier = NOT_FOUND_IDENTIFIER;
        }
        return MwnciVolatileResult.getVolatileResult(
                "getEntityPropertyLookupVolatile",
                argsIdentifier,
                asyncCall,
                INITIAL_LOADING_VALUE
        );

    }

    private XVolatileResult getEntityLookupVolatile(String entityUriIn, Optional<String> maybeSparqlProvider) {
        Optional<URI> entityUri = SparqlIriHelpers.getEntityUriFromString(entityUriIn);

        String identifier = entityUri.isPresent()
                ? maybeSparqlProvider + "/" + entityUri.get().toString() : NOT_FOUND_IDENTIFIER;

        return MwnciVolatileResult.getVolatileResult(
                "getEntityLookupVolatile",
                identifier,
                getEntityLookupAsyncFunc(entityUri, maybeSparqlProvider),
                INITIAL_LOADING_VALUE
        );
    }

    public static XSingleComponentFactory __getComponentFactory(String sImplementationName) {
        XSingleComponentFactory xFactory = null;

        if (sImplementationName.equals(IMPLEMENTATION_NAME))
            xFactory = Factory.createComponentFactory(MwnciInterfaceImpl.class, m_serviceNames);
        return xFactory;
    }

    public static boolean __writeRegistryServiceInfo(XRegistryKey xRegistryKey) {
        return Factory.writeRegistryServiceInfo(IMPLEMENTATION_NAME,
                m_serviceNames,
                xRegistryKey);
    }
}
