package cymru.mwnci.officeinterface;

import com.sun.star.lang.EventObject;
import com.sun.star.lang.XSingleComponentFactory;
import com.sun.star.lib.uno.helper.Factory;
import com.sun.star.registry.XRegistryKey;
import com.sun.star.script.AllEventObject;
import com.sun.star.script.XAllListener;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.controllers.IController;

public class MwnciEventHandler extends MwnciServiceBase implements XAllListener {

    private static final String m_implementationName = MwnciEventHandler.class.getName();
    private static final String[] m_serviceNames = {"com.sun.star.script.XAllListener"};

    private IController m_controller;

    private final XComponentContext m_componentContext;

    public MwnciEventHandler(XComponentContext context) {
        super(m_implementationName, m_serviceNames);

        m_componentContext = context;
    }

    public void setController(IController controller) {
        this.m_controller = controller;
    }

    public void firing(AllEventObject allEventObject) {
        String controlName = (String) allEventObject.Helper;
        String eventType = allEventObject.MethodName;

        this.m_controller.handleEvent(controlName, eventType, allEventObject);
    }

    public Object approveFiring(AllEventObject allEventObject) {
        return null;
    }

    public void disposing(EventObject eventObject) {
    }


    public static XSingleComponentFactory __getComponentFactory(String sImplementationName) {
        XSingleComponentFactory xFactory = null;

        if (sImplementationName.equals(m_implementationName))
            xFactory = Factory.createComponentFactory(MwnciEventHandler.class, m_serviceNames);
        return xFactory;
    }

    public static boolean __writeRegistryServiceInfo(XRegistryKey xRegistryKey) {
        return Factory.writeRegistryServiceInfo(m_implementationName,
                m_serviceNames,
                xRegistryKey);
    }

    @Override
    public String getImplementationName() {
        return m_implementationName;
    }

    @Override
    public boolean supportsService(String sService) {
        int len = m_serviceNames.length;

        for (int i = 0; i < len; i++) {
            if (sService.equals(m_serviceNames[i]))
                return true;
        }
        return false;
    }

    @Override
    public String[] getSupportedServiceNames() {
        return m_serviceNames;
    }

}
