package cymru.mwnci.officeinterface.tests;

import com.sun.star.beans.PropertyValue;
import com.sun.star.frame.FrameSearchFlag;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XDesktop;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.sheet.XSpreadsheetDocument;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;
import cymru.mwnci.officeinterface.protocolhandler.MwnciStatusListener;
import ooo.connector.BootstrapSocketConnector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

public abstract class MwnciInterfaceTestBase {

    protected XComponentLoader componentLoader;
    protected XSpreadsheetDocument activeSpreadsheetDocument;
    protected XComponentContext context;
    protected XDesktop desktop;

    private static final Logger logger = LogManager.getLogger(MwnciInterfaceTestBase.class);
    private MwnciStatusListener mwnciStatusListener;

    protected void beforeAll(File file) {
        try {
            String programFolder = System.getenv("OFFICE_PROGRAM_FOLDER");
            if (programFolder == null) {
                programFolder = "/usr/lib/libreoffice/program/";
            }

            context = BootstrapSocketConnector.bootstrap(programFolder);

            initDesktop();
            initComponentLoader();

            if (file == null) {
                loadBlankSpreadsheetDocument();
            } else {
                loadSpreadsheetDocument(file);
            }

            // listenForSparqlQueryStateChange
            mwnciStatusListener = MwnciStatusListener.getForMwnciProtocolHandler(this.context);
        } catch (java.lang.Exception e) {
            logger.error(e.getMessage(), e);
        }

        // Block the thread until all of the SPARQL queries have completed.
        mwnciStatusListener
                .getRequestAreActiveObservable()
                .filter(currentlyActiveSparqlRequests -> !currentlyActiveSparqlRequests)
                .blockingFirst();
    }


    protected void afterAll() {
        if (desktop != null) {
            desktop.terminate();
        }
    }

    private void initComponentLoader() throws Exception {
        componentLoader = UnoRuntime.queryInterface(XComponentLoader.class, desktop);
    }

    private void initDesktop() throws Exception {
        XMultiComponentFactory xMngr = context.getServiceManager();
        Object oDesktop = xMngr.createInstanceWithContext("com.sun.star.frame.Desktop", context);
        desktop = UnoRuntime.queryInterface(XDesktop.class, oDesktop);
    }

    protected void loadBlankSpreadsheetDocument() throws com.sun.star.io.IOException {
        XComponent xDoc = componentLoader.loadComponentFromURL("private:factory/scalc", "_default",
                FrameSearchFlag.ALL, new PropertyValue[0]);

        activeSpreadsheetDocument = UnoRuntime.queryInterface(XSpreadsheetDocument.class, xDoc);
    }

    protected void loadSpreadsheetDocument(File file) throws IOException, com.sun.star.io.IOException {
        String filePath = "file:///" + file.getCanonicalPath().replace('\\', '/');

        XComponent newDocumentComponent = componentLoader.loadComponentFromURL(
                filePath, "_blank", 0, new PropertyValue[0]);

        activeSpreadsheetDocument = UnoRuntime.queryInterface(XSpreadsheetDocument.class, newDocumentComponent);
    }

}


