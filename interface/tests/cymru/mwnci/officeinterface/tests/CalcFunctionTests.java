package cymru.mwnci.officeinterface.tests;

import com.sun.star.container.NoSuchElementException;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.sheet.XSpreadsheet;
import com.sun.star.table.CellContentType;
import com.sun.star.table.XCell;
import com.sun.star.table.XCellRange;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import cymru.mwnci.officeinterface.MwnciHelpers;
import cymru.mwnci.sparql.models.EntityWithUri;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CalcFunctionTests extends MwnciInterfaceTestBase {

    private static final Logger logger = LogManager.getLogger(CalcFunctionTests.class);

    private static final EntityWithUri m_robEntity = new EntityWithUri("http://mwnci.cymru/rdf/test/rob", "Rob");
    private static final EntityWithUri m_mwnciEntity = new EntityWithUri("http://mwnci.cymru/rdf/test/mwnci", "Mwnci");

    @BeforeAll
    public void init() {
        this.beforeAll(
                new File("./tests/cymru/mwnci/officeinterface/spreadsheets/LocalRepoTestSheet.ods")
        );
    }

    @AfterAll
    public void end() {
        this.afterAll();
    }

    @Test
    @DisplayName("Test A2 entity lookup")
    public void testA2() throws Exception {
        assertEntityWithUriDisplayedCorrectly("A2", m_mwnciEntity);
    }


    @Test
    @DisplayName("Test B2 Property lookup (creator) by URI")
    public void testB2() throws Exception {
        assertEntityWithUriDisplayedCorrectly("B2", m_robEntity);
    }

    @Test
    @DisplayName("Test C2 Property lookup (DateCreated) by PropertyName")
    public void testC2() throws Exception {
        XCell cell = getCell("C2");

        CellContentType contentType = cell.getType();
        assertEquals(CellContentType.FORMULA, contentType);

        String cellText = MwnciHelpers.getTextFromCell(cell)
                .trim();
        assertEquals("2019-04-01", cellText);
    }

    @Test
    @DisplayName("Test D2 Property lookup (Cool Rating) by PropertyName")
    public void testD2() throws Exception {
        XCell cell = getCell("D2");

        CellContentType contentType = cell.getType();
        assertEquals(CellContentType.FORMULA, contentType);

        assertEquals(1337, cell.getValue());
    }

    @Test
    @DisplayName("Test A3 entity lookup")
    public void testA3() throws Exception {
        assertEntityWithUriDisplayedCorrectly("A3", m_robEntity);
    }

    @Test
    @DisplayName("Test B3 Property lookup (creator) by URI. Ensure cell is blank.")
    public void testB3() throws Exception {
        assertFormulaCellIsBlank("B3");
    }

    @Test
    @DisplayName("Test C3 Property lookup (DateCreated) by PropertyName. Ensure cell is blank.")
    public void testC3() throws Exception {
        assertFormulaCellIsBlank("C3");
    }

    @Test
    @DisplayName("Test D3 Property lookup (Cool Rating) by PropertyName. Ensure too many entities error displayed.")
    public void testD3() throws Exception {
        XCell cell = getCell("D3");

        CellContentType contentType = cell.getType();
        assertEquals(CellContentType.FORMULA, contentType);

        String cellText = MwnciHelpers.getTextFromCell(cell);
        assertEquals("Error: Too many entities found.", cellText);
    }

    @Test
    @DisplayName("Test A8 entity URI lookup works.")
    public void testA8() throws Exception {
        XCell cell = getCell("A8");

        CellContentType contentType = cell.getType();
        assertEquals(CellContentType.FORMULA, contentType);

        String cellText = MwnciHelpers.getTextFromCell(cell);
        assertEquals(m_robEntity.getUri().toString(), cellText);
    }

    @Test
    @DisplayName("Test A9 entity URI lookup works. Ensure failure.")
    public void testA9() throws java.lang.Exception {
        assertFormulaCellHasNoValue("A9");
    }

    private void assertFormulaCellHasNoValue(String cellRef) throws java.lang.Exception {
        XCell cell = getCell(cellRef);

        CellContentType contentType = cell.getType();
        assertEquals(CellContentType.EMPTY, contentType);

        String cellText = MwnciHelpers.getTextFromCell(cell);
        assertEquals("", cellText);
        assertEquals(0, cell.getValue());
    }

    private XCell getCell(String cellRef) throws NoSuchElementException, WrappedTargetException, IndexOutOfBoundsException {
        XSpreadsheet sheet = getSheet("Sheet1");
        XCellRange cellRange = sheet.getCellRangeByName(cellRef);
        return cellRange.getCellByPosition(0, 0);
    }

    private XSpreadsheet getSheet(String sheetName) throws NoSuchElementException, WrappedTargetException {
        Object sheetO = activeSpreadsheetDocument.getSheets().getByName(sheetName);
        return UnoRuntime.queryInterface(XSpreadsheet.class, sheetO);
    }

    private void assertFormulaCellIsBlank(String cellRef) throws NoSuchElementException, WrappedTargetException, IndexOutOfBoundsException {
        XCell cell = getCell(cellRef);

        CellContentType contentType = cell.getType();
        assertEquals(CellContentType.FORMULA, contentType);

        String cellText = MwnciHelpers.getTextFromCell(cell);
        assertEquals("", cellText);
        assertEquals(0, cell.getValue());
    }

    private void assertEntityWithUriDisplayedCorrectly(String cellRef, EntityWithUri entity) throws NoSuchElementException, WrappedTargetException, IndexOutOfBoundsException {
        XCell cell = getCell(cellRef);

        CellContentType contentType = cell.getType();
        assertEquals(CellContentType.FORMULA, contentType);

        String cellText = MwnciHelpers.getTextFromCell(cell);
        String expectedCellText = MwnciHelpers.getDisplayTextForEntityWithUri(entity);
        assertEquals(expectedCellText, cellText);
    }
}
