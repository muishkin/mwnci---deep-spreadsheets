# Mwnci - Deep Spreadsheets

A masters project by ro65. This project aims to provide an extension to integrate live RDF with OpenOffice/LibreOffice. The primary source of RDF data will be the WikiData SPARQL endpoints. This project was inspired by Microsoft Excel's [Project Yellow](https://www.microsoft.com/en-us/garage/wall-of-fame/new-data-types-in-excel/) which integrates the Bing knowledge graph in to Excel. 

![Project Mwnci & WikiData providing planetary data.](https://gitlab.com/muishkin/mwnci---deep-spreadsheets/wikis/uploads/b81f536c4fb7d5693fcd1b307922c80b/ValueSearch_FillDownExample.png)

![Mwnci Demo Video](https://gitlab.com/muishkin/mwnci---deep-spreadsheets/uploads/dd0ec39633b6beb5ad0e3904d774a81d/mwnci_demo.webm)

[Demo Video MP4](https://gitlab.com/muishkin/mwnci---deep-spreadsheets/uploads/9fe47607acc60d337e03a0d020393b6d/mwnci_demo.mp4) / 
[Demo Video webm](https://gitlab.com/muishkin/mwnci---deep-spreadsheets/uploads/dd0ec39633b6beb5ad0e3904d774a81d/mwnci_demo.webm)

## Requirements:

### Basic

* Provide two Calc functions for accessing RDF data:
	* RDFSubject - Annotates a cell as containing a specific RDF Subject.
	* RDFObject - Annotates a cell as containing an object/value related to an RDFSubject by a predicate (in a triple)
* Provide sidebar panels to aid user
	* Help user select the subject
		* Allow for switching between SPARQL query sources.
		* Allows for selection of user language.
		* Provides a list of SPARQL filters which define common entity types.
		* Shows user a selection of available subjects under filters.		
	* Help user select the predicate (and hence related object/value).
		* Allow for switching between SPARQL query sources.
		* Allows for selection of language of entity labels.
		* Shows user a list of available predicates (and current matching objects) which can be chosen.
		* Restricts user selection so that only predicates which appear once (and only once) can be selected - 1-1 mapping required by cells.
		* Only displays immediately related triples.	
* Primary example datasource will be WikiData and will likely utilise the [SPARQL endpoint](https://query.wikidata.org/) for data gathering.
* Allow developers to implement different RDF data sources (implementation of Java interface & appropriate registration of class).

### Stretch

* Attempt to provide basic caching of known values for a given time-period to avoid overloading SPARQL endpoints. 
* Allow the user to extend the list of entity categories with their own custom SPARQL filters. 
* Allow context-specific entity category matching. i.e. if I a user inputs `London`, `Paris`, `Berlin`, `Washington` and then selects these cells for annotations, the extension should be able to infer that the best relationship linking these items is that they are all **capital cities**. Given this context, the extension should also be able to infer (disambiguate) that `Washington` refers to the city of `Washington, D.C.` rather than the state or President `Washington`. Initially this could be done by iterating through the list of available categories and attempting to find a match. For a more general solution, this could be approached in a similar way to [DBpedia Spotlight](https://www.dbpedia-spotlight.org/). N.B. the dbpedia-spotlight approach appears to work with limited success by passing in a comma seperated list of entities to the available [/annotate API endpoint](https://www.dbpedia-spotlight.org/api).

### Future Plans

* Allow federated SPARQL queries to improve distributed nature of querying.
* Provide support for querying across raw RDF triples files.
* Add offline functionality. Once entity selection has been made, allow user to use all available predicates without internet connection. This is an extension of the basic caching plan.
* Extend functionality to cope with 1-Many relationships between subject and predicate. Microsoft Excel team have done this by adding a [dynamic array](https://techcommunity.microsoft.com/t5/Excel-Blog/Preview-of-Dynamic-Arrays-in-Excel/ba-p/252944) data-type which spills out in to adjoining cells. 
* Allow object/value selection across multiple predicates/triples by specifying predicate path (needs quite a bit more thought and can relaistically be done by breaking the query across multiple cells with existing fuctionality).

## Demonstrations of Functionality
A nice demonstration spreadsheet would be:

Book, Author, Date of Birth

* Book - matched using subject matcher interface
* Author - Found for first one by referencing cell and then applied to below cells
* Date of birth - Applied similarly to Author.
