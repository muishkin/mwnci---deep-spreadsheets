#!/bin/bash

killall oosplash
killall libreoffice

./gradlew installOxt

export LOCAL_SPARQL_TEST_REPO="./lib/localRepoData/";
export MWNCI_DEBUG="true";

(libreoffice --calc --norestore &)
