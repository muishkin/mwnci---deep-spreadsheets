package cymru.mwnci.sparql.exceptions;

public class TooManyPropertiesFound extends MwnciException {
    public TooManyPropertiesFound(String message){
        super(message);
    }
}
