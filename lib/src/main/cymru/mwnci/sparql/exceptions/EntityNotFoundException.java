package cymru.mwnci.sparql.exceptions;

import java.net.URI;

public class EntityNotFoundException extends MwnciException {
    public EntityNotFoundException(URI entityUri){
        super("Unable to find entity with URI " + entityUri.toString());
    }
}
