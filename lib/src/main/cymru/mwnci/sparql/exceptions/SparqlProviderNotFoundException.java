package cymru.mwnci.sparql.exceptions;

import java.net.URI;

public class SparqlProviderNotFoundException extends MwnciException {
    public SparqlProviderNotFoundException(String providerName){
        super("Could not find matching SPARQL provider for '"+ providerName + "'");
    }

    public SparqlProviderNotFoundException(URI entityUri) {
        super("Could not find matching SPARQL provider for entityURI '" + entityUri.toString() + "'");
    }

}
