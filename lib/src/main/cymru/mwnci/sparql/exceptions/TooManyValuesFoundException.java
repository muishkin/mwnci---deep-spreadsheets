package cymru.mwnci.sparql.exceptions;

public class TooManyValuesFoundException extends MwnciException {
    public TooManyValuesFoundException(String message){
        super(message);
    }
}
