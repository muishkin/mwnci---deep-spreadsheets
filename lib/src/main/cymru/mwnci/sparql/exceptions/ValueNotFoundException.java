package cymru.mwnci.sparql.exceptions;

import java.net.URI;

public class ValueNotFoundException extends MwnciException {
    public ValueNotFoundException(URI subjectUri, String propertyPredicate){
        super("Unable to find value for subject with URI " + subjectUri.toString() +
                " and predicate: '" + propertyPredicate + "'");
    }
}
