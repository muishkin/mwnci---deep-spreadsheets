package cymru.mwnci.sparql.exceptions;

public abstract class MwnciException extends Exception {
    protected MwnciException(String error){
        super(error);
    }
}
