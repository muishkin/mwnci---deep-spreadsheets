package cymru.mwnci.sparql;

import com.google.inject.Guice;
import com.google.inject.Injector;
import cymru.mwnci.sparql.exceptions.SparqlProviderNotFoundException;
import cymru.mwnci.sparql.helpers.Debug;
import cymru.mwnci.sparql.helpers.SparqlIriHelpers;
import cymru.mwnci.sparql.models.SparqlServiceProvider;
import cymru.mwnci.sparql.services.Interfaces.ISparqlProvider;
import cymru.mwnci.sparql.services.providers.*;

import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;

public class Mwnci {

    private static final Injector m_injector = getInjector();

    public static final ISparqlProvider[] PROVIDERS = getSparqlProviders();

    private static ISparqlProvider[] getSparqlProviders() {
        List<ISparqlProvider> providers = new LinkedList<>();

        providers.add(m_injector.getInstance(WikiDataProvider.class));
        providers.add(m_injector.getInstance(ScotGovtStatisticsProvider.class));
        providers.add(m_injector.getInstance(VanderbiltUniversity.class));
        providers.add(m_injector.getInstance(EuropeanaProvider.class));
        providers.add(m_injector.getInstance(EpoProvider.class));

        if (Debug.IN_DEBUG) {
            providers.add(m_injector.getInstance(LocalTestProvider.class));
        }

        return providers.toArray(new ISparqlProvider[]{});
    }

    private static final Map<String, ISparqlProvider> MAP_NAME_TO_SPARQL_PROVIDER = getMapNameToSparqlProvider();

    private static Injector getInjector() {
        return Guice.createInjector(new AppModule());
    }

    /**
     * Returns an implementation of the requested service interface.
     *
     * @param interfaceClass The interface desired.
     * @param <T>            The interface desired.
     * @return An object implementing the desired service.
     */
    public static <T> T getService(Class<T> interfaceClass) {
        return m_injector.getInstance(interfaceClass);
    }

    /**
     * Returns a SPARQL provider by name.
     *
     * @param providerName Name of the provider to return, e.g. "wikidata". Case-insensitive.
     * @return The provider requested.
     * @throws SparqlProviderNotFoundException when the named provider cannot be found.
     */
    public static ISparqlProvider getSparqlProvider(String providerName) throws SparqlProviderNotFoundException {
        providerName = providerName.toLowerCase();

        if (MAP_NAME_TO_SPARQL_PROVIDER.containsKey(providerName)) {
            return MAP_NAME_TO_SPARQL_PROVIDER.get(providerName);
        }

        Optional<URI> maybeSparqlUri = SparqlIriHelpers.getEntityUriFromString(providerName);
        if (maybeSparqlUri.isPresent()) {
            StandardSparqlProvider standardSparqlProvider = m_injector.getInstance(StandardSparqlProvider.class);
            standardSparqlProvider.init(maybeSparqlUri.get().toString());
            return standardSparqlProvider;
        } else {
            throw new SparqlProviderNotFoundException(providerName);
        }
    }

    private static Optional<ISparqlProvider> getOwningProviderForUri(URI entityUri) {
        for (ISparqlProvider provider : MAP_NAME_TO_SPARQL_PROVIDER.values()) {
            if (provider.ownsUri(entityUri))
                return Optional.of(provider);
        }

        return Optional.ofNullable(null);
    }


    public static List<SparqlServiceProvider> getSparqlServiceProviderUserInfo() {
        return Arrays.stream(PROVIDERS)
                .map(provider -> new SparqlServiceProvider(provider.getServiceName(), provider.getDisplayName()))
                .collect(Collectors.toList());
    }

    public static Optional<ISparqlProvider> getSparqlProviderforUri(URI entityUri) {
        return getOwningProviderForUri(entityUri)
                .flatMap(owningProvider -> {
                    String providerName = owningProvider.getServiceName();
                    for (ISparqlProvider provider : PROVIDERS) {
                        if (provider.getServiceName().equals(providerName))
                            return Optional.of(provider);
                    }

                    return Optional.empty();
                });
    }

    /**
     * Returns a SPARQL provider by name.
     *
     * @param entityUri The entityURI we wish to lookup information for.
     * @return The first provider found which claims ownership of the URI.
     */
    public static Optional<SparqlServiceProvider> getSparqlProviderUserInfoForUri(URI entityUri) {
        return getSparqlProviderforUri(entityUri)
                .map(provider -> new SparqlServiceProvider(provider.getServiceName(), provider.getDisplayName()));
    }

    private static Map<String, ISparqlProvider> getMapNameToSparqlProvider() {
        Map<String, ISparqlProvider> map = new HashMap<>();

        for (ISparqlProvider provider : PROVIDERS) {
            map.put(provider.getServiceName(), provider);
        }

        return map;
    }
}
