package cymru.mwnci.sparql.services;

import com.google.inject.Singleton;
import cymru.mwnci.sparql.exceptions.EntityNotFoundException;
import cymru.mwnci.sparql.exceptions.TooManyValuesFoundException;
import cymru.mwnci.sparql.exceptions.ValueNotFoundException;
import cymru.mwnci.sparql.helpers.EntityHelpers;
import cymru.mwnci.sparql.helpers.NS;
import cymru.mwnci.sparql.helpers.SparqlIriHelpers;
import cymru.mwnci.sparql.helpers.StreamHelpers;
import cymru.mwnci.sparql.models.EntityProperty;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.models.queryconfig.SparqlQueryConfig;
import cymru.mwnci.sparql.services.Interfaces.ISparqlProvider;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleLiteral;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.base.AbstractRepository;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expression;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expressions;
import org.eclipse.rdf4j.sparqlbuilder.constraint.SparqlFunction;
import org.eclipse.rdf4j.sparqlbuilder.core.Prefix;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Queries;
import org.eclipse.rdf4j.sparqlbuilder.core.query.SelectQuery;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatterns;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Iri;

import java.net.URI;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Singleton
public class SparqlService implements ISparqlService {
    public static final String VAR_NAME_VALUE = "value";
    public static final String VAR_NAME_MAYBE_VALUE_LABEL = "maybeValueLabel";
    public static final String VAR_NAME_IS_ENTITY = "isEntity";
    public static final String VAR_NAME_ENTITY_LABEL = "entityLabel";
    public static final String VAR_NAME_SUBJECT = "subject";
    public static final String VAR_NAME_SUBJECT_LABEL = "subjectLabel";
    public static final String VAR_NAME_SUBJECT_DESCRIPTION = "subjectDescription";
    public static final String VAR_NAME_SUBJECT_ORDERING = "subjectOrdering";
    public static final String VAR_NAME_PREDICATE = "predicate";
    public static final String VAR_NAME_PREDICATE_LABEL = "predicateLabel";
    public static final String VAR_NAME_PREDICATE_DESCRIPTION = "predicateDescription";

    private static Logger logger = LogManager.getLogger(SparqlService.class);

    private static final Pattern PROPERTY_SEARCH_PREDICATE_PATTERN = Pattern.compile(
            "^\\s*\\?subject\\s+.+?\\s+\\?value\\s*$",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.UNICODE_CHARACTER_CLASS
    );

    private static GraphPattern getSubjectPredicateValuePattern(
            ISparqlProvider sparqlDataProvider, String propertyPredicate, String predicateLanguage, Iri subject,
            Variable value, Variable maybeValueLabel, Variable predicate, Variable predicateLabel,
            String outputLanguage, Variable isEntity) {

        boolean isPreFormedPredicateExpression = PROPERTY_SEARCH_PREDICATE_PATTERN.matcher(propertyPredicate)
                .matches();

        Optional<URI> maybeUri = SparqlIriHelpers.getEntityUriFromString(propertyPredicate);

        GraphPattern subjectPredicateValuePattern;
        if (isPreFormedPredicateExpression) {
            /**
             * The 'property' value contains an already-formatted GraphPattern of the form:
             * `?subject wdt:predicate ?value`
             * We can just return this assuming that it is valid SPARQL.
             *
             * I've considered the risk of SPARQL injection here without sanitisation of the inputs.
             * I've determined that this doesn't present a problem as this will only affect the user.
             * If they want to write a query themselves, they're perfectly able to do so against these public
             * SPARQL end-points, so what does it matter if we trust their input here?.
             *
             * The worst that could happen is that a user opens a SPARQL-active spreadsheet from an untrusted source
             * and inadvertently executes SPARQL code which inserts, modifies or deletes data on a privileged
             * SPARQL end-point. If your SPARQL end-point is not read-only, don't do what I'm doing here.
             */
            subjectPredicateValuePattern = () -> propertyPredicate + ".";
        } else if (maybeUri.isPresent()) {
            // The user has provided us with the URI of a known predicate. We should use it without question.
            Iri predicateIri = SparqlIriHelpers.mapUriToSparqlIri(maybeUri.get());
            subjectPredicateValuePattern = GraphPatterns.tp(subject, predicateIri, value);
        } else {
            // Else we'll do a free-text search on the name the user has provided us with. It should match a predicate
            // related to the subject.
            GraphPattern predicateLookupPattern = sparqlDataProvider.getPredicateTextSearchGraphPattern(predicate,
                    predicateLabel, propertyPredicate, predicateLanguage, false);

            subjectPredicateValuePattern = GraphPatterns.tp(subject, predicate, value)
                    .and(predicateLookupPattern);
        }

        Expression isLangString = Expressions.equals(
                Expressions.function(SparqlFunction.DATATYPE, value),
                SparqlIriHelpers.mapUriToSparqlIri(NS.TYPE_RDF_LANG_STRING)
        );

        Expression isLangStringAndInCorrectLanguage = Expressions.and(
                isLangString,
                sparqlDataProvider.filterLabelByLanguage(value, outputLanguage)
        );

        return subjectPredicateValuePattern
                // if it's a language string type, ensure it's in the output language
                .filter(
                        Expressions.or(
                                isEntity,
                                Expressions.not(isLangString),
                                isLangStringAndInCorrectLanguage
                        )
                )
                // If it's an object type, ensure we bring out the object's label in the correct language.
                .and(GraphPatterns.optional(
                        sparqlDataProvider.linkEntityToLabel(value, maybeValueLabel)
                                .filter(sparqlDataProvider.filterLabelByLanguage(maybeValueLabel, outputLanguage))
                ))
                .and(() -> "BIND(isIRI(" + value.getQueryString() + ") as " + isEntity.getQueryString() + ")");
    }

    /**
     * Returns prefixes required by the ISparqlProvider unioned with the ones requried for all SPARQL providers.
     *
     * @param sparqlProvider The ISparqlProvider used for the query.
     * @return
     */
    private static Prefix[] getStandardAndProvierSpecificPrefixes(ISparqlProvider sparqlProvider) {
        Set<Prefix> prefixes = new HashSet<>(Arrays.asList(sparqlProvider.getPrefixes()));
        prefixes.add(NS.XSD);
        prefixes.add(NS.RDF);
        prefixes.add(NS.RDFS);
        prefixes.add(NS.OWL);
        prefixes.add(NS.DC);

        return prefixes.toArray(new Prefix[]{});
    }

    public List<EntityWithUri> searchForSubject(ISparqlProvider sparqlProvider,
                                                String searchText,
                                                String searchLanguage,
                                                String sparqlFilter,
                                                int maxNumRecords
    ) {
        SparqlQueryConfig queryConfig = sparqlProvider.getQueryConfig();

        AbstractRepository repo = sparqlProvider.getRepository();

        SelectQuery query = Queries.SELECT();

        query.prefix(getStandardAndProvierSpecificPrefixes(sparqlProvider));

        Variable subject = SparqlBuilder.var(VAR_NAME_SUBJECT);
        Variable subjectLabel = SparqlBuilder.var(VAR_NAME_SUBJECT_LABEL);
        Variable subjectOrdering = SparqlBuilder.var(VAR_NAME_SUBJECT_ORDERING);
        Variable subjectDescription = SparqlBuilder.var(VAR_NAME_SUBJECT_DESCRIPTION);

        GraphPattern searchPattern = sparqlProvider.buildEntitySearchQuery(
                searchText, searchLanguage, sparqlFilter, subject, subjectLabel, subjectOrdering, subjectDescription);

        query = query
                .select(subject, subjectLabel, subjectDescription)
                .distinct()
                .where(searchPattern);

        if (queryConfig.EntitySearch.OrderEntitySearch) {
            query = query.orderBy(subjectOrdering);
        }

        query = query.limit(maxNumRecords);

        return fetchEntityWithUrisFromQuery(repo, query,
                VAR_NAME_SUBJECT, VAR_NAME_SUBJECT_LABEL, VAR_NAME_SUBJECT_DESCRIPTION);
    }

    private List<EntityWithUri> fetchEntityWithUrisFromQuery(AbstractRepository repo, SelectQuery query,
                                                             String fieldName_entityId, String fieldName_entityLabel,
                                                             String fieldName_entityDescription) {
        try (RepositoryConnection con = repo.getConnection()) {

            String queryString = query.getQueryString();

            logger.debug(queryString);

            TupleQuery tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);

            try (TupleQueryResult result = tupleQuery.evaluate()) {
                return StreamHelpers.iterationToStream(result)
                        .map(row -> {
                            String uri = row.getValue(fieldName_entityId).stringValue();
                            String label = row.hasBinding(fieldName_entityLabel)
                                    ? row.getValue(fieldName_entityLabel).stringValue()
                                    : "Error: No Label Found";
                            String description = row.hasBinding(fieldName_entityDescription)
                                    ? row.getValue(fieldName_entityDescription).stringValue()
                                    : null;

                            return new EntityWithUri(uri, label, description);
                        })
                        .collect(Collectors.toList());
            }

        } catch (RDF4JException e) {
            logger.error(e.getMessage(), e);
            throw e;
        }
    }

    public EntityWithUri getEntityForUri(ISparqlProvider sparqlProvider, URI entityUri, String language)
            throws EntityNotFoundException, TooManyValuesFoundException {
        logger.traceEntry();

        AbstractRepository repo = sparqlProvider.getRepository();

        SelectQuery query = Queries.SELECT();

        query.prefix(getStandardAndProvierSpecificPrefixes(sparqlProvider));

        Variable label = SparqlBuilder.var(VAR_NAME_ENTITY_LABEL);

        Iri entityIri = SparqlIriHelpers.mapUriToSparqlIri(entityUri);

        GraphPattern graphPattern = sparqlProvider.linkEntityToLabel(entityIri, label)
                .filter(sparqlProvider.filterLabelByLanguage(label, language));

        query = query.select(label)
                .where(graphPattern);

        try (RepositoryConnection con = repo.getConnection()) {
            String queryString = query.getQueryString();

            logger.debug(queryString);

            TupleQuery tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);

            logger.trace("Executing SPARQL request.");

            try (TupleQueryResult result = tupleQuery.evaluate()) {
                logger.trace("Finished Executing SPARQL request.");

                EntityWithUri entityWithUri;
                if (result.hasNext()) {
                    BindingSet row = result.next();
                    String labelText = row.getValue(VAR_NAME_ENTITY_LABEL).stringValue();
                    entityWithUri = new EntityWithUri(entityUri.toString(), labelText);

                } else {
                    throw new EntityNotFoundException(entityUri);
                }

                if (result.hasNext()) {
                    throw new TooManyValuesFoundException("Too many entities found for URI " + entityUri.toString());
                }

                return logger.traceExit(entityWithUri);
            }
        }
    }

    @Override
    public List<EntityWithUri> searchPropertiesForEntity(ISparqlProvider sparqlDataProvider, URI subjectUri,
                                                         String propertySearchTerm, String searchLanguage,
                                                         String outputLanguage, int maxNumRecords) {
        AbstractRepository repo = sparqlDataProvider.getRepository();

        Variable predicate = SparqlBuilder.var(VAR_NAME_PREDICATE);
        Variable predicateLabel = SparqlBuilder.var(VAR_NAME_PREDICATE_LABEL);
        Variable predicateDescription = SparqlBuilder.var(VAR_NAME_PREDICATE_DESCRIPTION);

        // Value is just used to ensure the subject has a predicate but doesn't actually get used.
        Variable value = SparqlBuilder.var(VAR_NAME_VALUE);

        Iri subjectIri = SparqlIriHelpers.mapUriToSparqlIri(subjectUri);

        GraphPattern predicateLookupPattern = sparqlDataProvider.getPredicateTextSearchGraphPattern(predicate,
                predicateLabel, propertySearchTerm, searchLanguage, true);

        // Filter predicates to ones this particular subject has.
        GraphPattern subjectPredicateLabelPattern = GraphPatterns.tp(subjectIri, predicate, value)
                .and(predicateLookupPattern);
        GraphPattern predicateLinkToDescription = GraphPatterns.optional(
                sparqlDataProvider.linkEntityToDescription(predicate, predicateDescription)
                        .filter(sparqlDataProvider.filterLabelByLanguage(predicateDescription, searchLanguage))
        );

        SelectQuery query = Queries
                .SELECT(predicate, predicateLabel, predicateDescription)
                .distinct()
                .prefix(getStandardAndProvierSpecificPrefixes(sparqlDataProvider))
                .where(
                        subjectPredicateLabelPattern
                                .and(predicateLinkToDescription)
                )
                .limit(maxNumRecords);

        // todo: Want to implement suitable ordering at some point?

        return fetchEntityWithUrisFromQuery(repo, query,
                VAR_NAME_PREDICATE, VAR_NAME_PREDICATE_LABEL, VAR_NAME_PREDICATE_DESCRIPTION);
    }

    public EntityProperty getPropertyValueForEntity(
            ISparqlProvider sparqlDataProvider, URI subjectUri, String propertyPredicate,
            String predicateLanguage, String outputLanguage, boolean allowMultipleValues)
            throws TooManyValuesFoundException, ValueNotFoundException {

        Variable value = SparqlBuilder.var(VAR_NAME_VALUE);
        Variable predicate = SparqlBuilder.var(VAR_NAME_PREDICATE);
        Variable predicateLabel = SparqlBuilder.var(VAR_NAME_PREDICATE_LABEL);
        Variable maybeValueLabel = SparqlBuilder.var(VAR_NAME_MAYBE_VALUE_LABEL);
        Variable isEntity = SparqlBuilder.var(VAR_NAME_IS_ENTITY);

        Iri subjectIri = SparqlIriHelpers.mapUriToSparqlIri(subjectUri);

        GraphPattern subjectPredicateValuePattern = getSubjectPredicateValuePattern(sparqlDataProvider,
                propertyPredicate, predicateLanguage, subjectIri, value, maybeValueLabel,
                predicate, predicateLabel, outputLanguage, isEntity);

        SelectQuery query = Queries
                .SELECT(value, predicate, maybeValueLabel, isEntity)
                .distinct()
                .prefix(getStandardAndProvierSpecificPrefixes(sparqlDataProvider))
                .where(subjectPredicateValuePattern)
                .limit(2); // Fetch one more than we anticipate to ensure we don't have too many matches.

        AbstractRepository repo = sparqlDataProvider.getRepository();
        try (RepositoryConnection con = repo.getConnection()) {
            String queryString = query.getQueryString();

            logger.debug(queryString);

            TupleQuery tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString);

            logger.trace("Executing SPARQL request.");

            try (TupleQueryResult result = tupleQuery.evaluate()) {
                logger.trace("Finished Executing SPARQL request.");

                EntityProperty entityProperty =
                        GetEntityProperty(subjectUri, result, propertyPredicate, allowMultipleValues);

                return logger.traceExit(entityProperty);
            }
        }

    }

    private EntityProperty GetEntityProperty(URI subjectUri, TupleQueryResult result, String propertyPredicate,
                                             boolean allowMultipleValues)
            throws ValueNotFoundException, TooManyValuesFoundException {
        List<EntityProperty> entityPropertyValues = new LinkedList<>();

        while (result.hasNext()) {
            entityPropertyValues.add(getEntityPropertyForNextRow(result));
        }

        if (entityPropertyValues.isEmpty())
            throw new ValueNotFoundException(subjectUri, propertyPredicate);

        if (entityPropertyValues.size() == 1)
            return entityPropertyValues.get(0);

        if (!allowMultipleValues)
            throw new TooManyValuesFoundException("Too many entities found.");

        String aggregateValue = entityPropertyValues.stream()
                .map(val -> {
                    boolean propertyIsEntity = val.getPropertyIsEntity();

                    if (propertyIsEntity) {
                        return EntityHelpers.getDisplayTextForEntityWithUri(val.getEntityWithUri());
                    }

                    return val.getValue()
                            .toString();
                })
                .collect(Collectors.joining(", "));

        EntityProperty aggregateProperty = EntityProperty.createForValue(aggregateValue);

        return aggregateProperty;
    }

    private EntityProperty getEntityPropertyForNextRow(TupleQueryResult result) {
        EntityProperty entityProperty;
        BindingSet row = result.next();

        Value value = row.getValue(VAR_NAME_VALUE);

        if (value instanceof IRI) {
            // Don't treat it as an entity IRI unless it has a label. Treat it as a URL instead (image, etc).
            if (row.hasBinding(VAR_NAME_MAYBE_VALUE_LABEL)) {

                // The value is an object.
                String uriString = value.stringValue();
                String entityLabel = row.getValue(VAR_NAME_MAYBE_VALUE_LABEL).stringValue();

                entityProperty = EntityProperty.createForEntity(
                        new EntityWithUri(uriString, entityLabel)
                );
            } else {
                // Treat it as a plain URL for an image or similar.
                entityProperty = EntityProperty.createForValue(
                        value.stringValue()
                );
            }
        } else {
            SimpleLiteral literalValue = (SimpleLiteral) value;

            IRI dataType = literalValue.getDatatype();
            String dataTypeUri = dataType.toString();

            Object valueObj;
            switch (dataTypeUri) {
                case NS.TYPE_RDF_LANG_STRING:
                case NS.TYPE_XSD_STRING:
                case NS.TYPE_GEO_WKT:
                case NS.TYPE_XSD_BOOLEAN:
                    valueObj = literalValue.stringValue();
                    break;
                case NS.TYPE_XSD_INT:
                case NS.TYPE_XSD_DECIMAL:
                case NS.TYPE_XSD_DOUBLE:
                    valueObj = literalValue.doubleValue();
                    break;
                case NS.TYPE_XSD_DATE:
                case NS.TYPE_XSD_DATE_TIME:
                case NS.TYPE_XSD_TIME:
                    valueObj = literalValue.calendarValue();
                    break;
                default:
                    throw new RuntimeException("DataType not handled: " + dataTypeUri);
            }

            entityProperty = EntityProperty.createForValue(valueObj);
        }
        return entityProperty;
    }
}
