package cymru.mwnci.sparql.services.Interfaces;

import cymru.mwnci.sparql.exceptions.EntityNotFoundException;
import cymru.mwnci.sparql.exceptions.TooManyValuesFoundException;
import cymru.mwnci.sparql.exceptions.ValueNotFoundException;
import cymru.mwnci.sparql.models.EntityProperty;
import cymru.mwnci.sparql.models.EntityWithUri;

import java.net.URI;
import java.util.List;

public interface ISparqlService {
    List<EntityWithUri> searchForSubject(ISparqlProvider sparqlProvider, String searchText, String searchLanguage,
                                         String sparqlFilter, int maxNumRecords);

    EntityWithUri getEntityForUri(ISparqlProvider sparqlProvider, URI entityUri, String language)
            throws EntityNotFoundException, TooManyValuesFoundException;

    List<EntityWithUri> searchPropertiesForEntity(ISparqlProvider sparqlDataProvider, URI subjectUri,
                                                  String propertyPredicate, String predicateLanguage,
                                                  String outputLanguage, int maxNumRecords);


    EntityProperty getPropertyValueForEntity(ISparqlProvider sparqlDataProvider, URI subjectUri,
                                             String propertyPredicate, String predicateLanguage, String outputLanguage, boolean allowMultipleValues)
            throws TooManyValuesFoundException, ValueNotFoundException;
}
