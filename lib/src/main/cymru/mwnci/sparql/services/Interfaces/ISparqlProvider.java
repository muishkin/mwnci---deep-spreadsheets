package cymru.mwnci.sparql.services.Interfaces;

import cymru.mwnci.sparql.models.UserSubjectEntityFilter;
import cymru.mwnci.sparql.models.queryconfig.SparqlQueryConfig;
import org.eclipse.rdf4j.repository.base.AbstractRepository;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expression;
import org.eclipse.rdf4j.sparqlbuilder.core.Prefix;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Query;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern;
import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfPredicate;
import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfSubject;

import java.net.URI;
import java.util.List;

public interface ISparqlProvider {
    Prefix[] getPrefixes();

    RdfPredicate getIsOrIsSubclassPredicate();

    GraphPattern linkEntityToLabel(RdfSubject subject, Variable subjectLabel);

    GraphPattern linkEntityToDescription(RdfSubject subject, Variable subjectDescription);

    Expression filterLabelByLanguage(Variable label, String language);

    Expression filterByTextSearch(Variable label, String searchTerm, boolean startsWith, boolean endsWith);

    <T extends Query> T setDefaultOrder(T query, Variable subject);

    AbstractRepository getRepository();

    GraphPattern getPredicateTextSearchGraphPattern(Variable predicate, Variable predicateLabel, String propertySearchTerm, String propertyLanguage, boolean allowEmptyLabel);

    List<UserSubjectEntityFilter> getUserSubjectFilters();

    GraphPattern buildEntitySearchQuery(String searchText,
                                        String searchLanguage, String sparqlFilter,
                                        Variable subject,
                                        Variable subjectLabel, Variable subjectOrdering, Variable subjectDescription);

    /**
     * Informs the caller of whether this SparqlProvider owns the entityUri provided.
     *
     * @return boolean representing whether this provider owns the URI.
     */
    boolean ownsUri(URI entityUri);

    /**
     * Provides the name of this SPARQL provider.
     *
     * @return
     */
    String getServiceName();

    /**
     * Provides the display name of this SPARQL provider.
     *
     * @return
     */
    String getDisplayName();

    SparqlQueryConfig getQueryConfig();
}
