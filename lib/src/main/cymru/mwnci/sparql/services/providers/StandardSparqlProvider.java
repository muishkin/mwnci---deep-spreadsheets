package cymru.mwnci.sparql.services.providers;

import cymru.mwnci.sparql.helpers.NS;
import cymru.mwnci.sparql.helpers.SparqlIriHelpers;
import cymru.mwnci.sparql.models.UserSubjectEntityFilter;
import cymru.mwnci.sparql.models.queryconfig.SparqlQueryConfig;
import cymru.mwnci.sparql.services.Interfaces.ISparqlProvider;
import org.eclipse.rdf4j.repository.base.AbstractRepository;
import org.eclipse.rdf4j.repository.sparql.SPARQLRepository;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expression;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expressions;
import org.eclipse.rdf4j.sparqlbuilder.constraint.SparqlFunction;
import org.eclipse.rdf4j.sparqlbuilder.core.Prefix;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Query;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatterns;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf;
import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfPredicate;
import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfSubject;

import java.net.URI;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class StandardSparqlProvider implements ISparqlProvider {

    protected final static UserSubjectEntityFilter[] FILTERS = new UserSubjectEntityFilter[]{
            new UserSubjectEntityFilter("Any", null)
    };

    protected AbstractRepository repository;
    private String m_repoUrl;

    public void init(String repoUrl) {
        m_repoUrl = repoUrl;
        repository = new SPARQLRepository(repoUrl);
    }

    public Prefix[] getPrefixes() {
        return new Prefix[0];
    }

    public RdfPredicate getIsOrIsSubclassPredicate() {
        return () -> RdfPredicate.a.getQueryString()
                + "/(" + NS.RDFS.iri("subClassOf").getQueryString()
                + "|" + NS.OWL.iri("equivalentClass").getQueryString() + ")*";
    }

    public GraphPattern linkEntityToLabel(RdfSubject subject, Variable subjectLabel) {
        return GraphPatterns.tp(subject, NS.RDFS.iri("label"), subjectLabel);
    }

    public GraphPattern linkEntityToDescription(RdfSubject subject, Variable subjectDescription) {
        // todo: Check this is right.
        return GraphPatterns.tp(subject, NS.RDFS.iri("comment"), subjectDescription);
    }

    public Expression filterLabelByLanguage(Variable label, String language) {
        Expression isLangString = Expressions.equals(
                Expressions.function(SparqlFunction.DATATYPE, label),
                SparqlIriHelpers.mapUriToSparqlIri(NS.TYPE_RDF_LANG_STRING)
        );

        Expression langStringMatches = Expressions.equals(
                Expressions.function(SparqlFunction.LANG, label),
                Rdf.literalOf(language)
        );

        return Expressions.or(
                Expressions.not(isLangString),
                langStringMatches
        ).parenthesize();
    }

    public Expression filterByTextSearch(Variable label, String searchTerm, boolean startsWith, boolean endsWith) {
        if (searchTerm == null || searchTerm.isEmpty()) {
            return Expressions.equals(Rdf.literalOf(true), Rdf.literalOf(true));
        }

        Pattern escapedSearchTerm = Pattern.compile(searchTerm, Pattern.LITERAL);

        StringBuilder searchPattern = new StringBuilder();

        if (startsWith)
            searchPattern.append("^");

        searchPattern.append(escapedSearchTerm.pattern());

        if (endsWith)
            searchPattern.append("$");

        return Expressions.regex(label, searchPattern.toString(), "i");
    }

    public <T extends Query> T setDefaultOrder(T query, Variable subject) {
        // todo: Improve this default implementation.
        return (T) query
                .orderBy(() -> "asc(" + subject.getQueryString() + ")");
    }

    public AbstractRepository getRepository() {
        if (!repository.isInitialized()) {
            repository.init();
        }

        return repository;
    }

    public GraphPattern getPredicateTextSearchGraphPattern(
            Variable predicate, Variable predicateLabel, String propertySearchTerm, String propertyLanguage,
            boolean allowEmptyLabel) {

        GraphPattern entityLabelLink = linkEntityToLabel(predicate, predicateLabel);
        if (allowEmptyLabel) {
            entityLabelLink = GraphPatterns.optional(entityLabelLink);
        }

        // Ensure this property is optional since a bunch of SPARQL endpoints use properties without
        // providing the necessary lookup-definitions for us. Thanks EU.
        return entityLabelLink
                .filter(
                        Expressions.and(
                                filterLabelByLanguage(predicateLabel, propertyLanguage),
                                filterByTextSearch(predicateLabel, propertySearchTerm,
                                        true, true)
                        )
                );
    }

    public List<UserSubjectEntityFilter> getUserSubjectFilters() {
        return Arrays.asList(FILTERS);
    }

    public GraphPattern buildEntitySearchQuery(
            String searchText,
            String searchLanguage, String sparqlFilter,
            Variable subject,
            Variable subjectLabel,
            Variable subjectOrdering,
            Variable subjectDescription) {
        List<Expression> filters = new LinkedList<>();

        // Link the subject to the subjectLabel and ensure it's in the appropriate language
        GraphPattern entityLinkToLabel = this.linkEntityToLabel(subject, subjectLabel);
        GraphPattern entityLinkToDescription = GraphPatterns.optional(
                this.linkEntityToDescription(subject, subjectDescription)
                        .filter(this.filterLabelByLanguage(subjectDescription, searchLanguage))
        );

        GraphPattern matchingGraphPattern = entityLinkToLabel
                .and(entityLinkToDescription);
        if (!(sparqlFilter == null || sparqlFilter.isEmpty())) {
            sparqlFilter = sparqlFilter.trim();
            if (!sparqlFilter.endsWith("."))
                sparqlFilter += ".";

            final String sparqlFilterFinal = sparqlFilter;
            matchingGraphPattern = matchingGraphPattern.and(
                    () -> sparqlFilterFinal
            );
        }

        matchingGraphPattern = matchingGraphPattern
                .and(() -> "BIND(" + subjectLabel.getQueryString() + " as " + subjectOrdering.getQueryString() + ")");

        // Search by searchText - exact match.
        filters.add(this.filterByTextSearch(subjectLabel, searchText, false, false));
        filters.add(this.filterLabelByLanguage(subjectLabel, "en"));


        Expression combinedFilters = Expressions.and(filters.toArray(new Expression[]{}));
        matchingGraphPattern = matchingGraphPattern.filter(combinedFilters);


        return matchingGraphPattern;
    }

    public boolean ownsUri(URI entityUri) {
        return false;
    }

    public String getServiceName() {
        return m_repoUrl;
    }

    public String getDisplayName() {
        return m_repoUrl;
    }

    public SparqlQueryConfig getQueryConfig() {
        SparqlQueryConfig config = new SparqlQueryConfig();

        config.EntitySearch.OrderEntitySearch = false;

        return config;
    }
}

