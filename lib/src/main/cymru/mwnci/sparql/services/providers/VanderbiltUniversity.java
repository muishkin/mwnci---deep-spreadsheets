package cymru.mwnci.sparql.services.providers;

import cymru.mwnci.sparql.models.queryconfig.SparqlQueryConfig;

import java.net.URI;

public class VanderbiltUniversity extends StandardSparqlProvider {

    public VanderbiltUniversity() {
        this.init("https://sparql.vanderbilt.edu/namespace/kb/sparql");
    }

    @Override
    public boolean ownsUri(URI entityUri) {
        return entityUri.getHost()
                .toLowerCase()
                .endsWith("vanderbilt.edu");
    }

    @Override
    public SparqlQueryConfig getQueryConfig() {
        SparqlQueryConfig config = new SparqlQueryConfig();

        config.EntitySearch.OrderEntitySearch = false;

        return config;
    }

    @Override
    public String getServiceName() {
        return "vanderbiltuni";
    }

    @Override
    public String getDisplayName() {
        return "Vanderbilt University";
    }

}
