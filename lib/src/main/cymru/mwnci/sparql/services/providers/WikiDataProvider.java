package cymru.mwnci.sparql.services.providers;

import com.google.inject.Singleton;
import cymru.mwnci.sparql.helpers.NS;
import cymru.mwnci.sparql.models.UserSubjectEntityFilter;
import cymru.mwnci.sparql.models.queryconfig.SparqlQueryConfig;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expression;
import org.eclipse.rdf4j.sparqlbuilder.constraint.Expressions;
import org.eclipse.rdf4j.sparqlbuilder.core.Prefix;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.core.query.Query;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPatterns;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Iri;
import org.eclipse.rdf4j.sparqlbuilder.rdf.Rdf;
import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfPredicate;
import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfSubject;

import java.net.URI;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Singleton
public class WikiDataProvider extends StandardSparqlProvider {

    public static final String WIKIBASE_NAMESPACE_URI = "http://wikiba.se/ontology#";
    public static final Prefix WIKIBASE = SparqlBuilder.prefix(
            new SimpleNamespace("wikibase", WIKIBASE_NAMESPACE_URI));
    public static final Prefix WD = SparqlBuilder.prefix(
            new SimpleNamespace("wd", "http://www.wikidata.org/entity/"));
    public static final Prefix WDT = SparqlBuilder.prefix(
            new SimpleNamespace("wdt", "http://www.wikidata.org/prop/direct/"));
    public static final Prefix P = SparqlBuilder.prefix(
            new SimpleNamespace("p", "http://www.wikidata.org/prop/"));
    public static final Prefix BD = SparqlBuilder.prefix(
            new SimpleNamespace("bd", "http://www.bigdata.com/rdf#"));

    private static final String IS_OR_SUBCLASS_STR = "wdt:P31/wdt:P279*";
    private static final UserSubjectEntityFilter[] WIKIDATA_FILTERS = new UserSubjectEntityFilter[]{
            new UserSubjectEntityFilter("Town", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q3957"),
            new UserSubjectEntityFilter("City", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q515"),
            new UserSubjectEntityFilter("County", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q28575"),
            new UserSubjectEntityFilter("Country", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q6256"),
            new UserSubjectEntityFilter("Continent", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q5107"),
            new UserSubjectEntityFilter("Planet", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q634"),
            new UserSubjectEntityFilter("Physical Constant", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q173227"),
            new UserSubjectEntityFilter("Company", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q783794"),
            new UserSubjectEntityFilter("Charity", "?subject " + IS_OR_SUBCLASS_STR + " wd:Q708676")
    };

    public WikiDataProvider() {
        this.init("https://query.wikidata.org/sparql");
    }

    @Override
    public Prefix[] getPrefixes() {
        return new Prefix[]{WIKIBASE, WD, WDT, P, BD};
    }

    @Override
    public RdfPredicate getIsOrIsSubclassPredicate() {
        Iri isPredicate = WDT.iri("P31");
        Iri subclassPredicate = WDT.iri("P279");

        return () -> isPredicate.getQueryString() + "/" + subclassPredicate.getQueryString() + "*";
    }

    /**
     * Order by number of site-links.
     * I am hoping this is a proxy for popularity.
     * <p>
     * e.g. https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples#People_deceased_in_2018_ordered_by_the_number_of_sitelinks
     *
     * @param query   The query to order
     * @param subject The subject we should be ordering on.
     * @param <T>     The type of query, usually SelectQuery
     * @return Returns the modified query back to the caller.
     */
    @Override
    public <T extends Query> T setDefaultOrder(T query, Variable subject) {
        Variable subjectOrdering = SparqlBuilder.var("subjectOrdering");

        return (T) query
                .where(subject.has(WIKIBASE.iri("sitelinks"), subjectOrdering))
                .orderBy(() -> "desc(" + subjectOrdering.getQueryString() + ")");
    }

    @Override
    public GraphPattern getPredicateTextSearchGraphPattern(Variable predicate, Variable predicateLabel,
                                                           String propertySearchTerm, String propertyLanguage, boolean allowEmptyLabel) {
        Variable predicateLookup = SparqlBuilder.var("predicateLookup");

        return GraphPatterns
                .select(predicate, predicateLabel)
                .where(
                        GraphPatterns
                                .tp(predicateLookup, RdfPredicate.a, WIKIBASE.iri("Property"))
                                .and(linkEntityToLabel(predicateLookup, predicateLabel))
                                .filter(
                                        Expressions.and(
                                                filterLabelByLanguage(predicateLabel, propertyLanguage),
                                                filterByTextSearch(predicateLabel, propertySearchTerm, false, false)
                                        )
                                )
                                .and(GraphPatterns.tp(predicateLookup, WIKIBASE.iri("directClaim"), predicate))
                );
    }

    @Override
    public List<UserSubjectEntityFilter> getUserSubjectFilters() {
        List<UserSubjectEntityFilter> filters = new LinkedList<>(super.getUserSubjectFilters());

        filters.addAll(
                Arrays.asList(WIKIDATA_FILTERS)
        );

        return filters;
    }


    /**
     * Query for wikidata which works MUCH faster than default nonsense...
     * <p>
     * SELECT DISTINCT ?subject ?subjectLabel ?subjectNum
     * WHERE {
     * <p>
     * SERVICE wikibase:mwapi {
     * bd:serviceParam wikibase:api "EntitySearch" .
     * bd:serviceParam wikibase:endpoint "www.wikidata.org" .
     * bd:serviceParam mwapi:search "aber" .
     * bd:serviceParam mwapi:language "en" .
     * ?subject wikibase:apiOutputItem mwapi:item .
     * ?subjectNum wikibase:apiOrdinal true .
     * }
     * ?subject wikibase:sitelinks ?subjectOrdering;
     * rdfs:label ?subjectLabel .
     * FILTER ( LANG( ?subjectLabel ) = "en" )
     * #  FILTER(contains( lcase(?subjectLabel), "aberystwyth")).
     * }
     * ORDER BY asc(?subjectNum)
     * LIMIT 10
     */
    @Override
    public GraphPattern buildEntitySearchQuery(
            String searchText, String searchLanguage, String sparqlFilter, Variable subject, Variable subjectLabel,
            Variable subjectOrdering,
            Variable subjectDescription) {
        GraphPattern searchServiceQuery;
        if ((searchText == null || searchText.isEmpty())
                && (sparqlFilter != null && !sparqlFilter.isEmpty())) {
            // In order to bring back *some* results when there is no search term, we run a slightly different query.
            // Order by popularity of article rather than the search service.
            Variable numSiteLinks = SparqlBuilder.var("numSiteLinks");
            searchServiceQuery = subject.has(WIKIBASE.iri("sitelinks"), numSiteLinks)
                    .and(
                            () -> "BIND(-" + numSiteLinks.getQueryString() + " as " + subjectOrdering.getQueryString() + ")"
                    );

        } else {
            searchServiceQuery = () -> {
                StringBuilder search = new StringBuilder();

                search.append(System.lineSeparator());

                search.append("SERVICE " + WIKIBASE.iri("mwapi").getQueryString() + " {");
                search.append(System.lineSeparator());

                String bdServiceParam = BD.iri("serviceParam").getQueryString();

                search.append(bdServiceParam + " " + WIKIBASE.iri("api").getQueryString() + " \"EntitySearch\" .");
                search.append(System.lineSeparator());
                search.append(bdServiceParam + " " + WIKIBASE.iri("endpoint").getQueryString() + " \"www.wikidata.org\" .");
                search.append(System.lineSeparator());
                // Rdf.literalOf().getQueryString() handles escaping of text values for us.
                search.append(bdServiceParam + " mwapi:search " + Rdf.literalOf(searchText).getQueryString() + " .");
                search.append(System.lineSeparator());
                // Rdf.literalOf().getQueryString() handles escaping of text values for us.
                search.append(bdServiceParam + " mwapi:language " + Rdf.literalOf(searchLanguage).getQueryString() + " .");
                search.append(System.lineSeparator());
                search.append(subject.getQueryString() + " " + WIKIBASE.iri("apiOutputItem").getQueryString() + " mwapi:item .");
                search.append(System.lineSeparator());
                search.append(subjectOrdering.getQueryString() + " " + WIKIBASE.iri("apiOrdinal").getQueryString() + " true .");
                search.append(System.lineSeparator());

                search.append("}");
                search.append(System.lineSeparator());

                return search.toString();
            };
        }

        List<Expression> filters = new LinkedList<>();

        // Link the subject to the subjectLabel and ensure it's in the appropriate language
        GraphPattern matchingGraphPattern = searchServiceQuery
                .and(this.linkEntityToLabel(subject, subjectLabel))
                .and(
                        GraphPatterns.optional(
                                this.linkEntityToDescription(subject, subjectDescription)
                                        .filter(this.filterLabelByLanguage(subjectDescription, searchLanguage))
                        )
                );

        if (sparqlFilter != null && !sparqlFilter.isEmpty()) {
            matchingGraphPattern = matchingGraphPattern.and(() -> sparqlFilter + ".");
        }

        // Search by returned label.
        filters.add(this.filterLabelByLanguage(subjectLabel, "en"));

        Expression combinedFilters = Expressions.and(filters.toArray(new Expression[]{}));
        matchingGraphPattern = matchingGraphPattern.filter(combinedFilters);

        return matchingGraphPattern;
    }

    @Override
    public boolean ownsUri(URI entityUri) {
        return entityUri.getHost()
                .toLowerCase()
                .endsWith("wikidata.org");
    }

    @Override
    public String getServiceName() {
        return "wikidata";
    }

    @Override
    public String getDisplayName() {
        return "WikiData";
    }

    @Override
    public SparqlQueryConfig getQueryConfig() {
        SparqlQueryConfig config = new SparqlQueryConfig();

        config.EntitySearch.OrderEntitySearch = true;

        return config;
    }


    @Override
    public GraphPattern linkEntityToDescription(RdfSubject subject, Variable subjectDescription) {
        return GraphPatterns.tp(subject, NS.SCHEMA.iri("description"), subjectDescription);
    }
}
