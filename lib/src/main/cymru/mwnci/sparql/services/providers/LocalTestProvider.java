package cymru.mwnci.sparql.services.providers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.base.AbstractRepository;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.sail.inferencer.fc.SchemaCachingRDFSInferencer;
import org.eclipse.rdf4j.sail.memory.MemoryStore;

import java.io.File;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * LocalTestProvider should only be used when testing before project build.
 */
public class LocalTestProvider extends StandardSparqlProvider {

    /**
     * Needs to be lower case.
     */
    public static final String NAME = "test";
    private final String m_localRepoPath;

    private static final Logger logger = LogManager.getLogger(LocalTestProvider.class);

    public LocalTestProvider() {
        String localRepoPath = System.getenv("LOCAL_SPARQL_TEST_REPO");

        if (localRepoPath == null)
            localRepoPath = System.getProperty("LOCAL_SPARQL_TEST_REPO");

        if (localRepoPath == null)
            localRepoPath = "./localRepoData";

        m_localRepoPath = localRepoPath;

        repository = new SailRepository(
                new SchemaCachingRDFSInferencer(
                        new MemoryStore()
                )
        );

        initialiseRepoData();
    }


    /**
     * Initialises the repository with some data for testing.
     */
    private void initialiseRepoData() {
        AbstractRepository repo = getRepository();

        try (RepositoryConnection con = repo.getConnection()) {
            try {
                File dataFolder = new File(m_localRepoPath);
                List<File> turtleFiles = Stream.of(dataFolder.listFiles())
                        .filter(p -> p.getName().endsWith(".ttl"))
                        .collect(Collectors.toList());

                for (File file : turtleFiles) {
                    con.add(file, "http://mwnci.cymru/rdf/test", RDFFormat.TURTLE);
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    @Override
    public boolean ownsUri(URI entityUri) {
        return entityUri.getHost()
                .toLowerCase()
                .endsWith("mwnci.cymru");
    }

    @Override
    public String getServiceName() {
        return NAME;
    }

    @Override
    public String getDisplayName() {
        return "Local Test Repo";
    }

}
