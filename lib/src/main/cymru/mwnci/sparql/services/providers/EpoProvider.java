package cymru.mwnci.sparql.services.providers;

import cymru.mwnci.sparql.models.queryconfig.SparqlQueryConfig;

import java.net.URI;

public class EpoProvider extends StandardSparqlProvider {

    public EpoProvider() {
        this.init("https://data.epo.org/linked-data/query");
    }

    @Override
    public boolean ownsUri(URI entityUri) {
        return entityUri.getHost()
                .toLowerCase()
                .endsWith("epo.org");
    }

    @Override
    public SparqlQueryConfig getQueryConfig() {
        SparqlQueryConfig config = new SparqlQueryConfig();

        config.EntitySearch.OrderEntitySearch = false;

        return config;
    }

    @Override
    public String getServiceName() {
        return "epo";
    }

    @Override
    public String getDisplayName() {
        return "European Patent Office";
    }

}
