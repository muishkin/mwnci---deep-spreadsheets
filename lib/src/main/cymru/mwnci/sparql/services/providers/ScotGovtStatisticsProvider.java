package cymru.mwnci.sparql.services.providers;

import cymru.mwnci.sparql.models.queryconfig.SparqlQueryConfig;

import java.net.URI;

public class ScotGovtStatisticsProvider extends StandardSparqlProvider {

    public ScotGovtStatisticsProvider(){
        this.init("https://statistics.gov.scot/sparql");
    }

    @Override
    public boolean ownsUri(URI entityUri) {
        return entityUri.getHost()
                .toLowerCase()
                .endsWith("statistics.gov.scot");
    }

    @Override
    public SparqlQueryConfig getQueryConfig() {
        SparqlQueryConfig config = new SparqlQueryConfig();

        config.EntitySearch.OrderEntitySearch = false;

        return config;
    }

    @Override
    public String getServiceName() {
        return "scotgovtstats";
    }

    @Override
    public String getDisplayName() {
        return "Scottish Government Statistics";
    }

}
