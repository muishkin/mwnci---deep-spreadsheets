package cymru.mwnci.sparql.services.providers;

import cymru.mwnci.sparql.helpers.NS;
import cymru.mwnci.sparql.models.queryconfig.SparqlQueryConfig;
import org.eclipse.rdf4j.sparqlbuilder.core.Variable;
import org.eclipse.rdf4j.sparqlbuilder.graphpattern.GraphPattern;
import org.eclipse.rdf4j.sparqlbuilder.rdf.RdfSubject;

import java.net.URI;

public class EuropeanaProvider extends StandardSparqlProvider {

    public EuropeanaProvider() {
        this.init("http://sparql.europeana.eu/");
    }

    @Override
    public boolean ownsUri(URI entityUri) {
        return entityUri.getHost()
                .toLowerCase()
                .endsWith("data.europeana.eu");
    }

    @Override
    public SparqlQueryConfig getQueryConfig() {
        SparqlQueryConfig config = new SparqlQueryConfig();

        config.EntitySearch.OrderEntitySearch = false;

        return config;
    }

    @Override
    public GraphPattern linkEntityToLabel(RdfSubject subject, Variable subjectLabel) {
        return subject.has(NS.DC.iri("title"), subjectLabel);
    }

    @Override
    public String getServiceName() {
        return "europeana";
    }

    @Override
    public String getDisplayName() {
        return "Europeana";
    }

}
