package cymru.mwnci.sparql.models.queryconfig;

/**
 * Allows ISparqlProviders to provide some hints to the SPARQL Provider to improve query performance.
 */
public class SparqlQueryConfig {

    public EntitySearchQueryConfig EntitySearch = new EntitySearchQueryConfig();

}
