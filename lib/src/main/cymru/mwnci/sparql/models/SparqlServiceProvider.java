package cymru.mwnci.sparql.models;

import cymru.mwnci.sparql.helpers.ObjectHelpers;

public class SparqlServiceProvider implements IMwnciDeepCopy<SparqlServiceProvider> {
    private String m_serviceName;
    private String m_serviceDisplayName;

    /**
     * Necessary for Jackson JSON deserialisation.
     */
    public SparqlServiceProvider(){}

    public SparqlServiceProvider(String serviceName, String serviceDisplayName) {
        this.m_serviceName = serviceName;
        this.m_serviceDisplayName = serviceDisplayName;
    }

    public String getServiceName() {
        return m_serviceName;
    }

    public void setServiceName(String serviceName) { m_serviceName = serviceName; }

    public String getServiceDisplayName() {
        return m_serviceDisplayName;
    }

    public void setServiceDisplayName(String serviceDisplayName) { m_serviceDisplayName = serviceDisplayName; }

    public SparqlServiceProvider deepCopy() {
        return new SparqlServiceProvider(this.m_serviceName, this.m_serviceDisplayName);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SparqlServiceProvider)) {
            return false;
        }

        SparqlServiceProvider other = (SparqlServiceProvider) obj;

        return ObjectHelpers.equals(this.m_serviceName, other.m_serviceName)
                && ObjectHelpers.equals(this.m_serviceDisplayName, other.m_serviceDisplayName);
    }

    @Override
    public int hashCode() {
        int hashCode = ~0;
        if (this.m_serviceName != null)
            hashCode *= this.m_serviceName.hashCode();
        if (this.m_serviceDisplayName != null)
            hashCode *= this.m_serviceDisplayName.hashCode();

        return hashCode;
    }
}
