package cymru.mwnci.sparql.models;

import cymru.mwnci.sparql.helpers.ObjectHelpers;

/**
 * Model containing a filter which the user can apply when searching for a subject entity.
 */
public class UserSubjectEntityFilter implements IMwnciDeepCopy<UserSubjectEntityFilter> {
    private String m_displayName;
    /**
     * Sparql statement must be of the form:
     * <p>
     * `?subject some:predicate some:value`
     * <p>
     * It will be used when filtering values for display in the search listbox.
     */
    private String m_sparqlStatement;

    /**
     * Necessary for Jackson JSON deserialisation.
     */
    public UserSubjectEntityFilter(){}

    public UserSubjectEntityFilter(String displayName, String sparqlStatement) {
        this.m_displayName = displayName;
        this.m_sparqlStatement = sparqlStatement;
    }

    public String getDisplayName(){
        return this.m_displayName;
    }

    public void setDisplayName(String displayName){
        this.m_displayName = displayName;
    }

    public String getSparqlStatement(){
        return this.m_sparqlStatement;
    }

    public void setSparqlStatement(String sparqlStatement){
        this.m_sparqlStatement = sparqlStatement;
    }

    public UserSubjectEntityFilter deepCopy() {
        return new UserSubjectEntityFilter(m_displayName, m_sparqlStatement);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof UserSubjectEntityFilter)) {
            return false;
        }

        UserSubjectEntityFilter other = (UserSubjectEntityFilter) obj;

        return ObjectHelpers.equals(this.m_displayName, other.m_displayName)
                && ObjectHelpers.equals(this.m_sparqlStatement, other.m_sparqlStatement);
    }

    @Override
    public int hashCode() {
        int hashCode = ~0;

        if (this.m_displayName != null)
            hashCode *= this.m_displayName.hashCode();

        if (this.m_sparqlStatement != null)
            hashCode *= this.m_sparqlStatement.hashCode();

        return hashCode;
    }
}
