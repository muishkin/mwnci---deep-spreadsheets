package cymru.mwnci.sparql.models;

import cymru.mwnci.sparql.helpers.ObjectHelpers;

import java.net.URI;


public class EntityWithUri implements IMwnciDeepCopy<EntityWithUri> {
    private String m_label;
    private URI m_uri;
    private String m_description;

    public EntityWithUri(String uri, String label) {
        this.m_uri = URI.create(uri);
        this.m_label = label;
        this.m_description = null;
    }


    public EntityWithUri(String uri, String label, String description) {
        this.m_uri = URI.create(uri);
        this.m_label = label;
        this.m_description = description;
    }

    public EntityWithUri() {
    }

    public String getLabel() {
        return this.m_label;
    }

    public void setLabel(String label) {
        this.m_label = label;
    }

    public URI getUri() {
        return this.m_uri;
    }

    public void setUri(URI uri) {
        this.m_uri = uri;
    }

    public String getDescription() {
        return m_description;
    }

    public void setDescription(String m_description) {
        this.m_description = m_description;
    }

    public EntityWithUri deepCopy() {
        EntityWithUri other = new EntityWithUri();

        other.m_uri = this.m_uri;
        other.m_label = this.m_label;
        other.m_description = this.m_description;

        return other;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof EntityWithUri)){
            return false;
        }

        EntityWithUri other = (EntityWithUri)obj;

        return ObjectHelpers.equals(this.m_uri, other.m_uri);
    }

    @Override
    public int hashCode() {
        int hash = ~0;

        hash *= this.m_uri.hashCode();

        return hash;
    }


}
