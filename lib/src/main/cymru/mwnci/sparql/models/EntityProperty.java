package cymru.mwnci.sparql.models;

import javax.xml.datatype.XMLGregorianCalendar;

public class EntityProperty {

    private final Object m_value;
    private final boolean m_propertyIsEntity;

    private EntityProperty(boolean propertyIsEntity, Object value){
        this.m_propertyIsEntity = propertyIsEntity;
        this.m_value = value;
    }

    public static EntityProperty createForEntity(EntityWithUri entityWithUri){
        return new EntityProperty(true, entityWithUri);
    }

    public static EntityProperty createForValue(Object value){
        return new EntityProperty(false, value);
    }

    public Object getValue(){
        return m_value;
    }

    /**
     * Implies that getValue will return
     * @return
     */
    public boolean getPropertyIsEntity(){
        return m_propertyIsEntity;
    }

    public boolean getPropertyIsDate() {
        return m_value instanceof XMLGregorianCalendar;
    }

    public XMLGregorianCalendar getDate() {
        return (XMLGregorianCalendar) m_value;
    }


    public EntityWithUri getEntityWithUri() {
        if(!m_propertyIsEntity){
            throw new IllegalArgumentException("Property is not an entity. Illegal access.");
        }

        return (EntityWithUri)m_value;
    }

}
