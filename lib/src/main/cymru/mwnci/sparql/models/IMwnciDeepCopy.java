package cymru.mwnci.sparql.models;

public interface IMwnciDeepCopy<T> {
    T deepCopy();
}
