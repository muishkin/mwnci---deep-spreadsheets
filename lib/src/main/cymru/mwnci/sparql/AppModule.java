package cymru.mwnci.sparql;

import com.google.inject.AbstractModule;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;
import cymru.mwnci.sparql.services.SparqlService;

public class AppModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ISparqlService.class).to(SparqlService.class);
    }
}
