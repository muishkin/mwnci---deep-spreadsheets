package cymru.mwnci.sparql.helpers;

import cymru.mwnci.sparql.models.EntityWithUri;

public class EntityHelpers {
    private EntityHelpers() {
    }

    public static String getDisplayTextForEntityWithUri(EntityWithUri entityWithUri) {
        StringBuilder displayText = new StringBuilder();

        displayText.append(entityWithUri.getLabel());

        String description = entityWithUri.getDescription();
        if (description != null && !description.isEmpty()) {
            displayText.append(" - ");
            displayText.append(description);
        }

        displayText.append(" (");
        displayText.append(entityWithUri.getUri());
        displayText.append(")");

        return displayText.toString();
    }
}
