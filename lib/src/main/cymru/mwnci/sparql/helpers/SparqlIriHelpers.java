package cymru.mwnci.sparql.helpers;

import org.eclipse.rdf4j.sparqlbuilder.rdf.Iri;

import java.net.URI;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SparqlIriHelpers {
    // Basic URI matching pattern, keeps matching after http:// or https:// until it hits a space.
    private static final String URI_BASIC_PATTERN = "https?:\\/\\/[^\\s]*";
    private static final String BRACKET_START = "[\\(\\[\\{]";
    private static final String BRACKET_END = "[\\)\\]\\}]";
    private static final Pattern START_OR_END_BRACKETS_MATCHER = Pattern.compile(
            "((^" + BRACKET_START + "+)|(" + BRACKET_END + "+$))",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.UNICODE_CHARACTER_CLASS
    );
    // The URI matching pattern is not perfect and will likely need improvement later.
    private static final Pattern URI_TEXT_MATCHER = Pattern.compile(
            "((" + BRACKET_START + URI_BASIC_PATTERN + BRACKET_END + ")|(" + URI_BASIC_PATTERN + "))",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.UNICODE_CHARACTER_CLASS
    );

    private SparqlIriHelpers() {
    }

    public static Iri mapUriToSparqlIri(URI entityUri) {
        return mapUriToSparqlIri(entityUri.toString());
    }

    public static Iri mapUriToSparqlIri(String entityUri) {
        return () -> "<" + entityUri + ">";
    }

    public static Optional<URI> getEntityUriFromString(String displayText) {
        Matcher m = URI_TEXT_MATCHER.matcher(displayText);

        if (!m.find()) {
            return Optional.empty();
        }

        String uri = START_OR_END_BRACKETS_MATCHER.matcher(m.group(1)).replaceAll("");
        return Optional.of(URI.create(uri));
    }
}
