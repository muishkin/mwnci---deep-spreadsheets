package cymru.mwnci.sparql.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Debug {
    private static final Logger logger = LogManager.getLogger(Debug.class);

    public static final boolean IN_DEBUG = getWhetherInDebug();


    private Debug() {
    }

    private static boolean getWhetherInDebug() {
        String prop = System.getenv("MWNCI_DEBUG");
        boolean inDebug = prop != null
                && Boolean.parseBoolean(prop);

        if (inDebug) {
            logger.debug("In debug mode.");
        }

        return inDebug;
    }
}
