package cymru.mwnci.sparql.helpers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.rdf4j.common.iteration.Iteration;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamHelpers {

    private StreamHelpers() {
    }

    private static Logger logger = LogManager.getLogger(StreamHelpers.class);

    /**
     * Converts an `Iteration<T, X>` to a `Stream<T>` and logs exceptions.
     * Fails loudly by converting the exception to a RuntimeException.
     *
     * Converts to `Iterator<T>` and `Spliterator<T>` during the process.
     * @param it
     * @param <T>
     * @param <X>
     * @return
     */
    public static <T, X extends Exception> Stream<T> iterationToStream(Iteration<T, X> it) {
        Iterator<T> iterator = new Iterator<T>() {
            public boolean hasNext() {
                try {
                    return it.hasNext();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    throw new RuntimeException(e);
                }
            }

            public T next() {
                try {
                    return it.next();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    throw new RuntimeException(e);
                }
            }
        };

        Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(iterator, 0);

        return StreamSupport.stream(spliterator, false);
    }
}
