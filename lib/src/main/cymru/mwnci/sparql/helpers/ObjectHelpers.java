package cymru.mwnci.sparql.helpers;

public class ObjectHelpers {

    private ObjectHelpers() {
    }

    public static boolean equals(Object one, Object two) {
        return (one == null && two == null)
                || (one != null && one.equals(two));
    }
}
