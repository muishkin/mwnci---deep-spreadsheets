package cymru.mwnci.sparql.helpers;

import org.eclipse.rdf4j.model.impl.SimpleNamespace;
import org.eclipse.rdf4j.sparqlbuilder.core.Prefix;
import org.eclipse.rdf4j.sparqlbuilder.core.SparqlBuilder;

/**
 * Class containing namespace helpers for common RDF ontologies.
 */
public class NS {
    public final static String XSD_NAMESPACE = "http://www.w3.org/2001/XMLSchema#";
    public final static String GEO_NAMESPACE = "http://www.opengis.net/ont/geosparql#";
    public final static String RDF_NAMESPACE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";

    public static Prefix XSD = SparqlBuilder.prefix(
            new SimpleNamespace("xsd", XSD_NAMESPACE)
    );

    public static Prefix GEO = SparqlBuilder.prefix(
            new SimpleNamespace("geo", GEO_NAMESPACE)
    );

    public static final Prefix RDF = SparqlBuilder.prefix(
            new SimpleNamespace("rdf", RDF_NAMESPACE));
    public static final Prefix RDFS = SparqlBuilder.prefix(
            new SimpleNamespace("rdfs", "http://www.w3.org/2000/01/rdf-schema#"));
    public static final Prefix OWL = SparqlBuilder.prefix(
            new SimpleNamespace("owl", "http://www.w3.org/2002/07/owl#"));
    public static final Prefix SCHEMA = SparqlBuilder.prefix(
            new SimpleNamespace("schema", "http://schema.org/"));
    public static final Prefix DC = SparqlBuilder.prefix(
            new SimpleNamespace("dc", "http://purl.org/dc/elements/1.1/"));


    public static final String TYPE_XSD_STRING = XSD_NAMESPACE + "string";
    public static final String TYPE_XSD_DECIMAL = XSD_NAMESPACE + "decimal";
    public static final String TYPE_XSD_DOUBLE = XSD_NAMESPACE + "double";
    public static final String TYPE_XSD_INT = XSD_NAMESPACE + "integer";
    public static final String TYPE_XSD_DATE_TIME = XSD_NAMESPACE + "dateTime";
    public static final String TYPE_XSD_DATE = XSD_NAMESPACE + "date";
    public static final String TYPE_XSD_TIME = XSD_NAMESPACE + "time";
    public static final String TYPE_XSD_BOOLEAN = XSD_NAMESPACE + "boolean";

    public static final String TYPE_GEO_WKT = GEO_NAMESPACE + "wktLiteral";

    public static final String TYPE_RDF_LANG_STRING = RDF_NAMESPACE + "langString";

    public NS() {
    }
}
