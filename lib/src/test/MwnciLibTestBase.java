import cymru.mwnci.sparql.exceptions.EntityNotFoundException;
import cymru.mwnci.sparql.exceptions.TooManyValuesFoundException;
import cymru.mwnci.sparql.exceptions.ValueNotFoundException;
import cymru.mwnci.sparql.models.EntityProperty;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.services.Interfaces.ISparqlProvider;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

public class MwnciLibTestBase {
    protected ISparqlService m_sparqlService;
    protected ISparqlProvider m_repoProvider;

    protected void assertEntityHasExpectedLabel(URI entityUri, String language, String expectedLabel)
            throws EntityNotFoundException, TooManyValuesFoundException {
        EntityWithUri entityWithUri = m_sparqlService.getEntityForUri(m_repoProvider, entityUri, language);

        this.assertEntityWithUriValid(entityWithUri);

        assertEquals(entityWithUri.getLabel(), expectedLabel);
        assertEquals(entityWithUri.getUri().toString(), entityUri.toString());
    }

    protected void assertEntityWithUriValid(EntityWithUri res) {
        String label = res.getLabel();
        URI uri = res.getUri();

        assertNotNullOrEmpty(label);

        // Should have scheme, host and path populated for all URIs.
        assertNotNullOrEmpty(uri.getScheme());
        assertNotNullOrEmpty(uri.getHost());
        assertNotNullOrEmpty(uri.getPath());
    }

    protected void assertNotNullOrEmpty(String text) {
        assertNotNull(text);
        assertFalse(text.isEmpty());
    }

    protected void assertLiteralValueReturnedForEntity(URI subjectUri, String propertyName) throws TooManyValuesFoundException, ValueNotFoundException {
        EntityProperty entityProperty = m_sparqlService.getPropertyValueForEntity(
                m_repoProvider,
                subjectUri,
                propertyName,
                "en",
                "en",
                false
        );
        assertFalse(entityProperty.getPropertyIsEntity());
        assertThrows(IllegalArgumentException.class, () -> entityProperty.getEntityWithUri());
        assertNotNull(entityProperty.getValue());
    }

}
