import cymru.mwnci.sparql.Mwnci;
import cymru.mwnci.sparql.exceptions.EntityNotFoundException;
import cymru.mwnci.sparql.exceptions.SparqlProviderNotFoundException;
import cymru.mwnci.sparql.exceptions.TooManyValuesFoundException;
import cymru.mwnci.sparql.exceptions.ValueNotFoundException;
import cymru.mwnci.sparql.models.EntityProperty;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ScotGovStatisticsProviderTests extends MwnciLibTestBase {

    private static final String SCOTLAND = "http://statistics.gov.scot/id/statistical-geography/S92000003";
    private static final String DUNDEE = "http://statistics.gov.scot/id/statistical-geography/S12000042";

    @BeforeAll
    public void init() throws SparqlProviderNotFoundException {
        this.m_sparqlService = Mwnci.getService(ISparqlService.class);
        this.m_repoProvider = Mwnci.getSparqlProvider("ScotGovtStats");
    }

    @Test
    @DisplayName("Test that a known entity can be found.")
    public void testEntityLookup()
            throws EntityNotFoundException, TooManyValuesFoundException {
        URI entityUri = URI.create("http://statistics.gov.scot/data/alcohol-related-discharge");

        EntityWithUri entityWithUri = m_sparqlService.getEntityForUri(m_repoProvider, entityUri, "en");

        this.assertEntityWithUriValid(entityWithUri);

        assertEquals("Alcohol Related Hospital Discharges", entityWithUri.getLabel());
        assertEquals(entityUri.toString(), entityWithUri.getUri().toString());
    }

    @Test
    @DisplayName("Test literal int value lookup.")
    public void testIntValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create("http://statistics.gov.scot/data/population-estimates-current-geographic-boundaries/year/2018/S92000003/age/all/sex/all/people/count");

        assertLiteralValueReturnedForEntity(subjectUri, "count");
    }


    @Test
    @DisplayName("Test literal string value lookup.")
    public void testStringValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create(DUNDEE);

        assertLiteralValueReturnedForEntity(subjectUri, "display name");
    }


    @Test
    @DisplayName("Test literal boolean value lookup.")
    public void testBooleanValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create(DUNDEE);

        assertLiteralValueReturnedForEntity(subjectUri, "active");
    }


    @Test
    @DisplayName("Test literal date value lookup.")
    public void testDateValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create(DUNDEE);

        assertLiteralValueReturnedForEntity(subjectUri, "active from");
    }


    @Test
    @DisplayName("Test raw predicate URI lookup.")
    public void testRawPredicateUriValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create(SCOTLAND);

        assertLiteralValueReturnedForEntity(subjectUri, "http://statistics.data.gov.uk/def/statistical-geography#officialname");
    }


    @Test
    @DisplayName("Test related entity value lookup.")
    public void testEntityValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create(DUNDEE);

        String propertyName = "parent";

        EntityProperty entityProperty = m_sparqlService.getPropertyValueForEntity(
                m_repoProvider,
                subjectUri,
                propertyName,
                "en",
                "en",
                false
        );
        assertTrue(entityProperty.getPropertyIsEntity());
        EntityWithUri entityWithUri = entityProperty.getEntityWithUri();
        assertNotNull(entityWithUri);

        assertNotNull(entityWithUri.getLabel());
        assertNotNull(entityWithUri.getUri());
    }
}
