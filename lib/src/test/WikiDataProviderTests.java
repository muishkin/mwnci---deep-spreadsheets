import cymru.mwnci.sparql.Mwnci;
import cymru.mwnci.sparql.exceptions.EntityNotFoundException;
import cymru.mwnci.sparql.exceptions.SparqlProviderNotFoundException;
import cymru.mwnci.sparql.exceptions.TooManyValuesFoundException;
import cymru.mwnci.sparql.exceptions.ValueNotFoundException;
import cymru.mwnci.sparql.models.EntityProperty;
import cymru.mwnci.sparql.models.EntityWithUri;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.net.URI;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class WikiDataProviderTests extends  MwnciLibTestBase {

    @BeforeAll
    public void init() throws SparqlProviderNotFoundException {
        this.m_sparqlService = Mwnci.getService(ISparqlService.class);
        this.m_repoProvider = Mwnci.getSparqlProvider("WikiData");
    }

    @Test
    @DisplayName("Test that the WikiData subject search works.")
    public void testSearch() {
        List<EntityWithUri> results = m_sparqlService.searchForSubject(
                m_repoProvider, "wolver", "en", "", 10);

        assertFalse(results.isEmpty());

        results.forEach(this::assertEntityWithUriValid);
    }

    @Test
    @DisplayName("Test that a known entity can be found.")
    public void testEntityLookup()
            throws EntityNotFoundException, TooManyValuesFoundException {
        URI entityUri = URI.create("http://www.wikidata.org/entity/Q126269");

        EntityWithUri entityWithUri = m_sparqlService.getEntityForUri(m_repoProvider, entityUri, "en");

        this.assertEntityWithUriValid(entityWithUri);

        assertEquals("Wolverhampton", entityWithUri.getLabel());
        assertEquals(entityUri.toString(), entityWithUri.getUri().toString());
    }

    @Test
    @DisplayName("Test literal int value lookup.")
    public void testIntValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create("http://www.wikidata.org/entity/Q126269");

        assertLiteralValueReturnedForEntity(subjectUri, "population");

    }

    @Test
    @DisplayName("Test literal string value lookup.")
    public void testStringValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create("http://www.wikidata.org/entity/Q126269");

        assertLiteralValueReturnedForEntity(subjectUri, "official name");
    }

    @Test
    @DisplayName("Test literal geographic coordinate value lookup.")
    public void testGeoCoordValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create("http://www.wikidata.org/entity/Q126269");

        assertLiteralValueReturnedForEntity(subjectUri, "coordinate location");
    }

    @Test
    @DisplayName("Test raw predicate URI lookup.")
    public void testRawPredicateUriValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create("http://www.wikidata.org/entity/Q84");

        assertLiteralValueReturnedForEntity(subjectUri, "http://www.wikidata.org/prop/direct/P571");
    }

    @Test
    @DisplayName("Test related entity value lookup.")
    public void testEntityValueLookup()
            throws ValueNotFoundException, TooManyValuesFoundException {
        URI subjectUri = URI.create("http://www.wikidata.org/entity/Q126269");

        String propertyName = "Country";

        EntityProperty entityProperty = m_sparqlService.getPropertyValueForEntity(
                m_repoProvider,
                subjectUri,
                propertyName,
                "en",
                "en",
                false
        );
        assertTrue(entityProperty.getPropertyIsEntity());
        EntityWithUri entityWithUri = entityProperty.getEntityWithUri();
        assertNotNull(entityWithUri);

        assertNotNull(entityWithUri.getLabel());
        assertNotNull(entityWithUri.getUri());
    }

}
