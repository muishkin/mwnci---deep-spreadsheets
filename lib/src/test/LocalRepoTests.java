import cymru.mwnci.sparql.Mwnci;
import cymru.mwnci.sparql.exceptions.EntityNotFoundException;
import cymru.mwnci.sparql.exceptions.SparqlProviderNotFoundException;
import cymru.mwnci.sparql.exceptions.TooManyValuesFoundException;
import cymru.mwnci.sparql.services.Interfaces.ISparqlService;
import cymru.mwnci.sparql.services.providers.LocalTestProvider;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.net.URI;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LocalRepoTests extends MwnciLibTestBase {

    private static final String REPO_BASE_PATH = "http://mwnci.cymru/rdf/test/";

    @BeforeAll
    public void init() throws SparqlProviderNotFoundException {
        this.m_sparqlService = Mwnci.getService(ISparqlService.class);
        this.m_repoProvider = Mwnci.getSparqlProvider(LocalTestProvider.NAME);
    }

    @Test
    @DisplayName("Test that a known entity can be found.")
    public void testEntityLookup()
            throws EntityNotFoundException, TooManyValuesFoundException {
        URI entityUri = URI.create(REPO_BASE_PATH + "entity1");

        assertEntityHasExpectedLabel(entityUri, "en", "My first entity");
        assertEntityHasExpectedLabel(entityUri, "fr", "Mon entité première");
        assertEntityHasExpectedLabel(entityUri, "de", "Mein erst Entität");
    }

//    @Test
//    @DisplayName("Test literal int value lookup.")
//    public void testIntValueLookup()
//            throws ValueNotFoundException, TooManyValuesFoundException {
//        URI subjectUri = URI.create("http://statistics.gov.scot/data/population-estimates-current-geographic-boundaries/year/2018/S92000003/age/all/sex/all/people/count");
//
//        assertLiteralValueReturnedForEntity(subjectUri, "count");
//    }
//
//
//    @Test
//    @DisplayName("Test literal string value lookup.")
//    public void testStringValueLookup()
//            throws ValueNotFoundException, TooManyValuesFoundException {
//        URI subjectUri = URI.create(DUNDEE);
//
//        assertLiteralValueReturnedForEntity(subjectUri, "display name");
//    }
//
//
//    @Test
//    @DisplayName("Test literal boolean value lookup.")
//    public void testBooleanValueLookup()
//            throws ValueNotFoundException, TooManyValuesFoundException {
//        URI subjectUri = URI.create(DUNDEE);
//
//        assertLiteralValueReturnedForEntity(subjectUri, "active");
//    }
//
//
//    @Test
//    @DisplayName("Test literal date value lookup.")
//    public void testDateValueLookup()
//            throws ValueNotFoundException, TooManyValuesFoundException {
//        URI subjectUri = URI.create(DUNDEE);
//
//        assertLiteralValueReturnedForEntity(subjectUri, "active from");
//    }
//
//
//    @Test
//    @DisplayName("Test raw predicate URI lookup.")
//    public void testRawPredicateUriValueLookup()
//            throws ValueNotFoundException, TooManyValuesFoundException {
//        URI subjectUri = URI.create(SCOTLAND);
//
//        assertLiteralValueReturnedForEntity(subjectUri, "http://statistics.data.gov.uk/def/statistical-geography#officialname");
//    }
//
//
//    @Test
//    @DisplayName("Test related entity value lookup.")
//    public void testEntityValueLookup()
//            throws ValueNotFoundException, TooManyValuesFoundException {
//        URI subjectUri = URI.create(DUNDEE);
//
//        String propertyName = "parent";
//
//        EntityProperty entityProperty = m_sparqlService.getPropertyForEntity(
//                m_localRepoProvider,
//                subjectUri,
//                propertyName,
//                "en",
//                "en"
//        );
//        assertTrue(entityProperty.getPropertyIsEntity());
//        EntityWithUri entityWithUri = entityProperty.getEntityWithUri();
//        assertNotNull(entityWithUri);
//
//        assertNotNull(entityWithUri.getLabel());
//        assertNotNull(entityWithUri.getUri());
//    }
//

}
